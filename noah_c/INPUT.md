Namelist documentation in the "helgoland" setup
===============================================

mossco_run.nml:
  configures simulation period and simulation name

gotmrun.nml:
  configures common simulation parameters of the GOTM component

gotmmean.nml
  configures parameters of GOTM mean flow simulation

gotmturb.nml:
  configures parameters of GOTM's turbulence library

airsea.nml:
  configures GOTM's internal forcing by atmospheric conditions

obs.nml:
  configures internal forcing of variables in GOTM (such as pressure gradients, sea surface elevation, reference temperature and salinity profiles)

fabm.nml:
  configures FABM in water/pelagic components (fabm_gotm, fabm0d).
  this namelist links by default to fabm-npzd-spm.nml

fabm_sed.nml:
  configures FABM in the soil/sediment components (fabm_sediment)

run_sed.nml:
  configures commonn simulation parameters of the fabm_sediment component

constant.dat:
  list of name,value pairs to be used by the constant_component

gotm_meteo.dat:
  meteorological data to be used by GOTM's internal airsea module

benthic.nml:
 configure the erosed parameters regarding underlayer mechanism and number of horizontal elements
transportparam.nml:
obsolent
sedparams.txt:
 list of all parameters needed for non-cohesive and cohesive sediment erosion and deposition
mbalthica.nml:
 list of parameters required for Macoma balthica or equivalentley Tellina fabula
microphyt.nml:
 list of parameters required for microphytobenthos
