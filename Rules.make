# This Makefile snippet is part of MOSSCO; definition of MOSSCO-wide make rules
#
# @copyright (C) 2013-2020 Helmholtz-Zentrum Geesthacht
# @author Carsten Lemmen <carsten.lemmen@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#

.EXPORT_ALL_VARIABLES:

ifndef MOSSCO_DIR
  $(error Please define MOSSCO_DIR)
endif

external_GETMDIR=$(MOSSCO_DIR)/external/getm/code
ifndef MOSSCO_GETMDIR
  ifneq ($(wildcard $(external_GETMDIR)/src/getm/main.F90),)
    MOSSCO_GETMDIR=$(external_GETMDIR)
  endif
endif
ifdef MOSSCO_GETMDIR
  GETMDIR=$(MOSSCO_GETMDIR)
endif

external_GOTMDIR=$(MOSSCO_DIR)/external/gotm/code
ifndef MOSSCO_GOTMDIR
	ifneq ($(wildcard $(external_GOTMDIR)/src/gotm/gotm.F90),)
    MOSSCO_GOTMDIR=$(external_GOTMDIR)
  endif
endif
ifdef MOSSCO_GOTMDIR
  GOTMDIR=$(MOSSCO_GOTMDIR)
endif

external_editscenario=../external/editscenario/editscenario/editscenario.py
ifneq ($(wildcard $(external_editscenario)),)
  EDITSCENARIO=$(external_editscenario)
else
external_editscenario=../external/editscenario/bin/editscenario.py
ifneq ($(wildcard $(external_editscenario)),)
EDITSCENARIO=$(external_editscenario)
else
  external_editscenario=../external/editscenario/editscenario.py
ifneq ($(wildcard $(external_editscenario)),)
	EDITSCENARIO=$(external_editscenario)
else
  $(warning You might need the `editscenario` program, please run `make external`)
  EDITSCENARIO=editscenario.py
endif
endif
endif

.PHONY: info clean distclean

info:
	$(MAKE) -C $(MOSSCO_DIR) info namlist_gotm namelist_mossco namelist_getm

namelist_mossco: $(setup)_mossco.xml
	$(RM) mossco_run.nml
	$(EDITSCENARIO) $(setup)_mossco.xml -e nml . --schemadir=$(MOSSCO_DIR)/schemas --targetversion=mossco-0.1
ifdef GETMDIR
namelist_getm: $(name).xml
	$(RM) getm.inp
	$(EDITSCENARIO) $(name).xml -e nml . --schemadir=$(GETMDIR)/schemas --targetversion=getm-2.5
endif
ifdef GOTMDIR
namelist_gotm: $(setup).xml
	$(EDITSCENARIO) $(setup).xml --skipvalidation -e nml . --schemadir=$(GOTMDIR)/gui.py/schemas/scenario --targetversion=gotm-4.1.0
endif

clean:
	@-$(RM) *stderr *stdout PET* *.pe[0-9]* *.po[0-9]* *.o[0-9]* *.e[0-9]*
	@-$(RM) -rf  *dSYM core.* stats*.txt

distclean: clean $(name)_clean
	@-$(RM) restart*.out *.*[0-9].nc
