# -*- coding: utf-8 -*-
"""
# This script is is part of MOSSCO. It creates a 1D (point/node data)
# UGRID/CF compliant NetCDF file for stations
#
# @copyright (C) 2018 Helmholtz-Zentrum Geesthacht
# @author Carsten Lemmen
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#
"""
import netCDF4
import numpy as np
import sys
import os
import csv

def createUgrid(locationdict):

    nnodes=len(locationdict)
    nedges=nnodes - 1 # Create an edge between subsequent nodes
    nbounds=2

    ugridfile='bsh_stations_locstream.nc'
    nc=netCDF4.Dataset(ugridfile,'w',format='NETCDF3_CLASSIC')

    nc.Conventions = 'CF-1.7, UGRID-1.0'

    nc.createDimension('nodes',nnodes)
    nc.createDimension('edges',nedges)
    nc.createDimension('bounds',nbounds)

    m = nc.createVariable('station_locstream','i4',())
    m.cf_role='mesh_topology'
    m.topology_dimension = 1.
    m.node_coordinates='lon lat'
    m.edge_node_connectivity='connectivity'

    lat = nc.createVariable('lat','double',('nodes'))
    lat.standard_name='latitude'
    lat.units='degrees_north'

    lon = nc.createVariable('lon','double',('nodes'))
    lon.standard_name='longitude'
    lon.units='degrees_east'

    conn = nc.createVariable('connectivity','i',('edges','bounds'))
    conn.description='edge_node_connectivity'
    conn.start_index=0

    for i,key in enumerate(locationdict) :
        lat[i]=locationdict[key]['coords'][0]
        lon[i]=locationdict[key]['coords'][1]

    for i in range(0,nedges):
        conn[i,:] = [i,i+1]

    nc.title = 'Test file for locstream generation from UGRID in ESMF'
    nc.institution = 'Helmholtz-Zentrum Geesthacht'
    nc.author = 'Carsten Lemmen <carsten.lemmen@hzg.de>'
    nc.license = 'CC-by-SA 4.0'
    nc.source = 'Sample station locations obtained from BSH website'
    nc.history = 'Created with {}'.format(sys.argv[0])

    nc.close()

if __name__ == '__main__':


    estuary_cells = {}
    estuary_cells['T11']={'name':'T11','coords':(54.67,6.92)}
    estuary_cells['T12']={'name':'T12','coords':(54.67,7.4)}
    estuary_cells['T2']={'name':'T2','coords':(55.2,5)}
    estuary_cells['T22']={'name':'T22','coords':(54.13,6.35)}
    estuary_cells['T26']={'name':'T26','coords':(54.18,7.46)}

    estuary_cells['T36']={'name':'T36','coords':(53.68,6.42)}
    estuary_cells['T41']={'name':'T41','coords':(54,8.11)}
    estuary_cells['T5']={'name':'T5','coords':(55,6.33)}
    estuary_cells['T8']={'name':'T8','coords':(55,8)}
    estuary_cells['T26']={'name':'T26','coords':(54.18,7.46)}

    createUgrid(estuary_cells)
