# !/bin/bash

GRIDS="esacci wilson sns sns3d nsbs remo clm alphaventus elbeweser oderhaff wbs bsh_stations bdyinfo"
FORMATS="gridspec scrip ugrid esmf"
GRIDS=sns3d

rm -f grid2netcdf.log

for G in ${GRIDS}; do

  test -f ${G}_input.cfg || continue

  for F in ${FORMATS}; do

    test -f ${G}_${F}.nc || continue

    UPCASE=$(echo $F | awk '{print toupper($0)}')

cat << EOT > ${G}_grid_${F}.cfg
format: ${UPCASE}
filename: ${G}_${F}.nc
EOT

cat << EOT > grid2netcdf_${G}_${F}.yaml
dependencies:
  - ${G}_input:
    - component: ${G}_grid_${F}
      grid: ${G}_grid_${F}

instances:
 - ${G}_grid_${F}: grid
 - ${G}_input: netcdf_input
 - ${G}_${F}_netcdf: netcdf

coupling:
  - components:
    - ${G}_grid_${F}
    - ${G}_input
    interval: none
  - components:
    - ${G}_input
    - ${G}_${F}_netcdf
    interval: 30 m
EOT

    rm -f PET0.regrid-1x1-grid2netcdf_${G}_${F}
    mossco -b grid2netcdf_${G}_${F}
    ./grid2netcdf_${G}_${F}

    L=$(tail -2 PET0.regrid-1x1-grid2netcdf_${G}_${F} | grep -c finished)
    if [[ "$L" == "1" ]] ; then
      echo "PASSED test ${G}_${F}, check file ${G}_${F}_netcdf.nc" | tee -a grid2netcdf.log
    else
      echo "FAILED test ${G}_${F}"  | tee -a grid2netcdf.log
    fi
  done

done
