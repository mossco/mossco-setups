Ich selbst habe aus Testgründen bisher nur ICON-Grids mit den
notwendigsten Daten erzeugt. Daher weiß ich gerade nicht ob meine Gitter
CF-compliant sind. Hier (http://icon-downloads.zmaw.de/) findest du sowohl
globale als auch regionale Gitter in unterschiedlichen Auflösungen. Eine
Help-page mit Beschreibung ist auch vorhanden. Vielleicht findest du etwas
was du brauchst.
