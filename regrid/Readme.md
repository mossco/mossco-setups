---
author: Carsten Lemmen
copyright: 2017, 2018 Helmholtz-Zentrum Geesthacht
license: CC-by-SA 4.0
title: Instructions for the `$MOSSCO_SETUPDIR/regrid` setup
...

This setup was created to test regridding with the `grid_component` and the
regrid_coupler.

# Preparation

All configuration files are local to this directory, but
some data files need to be created  or linked.

1. Create the necessary `SCRIP` files from the NSBS6nm, SNS, Oderhaff, gb300m,
   and the WBS setups, and
   optionally from other sources.  You can use the scripts `latlon2scrip.py`
   and `topo2scrip.py` that reside in `$MOSSCO_DIR/scripts` or use netcdf
   operators (use the Makefile target `make grids`).  File locations
   to the resulting grid specification are specified in `*_grid.cfg`.

2. Make sure that this directory (or a copy of it) is located under
   `$MOSSCO_SETUPDIR`, such that links to the porosity input files
   (specified in `*_porosity.cfg`) and the topo files (`*_topo.nc`) work.
   Otherwise, you need to adjust the paths.

3. Some files may need to be privately shared (e.g. via a Dropbox), as they
   cannot be made publicly available.

# Model domains

The grids and models used in this setup come either from files in grid
specification formats (SCRIP, GRIDSPEC/CF, UGRID, or ESMF)
or from the `getm` hydrodynamic model.

Testing should start with the nsbs domain (small memory footprint and rectilinear
spherical grid) and proceed to sns (more complicated curvilinear spherical grid),
three dimensions and unstructured.

## GETM grids

- North Sea/Baltic Sea (nsbs): this is a low-resolution (10 km) setup with 37488
  grid points (213 x 176) covering both the North Sea and the Baltic Sea on
  a spherical grid
- Southern North Sea (sns): this is a medium-resolution (2-4 km) setup with
  13761 grid points (139 x 99) on a curvilinear spherical grid. It is a subdomain
  of the nsbs for the North Sea.
- German Bight 300 (gb300): this is a high-resolution setup with 300 m resolution
  All examples with this need large amounts of memory.  It is a
  subdomain of the nsbs and sns for the North Sea.
- Western Baltic Sea (wbs): this is a high-resolution setup with 521794 grid
  points.  All examples with this need large amounts of memory.  It is a
  subdomain of the nsbs for the Baltic Sea.
- Oderhaff (oderhaff): this is a high-resolution setup with 338122 grid points.
  It is a subdomain of the nsbs and wbs on a spherical grid.

All of the above grids can be created online by the GETM component based on a
bathmetry file that is specified by the setting `bathymetry = ` in `getm.inp`.
This is by default the file `nsbs_topo.nc`.  Other topographies can be used
instead and can be found in the respective directories of the other setups
(in repo for GB300m, SNS and NSBS, privately shared for Oderhaff and WBS).

> For all of these grids, SCRIP files have been created (which only work in 2D),
> and work is required for CF/GRIDSPEC versions of these in three dimensions.
> 2D CF are available for nsbs and sns.

## Non-GETM geometries

- REMO is the grid that atmospheric data from the REMO model is provided on
- CLM is the (rotated pole) atmospheric grid from the Community Climate Limited Area Model
- TRIM is on oceanographic grid on rectilinear Gauss-Krüger coordinates.
- UNTRIM is an unstructured grid.
- ESACCI is a structured spherical grid from satellite observations
- alphaventus is a very small part of the SNS, available as structured spherical grid.
- wilson is a Wilson (2018) North Atlantic shelf grid, structured spherical
- elbeweser is a ugrid containing river flux for the Elbe and Weser estuaries, both
  as point data and as area data, within the SNS domain
- ices_boxes is a ugrid with 5 large (32 node) elements in the southern North Sea
- bsh_stations is location stream of 9 BSH (German Federal Hydrographic Agency)
  stations located within the SNS

GRIDSPEC/CF
:: alphaventus, esacci, remo, wilson

SCRIP
:: clm, esacci, remo, wilson

UGRID
:: untrim_channel, elbeweser_unstructured, elbeweser_locstream, ices_boxes,
   bsh_stations

ESMF
:: (none)

All files in SCRIP formats can be converted to UGRID/ESMF unstructured with
the ESMF_Scrip2Unstruc utility (see below). There is no such file yet for TRIM.

# Running the tests

Execute the following tests, and check the resulting `PET` log files for errors,
you may also examine the resulting `*_netcdf.nc` output files.

    make clean

## File-based no-regridding tests

    mossco nsbs
    mossco sns
    mossco nsbs2nsbs

All of these should pass.  There is still a problem
in updating the `in_soil` porosity from the surface,
but that is beyond regridding functionality.

Most existing grid reading and output to netcdf can
be tested with the utility `grid2netcdf.sh`

    ./grid2netcdf.sh

The status of grid reading (as a result of `grid2netcdf.sh`) is:

| Grid   | CF    | SCRIP |
| ------ | ----- | ----- |
| sns    | pass  | fail  |
| nsbs   | pass  | pass  |
| esacci | pass  | pass  |
| remo   | pass  | pass  |
| sns3d  | fail  | --    |
| wilson | fail  | fail  |

The failure of sns/scrip and wilson is probably due to a bad specification
in the scrip or gridspec files. They run, but produce garbage.  The sns3d
does not run since the grid is not correctly read as 3d but as
2d which confuses the subsequent netcdf_input_component.

## Online no-regridding tests

    mossco getm

Should pass and demonstrates that the hydrodynamic model can run.

## File-based tests that need regridding

### ESA CCI chlorophyll mapped onto all other grids

The first example `esacci2netcdf.yaml` tests regridding
from a subdomain (esacci) to all of
the specified sub- and superregions (alphaventus, clm, sns, remo, ices)
from geometries read from file.
Data on chlorophyll is read from the esacci_gridspec file and interpolated onto
other geometries; some of the targets need extrapolation.
Some of these may be commented out to increase processing speed.  Work is
required to save the internal state / global storage of the coupler to
memorize coupling fields handle across ESMF phases. Please comment/uncomment
to enable more or less regridding tasks.

    mossco esacci2netcdf

### Regular grid status table

| Target      | CF     | SCRIP |
| ----------- | ------ | ----- |
| sns         | works  | fails |
| nsbs        | works  | fails |
| esacci      | works1 | fails |
| remo        | works  | fails |
| sns3D       | fails  | n/a   |
| wilson      | t.b.d  | t.b.d |
| alphaventus | works  | n/a   |
| clm         | n/a    | works |
| gb300       | n/a    | n/a   |
| oderhaff    | n/a    | fail  |
| wbs         | n/a    | fail  |

Status
 - regridding to CF is complete for sns, nsbs, clm, remo, alphaventus,
   regridding onto itself (esacci) swaps latitude in ncview
 - regridding to SCRIP works only for clm and fails for all others
 - grid files are needed for gb300 test case
 - the sns 3d only produces 2d output

#### Unstructured and location stream status table

| Target                 | UGRID          | SCRIP | UGRID | ESMF |
| ---------------------- | -------------- | ----- | ----- | ---- |
| ices                   | works on nodes |       |       |      |
| bsh                    |                |       |       |      |
| elbeweser_locstream    |                | t.b.d |       |      |
| elbeweser_unstructured |                | t.b.d |       |      |

  - regridding to the ices_boxes UGRID currently projects onto nodes, but should
    ultimately project on elements.

### Wilson mean_disturbance mapped onto all other grids

The second example tests regridding from a North Atlantic shelf domain (wilson) to all of
the specified subregions (alphaventus, clm, esacci, nsbs, remo, sns) from grids read from file.
Data on mean_disturbance is read from the wilson_gridspec file and interpolated onto
other grids.

    mossco wilson2netcdf

| Target      | CF                     | SCRIP | UGRID |
| ----------- | ---------------------- | ----- | ----- |


There is a problem with the `wilson` SCRIP grid specification, such that this
test should be skipped until the SCRIP is fixed.

### NSBS porosity mapped onto all other grids

The third example tests regridding from the entire domain (nsbs) to all of
the specified subregions (sns, wbs, oderhaff) from grids read from file.
Data on deposition and porosity are read from nsbs_porosity/nsbs_deposition and
interpolated to other grids.
Some of these may be commented out to increase processing speed.  Work is
required to save the internal state / global storage of the coupler to
memorize coupling fields handle across ESMF phases. Please comment/uncomment
to enable more or less regridding tasks.

    mossco nsbs2netcdf

| Target                 | status                             |
| ---------------------- | ---------------------------------- |
| sns                    | works                              |
| nsbs                   | works                              |
| esacci                 | works*                             |
| remo                   | works                              |
| sns3D                  | fail, ESMF not implemented         |
| wilson                 | fail, SCRIP reading                |
| alphaventus            | works                              |
| clm                    | works                              |
| gb300                  | fail SCRIP reading                 |
| oderhaff               | fail result garbled                |
| wbs                    | fail result garbled                |
| bsh                    | fail ESMF not implemented          |
| elbeweser_unstructured | works, results need checking       |
| elbeweser_locstream    | fail MOSSCO netcdf not implemented |
|                        |                                    |

#### Adding a science component

In the fourth suite of tests, a science component is involved.
Most likely, these could fail in the `fabm_sediment_component.F90`, when
porosity is incorrectly regridded and produces negative values, that
are subsequently caught and throw an error. This might be circumvented
by using temperature instead of porosity for regridding, but it also
challenges the thinking on regridding at edges and grid masking.
Also, the tests may fail within the science components with NaN values.
Disregard this for now until fixed.

    mossco nsbs2alphaventus  # very small to inner domain
    mossco nsbs2sns          # small with problematic bounds
    mossco nsbs2remo         # small to outer domain
    mossco nsbs2clm          # small to outer domain

    mossco nsbs2wbs          # very large
    mossco nsbs2oderhaff     # very large
    mossco nsbs2gb300        # very large

    mossco sns2gb300

## Online regridding from getm to netcdf

The next example tests regridding from the entire domain (getm on nsbs topography)
to all of the specified subregions from grids read from file.

    mossco getm2netcdf

### Online regrid status table

| Target                 | status |
| ---------------------- | ------ |
| sns                    |        |
| nsbs                   |        |
| esacci                 |        |
| remo                   |        |
| alphaventus            |        |
| clm                    |        |
| elbeweser_unstructured |        |

Test cases `elbeweser_locstream`, `bsh`, `wilson`, `gb300`, `oderhaff`, and
`wbs` not tested yet due to earlier failures.

There is also a 3D example that currently only regrids onto the sns 3D CF file.  This file is currently not handled correctly for ESMF limitations reading 3D CF grids (we need alternate way via CF Meshes)

    mossco getm2netcdf3

## Online regridding from netcdf to getm

Vertical regridding from 7 layer input file (in sns horizontal specification, the data is in mossco_gf.nc).  Not expected to work until CF Meshes are implemented or ESMF fixed 3D Grid CF reading.
"netcdf_input_component.F90:846 InitializeP1 Operation not yet supported"

    mossco restart7getm.yaml

## Examples that do not work for other reasons

1. `wbs2oderhaff`: there is no porosity information for wbs, as long as the
  regridding from nsbs2wbs is not working

# Using ESMF_Regrid

## Test currently existing CF/GRIDSPEC file interconversions with ESMF_Regrid capabilities

for src in esacci wilson sns nsbs remo; do
  if [[ $src == esacci ]]; then V=chlor_a; fi
  if [[ $src == nsbs ]]; then V=bathymetry; fi
  if [[ $src == nsbs ]]; then V=bathymetry; fi
  if [[ $src == sns ]]; then V=bathymetry; fi
  if [[ $src == wilson ]]; then V=mean_disturbance; fi
  if [[ $src == remo ]]; then V=temperature_at_2m_above_water_surface; fi

  for dst in esacci wilson sns nsbs remo; do

    test -f ${src}_gridspec.nc || continue
    test -f ${dst}_gridspec.nc || continue

    S=${src}_gridspec.nc
    D=test_esmfregrid_${src}_${dst}.nc
    cp ${dst}_gridspec.nc $D

    for method in bilinear neareststod nearestdtos ; do
    echo ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    done
  done
done

## Test currently existing UGRID/CF file interconversions with ESMF_Regrid capabilities

for src in elbeweser; do
  if [[ $src == elbeweser ]]; then V=volflux; fi

  for dst in sns; do #esacci wilson sns nsbs remo; do

    S=${src}_locstream.nc
    D=test_esmfregrid_${src}_${dst}.nc

    test -f ${S} || continue
    test -f ${dst}_gridspec.nc || continue

    cp ${dst}_gridspec.nc $D

    for method in bilinear neareststod nearestdtos ; do
    echo ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    done
  done
done

for src in esacci wilson sns nsbs remo; do
#for src in sns; do
  if [[ $src == esacci ]]; then V=chlor_a; fi
  if [[ $src == nsbs ]]; then V=bathymetry; fi
  if [[ $src == nsbs ]]; then V=bathymetry; fi
  if [[ $src == sns ]]; then V=bathymetry; fi
  if [[ $src == wilson ]]; then V=mean_disturbance; fi
  if [[ $src == remo ]]; then V=temperature_at_2m_above_water_surface; fi

  for dst in bdyinfo_locstream ; do #elbeweser_locstream elbeweser_ugrid elbeweser_unstructured_scrip ices_boxes_ugrid; do

    S=${src}_gridspec.nc
    D=test_esmfregrid_${src}_${dst}.nc

    test -f ${S} || continue
    test -f ${dst}.nc || continue

    cp ${dst}.nc $D

    for method in bilinear neareststod nearestdtos ; do
    echo ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    ESMF_Regrid -s $S --src_var $V --src_regional --dst_regional -i -d $D --dst_var $method -m $method
    done
  done
done

# Test conversion of grids with other utilities

## ESMF-provided utitily to convert scrip files to unstructured files

for F in *_scrip.nc ; do
  if [[ ${F:0:4} == 'test' ]]; then continue; fi
  #echo ${F:0:4}
  #continue
  echo ESMF_Scrip2Unstruct  $F ${F%%_scrip.nc}_auto_esmf.nc  1 ESMF
  ESMF_Scrip2Unstruct  $F ${F%%_scrip.nc}_auto_esmf.nc  1 ESMF
  echo ESMF_Scrip2Unstruct  $F ${F%%_scrip.nc}_auto_ugrid.nc  1 UGRID
  ESMF_Scrip2Unstruct  $F ${F%%_scrip.nc}_auto_ugrid.nc  1 UGRID
done

## NCO

NCO from version 4.6.6 support conversion of 2D logically rectangular with
onedimensional coordinate to SCRIP and UGRID conventions.

    ncks -O --rgr infer --rgr ugrid=esacci_ugrid.nc \
     --rgr scrip=esacci_scrip.nc --rgr grd_title='ESA CCI' esacci_gridspec.nc dummy.nc

    ncks -O --rgr nfr=y --rgr grid=esacci_scrip.nc --rgr grd_ttl='ESA CCI' esacci_gridspec.nc dummy.nc

# Notes

Steps to recreate the TRIM file

    ncks -O -C -v lat.*,lon.*,h,X,Y,Z trim_grid1_sample.nc trim_grid.nc
