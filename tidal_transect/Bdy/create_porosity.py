import netCDF4
import sys
from pylab import linspace
from numpy import array,arange,ones
from datetime import datetime,timedelta

nc = netCDF4.Dataset('porosity_map.nc','w',format='NETCDF3_CLASSIC')

tnum = 12
ynum = 4
xnum = 60
mv = -1.e30

nc.createDimension('time',tnum)
nc.createDimension('x',xnum)
nc.createDimension('y',ynum)

t = nc.createVariable('time','f8',('time',))
t.units = 'seconds since 2003-01-01 00:00:00'
d = array([31,28,31,30,31,30,31,31,30,31,30,31])
t[:] = 86400.*(d.cumsum()-1.0*d)

vars = {}
vars['porosity_at_soil_surface']= linspace(0.35,0.56,xnum)

for varname,data in vars.iteritems():
  vv = nc.createVariable(varname,'f4',('time','y','x'))
  vv.long_name = varname
  vv.missing_value = mv
  vv[:] = mv
  for tt in range(tnum):
    vv[tt,1,:] = data
  nc.sync()

nc.close()
