import netCDF4
import sys
from numpy import array,arange,ones
from datetime import datetime,timedelta

if len(sys.argv)>1:
  n = int(sys.argv[1])
else:
  print "usage: create_mossco_bdy.py N (N - number of layers)"
  sys.exit(1)

nc = netCDF4.Dataset('fabm_pelagic_climatology.nc','w',format='NETCDF3_CLASSIC')

tnum = 12
ynum = 4
xnum = 60
mv = -1.e30

nc.createDimension('time',tnum)
nc.createDimension('x',xnum)
nc.createDimension('y',ynum)
nc.createDimension('layer',n)

t = nc.createVariable('time','f8',('time',))
t.units = 'seconds since 2003-01-01 00:00:00'
d = array([31,28,31,30,31,30,31,31,30,31,30,31])
t[:] = 86400.*(d.cumsum()-1.0*d)

vars = {}
vars['nutrients_in_water']= array([10.0,20.0,25.0,23.0,10.0,2.0,0.5,0.5,0.5,0.5,4.0,7.0])

vars['concentration_of_SPM_in_water_001'] = 7.0*ones((12,))
vars['concentration_of_SPM_in_water_002'] = 1.0*ones((12,))

for varname,data in vars.iteritems():
  vv = nc.createVariable(varname,'f4',('time','layer','y','x'))
  if len(varname.split('_in_water'))>1:
    vv.long_name = varname.split('_in_water')[0]+'_in_water'
  else:
    vv.long_name = varname
  vv.missing_value = mv
  vv[:] = mv
  for l in range(n):
    vv[:,l,1,1] = data
  nc.sync()

nc.variables['concentration_of_SPM_in_water_001'].external_index = 2
nc.variables['concentration_of_SPM_in_water_002'].external_index = 1
nc.sync()

nc.close()
