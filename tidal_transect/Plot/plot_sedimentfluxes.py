from pylab import *
import netCDF4
import sys
djet = cm.jet

if len(sys.argv)>1:
  ncfile = sys.argv[1]
else:
  ncfile = 'mossco_gffe.nc'

nc=netCDF4.Dataset(ncfile)
ncv=nc.variables

oxyf = ncv['dissolved_oxygen_upward_flux_at_soil_surface'][:,1]
oduf = ncv['dissolved_reduced_substances_upward_flux_at_soil_surface'][:,1]
nitf = ncv['mole_concentration_of_nitrate_upward_flux_at_soil_surface'][:,1]
ammf = ncv['mole_concentration_of_ammonium_upward_flux_at_soil_surface'][:,1]

pocf = ncv['fast_detritus_C_upward_flux_at_soil_surface'][:,1] + \
       ncv['slow_detritus_C_upward_flux_at_soil_surface'][:,1]

dh = ncv['layer_height_in_soil'][:,:,1,:]
denit = ncv['denitrification_rate_in_soil'][:,:,1,:]
por = ncv['porosity_in_soil'][:,:,1,:]

denitf = sum(dh*denit*por,axis=1)
o2f = oduf - oxyf

secs = ncv['time'][:]
days = secs/86400.

x = 60.5 - arange(60) # [km]

# define colors
cfine=(0.8,0.9,0.4)
ccoarse=(0.5,0.5,0.0)
lwfine=2.0
fs=16
xl=5

figure(figsize=(10,8))

n=0
ax=axes([0.12+n*0.22,0.12,0.2,0.65])
pc=pcolor(x,days,pocf.squeeze()*-86400./1000.*12.,cmap=djet)
text(xl,10,'POC flux',color='w',size=fs)
ylim(0,365)
xlim(0,60)
xlabel('distance from coast [km]')
ylabel('day of year')

cax=axes([0.12+n*0.22,0.9,0.2,0.01])
cb=colorbar(cax=cax,orientation='horizontal')
cb.set_label(u'POC flux [gC/m\u00b2/d]')
tlabels=[xtl.get_text() for xtl in cax.get_xticklabels()]
cax.set_xticklabels(tlabels,rotation=90.)

n=1
ax=axes([0.12+n*0.22,0.12,0.2,0.65])
pcolor(x,days,(nitf+ammf).squeeze()*86400.,cmap=djet)
text(xl,10,'DIN flux',color='w',size=fs)
ylim(0,365)
xlim(0,60)
ax.set_yticklabels([])

cax=axes([0.12+n*0.22,0.9,0.2,0.01])
cb=colorbar(cax=cax,orientation='horizontal')
cb.set_label(u'DIN flux [mmolN/m\u00b2/d]')
tlabels=[xtl.get_text() for xtl in cax.get_xticklabels()]
cax.set_xticklabels(tlabels,rotation=90.)

n=2
ax=axes([0.12+n*0.22,0.12,0.2,0.65])
pcolor(x,days,denitf.squeeze(),cmap=djet)
text(xl,10,'denitrification',color='w',size=fs)
ylim(0,365)
xlim(0,60)
ax.set_yticklabels([])

cax=axes([0.12+n*0.22,0.9,0.2,0.01])
cb=colorbar(cax=cax,orientation='horizontal')
cb.set_label(u'denitrification [mmolN/m\u00b2/d]')
tlabels=[xtl.get_text() for xtl in cax.get_xticklabels()]
cax.set_xticklabels(tlabels,rotation=90.)

n=3
ax=axes([0.12+n*0.22,0.12,0.2,0.65])
pcolor(x,days,o2f.squeeze()*86400.,cmap=djet)
text(xl,10,'oxygen\nconsumption',color='w',size=fs)
ylim(0,365)
xlim(0,60)
ax.set_yticklabels([])

cax=axes([0.12+n*0.22,0.9,0.2,0.01])
cb=colorbar(cax=cax,orientation='horizontal')
cb.set_label(u'oxygen\nconsumption\n[mmolO2/m\u00b2/d]')
tlabels=[xtl.get_text() for xtl in cax.get_xticklabels()]
cax.set_xticklabels(tlabels,rotation=90.)

nc.close()

savefig('%s_sedimentfluxes.png'%ncfile[:-3],dpi=300)
show()

