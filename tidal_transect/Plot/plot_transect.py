import matplotlib
#matplotlib.use('Agg')
matplotlib.use('GTKAgg')
from pylab import *
import netCDF4
import netcdftime
import sys
import os
import numpy as np

screen=False

if len(sys.argv)>1:
    ncfile=sys.argv[1]
else:
    ncfile='mossco_gffn.nc'

if len(sys.argv)>2:
    tsplit=sys.argv[2].split(',')
    tmin=int(tsplit[0])
    if len(tsplit)==2:
      tmax=int(tsplit[1])
    else:
      tmax=tmin
else:
    tmin=186
    tmax=tmin

nc=netCDF4.MFDataset(ncfile)
ncv=nc.variables

xnum=len(nc.dimensions['getmGrid3D_getm_1'])-2
znum=len(nc.dimensions['getmGrid3D_getm_3'])
xs = slice(2,None)
ys = 1
szs = slice(0,10) # use the upper 10 layers

sz = squeeze(ncv['layer_center_depth_in_soil'][:,szs,ys,xs]) # [m]
sznum = sz.shape[1]
depth = squeeze(ncv['water_depth_at_soil_surface'][:,ys,xs]) # [m]
tv = ncv['time']
tnum=len(nc.dimensions['time'])

# calculate layer center depths
pz = zeros((tnum,znum+2,xnum))
for t in range(tnum):
  for x in range(xnum):
    pz[t,0,x] = -depth[t,x]
    pz[t,1:znum-1,x] = float(depth[t,x])/znum
    pz[t,1:znum-1,x] = (cumsum(pz[t,1:znum-1,x]) - 0.5*pz[t,1:znum-1,x]) - depth[t,x]
    pz[t,-1,x] = -depth[t,x] + depth[t,x]
pz = ma.masked_where(isnan(pz),pz)

xc = ncv['getmGrid3D_getm_x'][xs]/1000. # [km]
px = meshgrid(xc,arange(znum+2))[0]
sx = meshgrid(xc,arange(sznum+2))[0]

# rescale soil zax grid
szplot1=sz.copy()
zoff=2.0 #m
scaledepth=5. #m - water depth, that appears of similar plot height as the sediment
scale = scaledepth/sz[1,-1,1]
for k in range(sznum):
  szplot1[:,k,:] = squeeze(-depth[:,:]) - zoff - scale * sz[:,k,:]
  szplot1.mask[:,k,:] = depth.mask

# stack pseudo-data for plotting (already done for x-coordinate)
szplot = zeros((tnum,sznum+2,xnum))
szplot[:,0,:]= szplot1[:,0,:].filled(-10.)
szplot[:,1:-1,:]=szplot1.filled(-10.)
szplot[:,-1,:]=szplot1[:,-1,:].filled(-10.)

utime=netcdftime.utime(tv.units)
time = utime.num2date(tv[:])


pcmap=cm.jet
pcmin=20
pcmax=25.0
pelagtitle=u'nutrients\n[mmolN/m\u00b3]\n'
pvarn='nutrients_in_water'

pcmap=cm.jet
pcmin=0
pcmax=3.0
pelagtitle=u'detritus\n[mmolN/m\u00b3]\n'
pvarn='detritus_in_water'

scmap=cm.jet
scmin=0.0
scmax=25.0
soiltitle=u'pore water nitrate\n[mmolN/m\u00b3]\n'
svarn='mole_concentration_of_nitrate_in_soil'

scmap=cm.YlGnBu
scmin=0.0
scmax=50.0
soiltitle=u'denitrification rate\n[mmolN/m\u00b3/d]\n'
svarn='denitrification_rate_in_soil'

dpi=300
fv=-999.
xorig=xc[0]
scoff=-4. # km

for t in range(tmin,tmax+1):
  if True:
      figure(figsize=(10,5))
      ax=axes([0.0,0.0,1.0,1.0])

      pdata = squeeze(ncv[pvarn][t,:,ys,xs])
      pdata = np.vstack((pdata[0,:].filled(fv),pdata.filled(fv),pdata[-1,:].filled(fv)))
      pdata = ma.masked_where(pdata==fv,pdata)

      pelag=ax.contourf(px,pz[t].filled(-10.),pdata,cmap=pcmap)
      #pelag.set_clim(pcmin,pcmax)

      pcax=axes([0.8,0.05,0.02,0.3])
      #colorbar(pelag,cax=pcax,ticks=linspace(pcmin,pcmax,6))
      colorbar(pelag,cax=pcax)
      pcax.set_title(unicode(pelagtitle),size=8.0)

      sdata = squeeze(ncv[svarn][t,szs,ys,xs])
      sdata = np.vstack((sdata[0,:].filled(fv),sdata.filled(fv),sdata[-1,:].filled(fv)))
      sdata = ma.masked_where(sdata==fv,sdata)

      soil=ax.contourf(sx,szplot[t],sdata,cmap=scmap)
      #soil.set_clim(scmin,scmax)
      scax=axes([0.9,0.05,0.02,0.3])
      #colorbar(soil,cax=scax,ticks=linspace(scmin,scmax,6))
      colorbar(soil,cax=scax)
      scax.set_title(unicode(soiltitle),size=8.0)

      ax.set_xlim(xorig+scoff*2,60.)
      ax.set_ylim(-30,3.0)
      ax.axis('off')

      if True:
        # ocean scale
        ax.plot([xorig+scoff,xorig+scoff],[-2.,-12.],'b-',lw=3.0)
        ax.text(xorig+scoff-1.0,-7,'ocean\n10 m',color='b',size=9.,horizontalalignment='right',verticalalignment='center',rotation=90.)

      if True:
        # sediment scale
        scaleddepth=scale*0.04
        ax.plot([xorig+scoff,xorig+scoff],[-21.,-21.-scaleddepth],color='brown',ls='-',lw=3.0)
        ax.text(xorig+scoff-1.0,-21-0.5*scaleddepth,'sediment\n4 cm',size=9.,color='brown',horizontalalignment='right',verticalalignment='center',rotation=90.)

      if True:
        # horizontal scale
        ax.plot([15.0,35.0],[-27.,-27.],color=(0.4,0.4,0.4),ls='-',lw=3.0)
        ax.text(25.0,-27+1,'along-transect 20 km',color=(0.4,0.4,0.4),size=9.,horizontalalignment='center',verticalalignment='bottom')

      ax.text(xorig,1.5,str(time[t]),size=14.)

savefig('transect_%04d.pdf'%t,dpi=300)
show()
