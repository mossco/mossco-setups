from pylab import *
import netCDF4
import sys

if len(sys.argv)>1:
  ncfile = sys.argv[1]
else:
  ncfile = 'mossco_gffe.nc'
if len(sys.argv)>2:
  idx = int(sys.argv[2])
else:
  idx = 40

nc=netCDF4.Dataset(ncfile)
ncv=nc.variables

stress = ncv['shear_stress_at_soil_surface'][:,1,idx]
u = ncv['x_velocity_in_water'][:,0,1,idx]
v = ncv['y_velocity_in_water'][:,0,1,idx]
erosion1 = ncv['concentration_of_SPM_upward_flux_at_soil_surface_001'][:,1,idx]
spm1 = ncv['concentration_of_SPM_in_water_001'][:,0,1,idx]
erosion2 = ncv['concentration_of_SPM_upward_flux_at_soil_surface_002'][:,1,idx]
spm2 = ncv['concentration_of_SPM_in_water_002'][:,0,1,idx]

secs = ncv['time'][:]
days = secs/86400.

# define colors
cfine=(0.8,0.9,0.4)
ccoarse=(0.5,0.5,0.0)
lwfine=2.0

figure(figsize=(8,8))

subplot(411)
plot(days,squeeze(spm2),'-',color=ccoarse,label='coarse')
plot(days,squeeze(spm1),'-',color=cfine,label='fine',lw=lwfine)
plot(days, squeeze(spm1+spm2),'k-',lw=1.0,label='TSM')
ylabel('near-bottom\nSPM [mg/l]')
legend(loc='upper left',frameon=False)

subplot(412)
plot(days,erosion1,'-',color=cfine,lw=lwfine)
plot(days,erosion2,'-',color=ccoarse)
plot([0,9],[0.0,0.0],'k--',lw=0.3)
ylabel('erosion [g/m2/s]')

subplot(413)
plot(days,stress,'k-')
ylabel('bottom stress [Pa]')

subplot(414)
vel=sqrt(u**2+v**2)
plot(days,squeeze(vel),'k-')
ylabel('near-bottom vel [m/s]')
xlabel('days')

nc.close()

show()

