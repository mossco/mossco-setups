#!/bin/bash

paramval=$1
parampath=$2
unmodpath=$3
paramvar=$4
paramvar1=$5
#paramval1=$6
paramrange1=$6
paramvar2="dens_lpm" #$7
paramrange2=$8

#currrun=$(echo $(date) $paramvar $paramval |sha512sum|cut -d ' ' -f1) #
for i in $(seq 250 1 270); do #260 1 260  #coagulation
	for j in $(seq 2000 100 3000); do  #dens_lpm
		currrun=$paramvar$paramval$paramvar1$i$paramvar2$j
		pathn=$paramvar$paramval"_"$paramvar1$i"_"$paramvar2$j"_"$currrun
		echo $pathn
		echo $paramvar $paramval $paramvar1 $i $paramvar2 $j $parampath $umodpath $currrun 
			mkdir $parampath$currrun
			cd  $parampath$currrun
			cp  $unmodpath"../build3/gotm" .   #changed path for gotm.exe
			echo "copying gotm"  >> $(echo $parampath$currrun".log")
		#cp $unmodpath"airsea.nml" .
		#cp $unmodpath"fabm_pelagic.nml" .
		#cp $unmodpath"gotm_fabm.nml" .
		#cp $unmodpath"gotmmean.nml" .
		#cp $unmodpath"gotmrun.nml" .
		#cp $unmodpath"gotmturb.nml" .
		#cp $unmodpath"obs.nml" .
		#cp $unmodpath"gotm_meteo.dat" .
			cp $unmodpath"fabm.yaml" .
			cp $unmodpath"output.yaml" .
                	cp $unmodpath"gotm.yaml" .

			echo "copying namelists"  >> $(echo $parampath$currrun".log")

			sed -i '/!/d' fabm.yaml 
	  		echo $paramval
	  		echo "modifing agg for variable "$paramvar" with value "$paramval" and "$paramvar1" with value "$i" " and "$paramvar2" with value "$j"  >> $(echo $parampath$currrun".log")
          
			sed -i '/'$paramvar'/ c\''      '$paramvar': '$paramval fabm.yaml #'      '
			sed -i '/'$paramvar1'/ c\''      '$paramvar1': '$i fabm.yaml
			sed -i '/'$paramvar2'/ c\''      '$paramvar2': '$j fabm.yaml #added
	  		echo "starting fabm with "$paramvar" = "$paramval  >> $(echo $parampath$currrun".log")
	  		./gotm &>> $(echo $parampath$currrun"/"$paramvar$paramval$paramvar1$i$paramvar2$j"run.log") 
	  		echo "stopping fabm"  >> $(echo $parampath$currrun".log") 
	  #mv output.nc $paramvar$paramval"out.nc"
	  		echo "renameing output"  >> $(echo $parampath$currrun".log")
	  #ncpdq -O -d time,,,5 -a z,time bale2002.nc test.nc
	  	#ncap2 -A -s "totlpm$paramval=spm_spm+agg_agglpm" -v bale2002.nc ../test.nc
	  	#ncap2 -A -s "esd$paramval=agg_esd" -v bale2002.nc ../test.nc
		done
	  done

wait
	
