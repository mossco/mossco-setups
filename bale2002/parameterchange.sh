#!/bin/bash
cd /home/enpei/mossco/setups/bale2002 
LANG=en_US
	
#paramvar="agglpm"
#paramrange=$(seq 0.001 10 60)

#paramvar="onoff"
#paramrange=$(seq 0 1 1)

#paramvar="Dsize"
#paramrange=$(seq 3e-5 5e-6 4e-5)

paramvar="breakup_factor" 
paramrange=$(seq 180000 10000 300000) #60000 40000 180000 220000 40000 220000  14000 6000 20000 122000 50000 222000# #1500000 #1500  #1400 $(seq 13000 1 13000) #$(seq 13000 0.1 13000.1) #(seq 50 100 300) $(seq 11500 100 12000) 

paramvar1="coagulation_rate"
paramrange1=$(seq 250 1 270) #260 1 260 110 50 160 (seq 14 1 14)  #1300 #14, 5  #40 #|tr "," ".")   #needs to be changed manually in parameterfun.sh

paramvar2="dens_lpm"
paramrange2=$(seq 2000 100 3000) #1600 200 2600

#paramvar="tauc_const" 
#paramrange=$(seq 0.5 0.5 1.5)

#paramvar="fractal_dimension"
#paramrange=$(seq 1.6 0.2 2.4) #1.96 0.04 2.04 1.996 0.004 2.004



#paramvar="agg_porosity"
#paramrange=$(seq 0.96 0.005 0.98) #|tr "," ".")

#paramvar="size_method"
#paramrange=$(seq 8 1 9) #|tr "," ".") [1,2,4,6,7]

parampath="/home/enpei/mossco/setups/bale2002/paramstudy/"
unmodpath="/home/enpei/mossco/setups/bale2002/" 
echo "preparing starting parameter variation experiment for "$paramvar" and "$paramvar1" and "$paramvar2" at "$(date) 
rm -rf $parampath
mkdir $parampath
cd $parampath
echo "preparing starting parameter variation experiment for "$paramvar" and "$paramvar1" and "$paramvar2" at "$(date)  >> $(echo $parampath$paramvar".log")

#ls -lh $parampath
echo ${paramrange}
echo "${paramrange}" |parallel --jobs 0 --progress /home/enpei/mossco/setups/bale2002/parameterfun.sh {1} $parampath $unmodpath $paramvar $paramvar1 $paramvar2 ::::-
sem --wait
wait
echo "done"  >> $(echo $parampath$currrun".log")
ls $parampath

python ../Plot/parameter.py "${paramvar}" "${paramrange}" "${paramvar1}" "${paramrange1}" "${paramrange2}" "${paramvar2}"

#cd $parampath$currrun
#rm *.nc
#cd ../
#less $parampath$currrun".log" 

