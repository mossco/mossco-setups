# shallow_lake setup

This simulates a 3 m  deep rectangular lake.

## MOSSCO setups to run here

## Input files

filename | component | purpose
---------|-----------|---------
topo.nc  | getm      | bathymetry data
getm.inp | getm | setup parameters
parallel.inp | getm | parallelization control parameters
gotmturb.nml | getm,gotm | turbulence model parameters
tprof.dat | getm | temperature profile data
fabm.nml | fabm_pelgaic | fabm model parameters
fabm_pelagic.nml | fabm_pelagic | component runtime parameters
fabm_sed.nml | fabm_sediment  | fabm model parameters
run_sed.nml | fabm_sediment | component runtime parameters
benthic_pelagic_coupler.nml | benthic_pelagic_coupler | coupler runtime parameters
benthic.nml | erosed | component runtime parameters
mbalthica.nml | benthos | macrofauna parameters
microphyt.nml | benthos | mirophytobenthos parameters
constant.dat | constant | constants to be exported


