# -*- coding: utf-8 -*-
"""
# This script is is part of MOSSCO. It creates from basic grid parameters
# a GRIDSPEC/CF compliant NetCDF file.
#
# @copyright (C) 2018 Helmholtz-Zentrum Geesthacht
# @author Carsten Lemmen <carsten.lemmen@hzg.de>
# @author Richard Hofmeister <richard.hofmeister@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#
"""

import netCDF4
import numpy as np
import sys
import time

# Give boundaries (not really needed, but fully implemented)
#

tnum = 12     # monthly climatology
ll_lon = 0.   # lower left of cell corner
ll_lat = 0.
ur_lon = 7.  # upper right of cell corner
ur_lat = 7.
delta_lon = 1 # grid resolution delta lon in decimal degree
delta_lat = 1

if __name__ == '__main__':

  nlat=np.abs(ur_lat-ll_lat)/np.abs(delta_lat)
  nlon=np.abs(ur_lon-ll_lon)/np.abs(delta_lon)

  # ensure that nlat and nlot are whole numbers and adust delta
  if (nlat != np.round(nlat)):
    delta_lat=np.abs(ur_lat-ll_lat)/np.round(nlat)
  nlat=int(np.round(nlat))

  if (nlon != np.round(nlon)):
    delta_lon=np.abs(ur_lon-ll_lon)/np.round(nlon)
  nlon=int(np.round(nlon))

  if len(sys.argv)>1:
    basename=sys.argv[1]
  else:
    basename = 'matrix_forcing.nc'

  nc=netCDF4.Dataset(basename,'w')

  nc.createDimension('time',tnum)
  nc.createDimension('lon',nlon)
  nc.createDimension('lat',nlat)
  nc.createDimension('bound',2)

  lon = nc.createVariable('lon','f8',('lon'))
  lon.bounds='lon_bnds'
  lon.units='degree_east'
  lon.long_name='longitude'
  lon.standard_name='longitude'

  lon_bnds = nc.createVariable('lon_bnds','f8',('lon','bound'))

  lat = nc.createVariable('lat','f8',('lat'))
  lat.bounds='lat_bnds'
  lat.units='degree_north'
  lat.long_name='latitude'
  lat.standard_name='latitude'

  lat_bnds = nc.createVariable('lat_bnds','f8',('lat','bound'))

  t = nc.createVariable('time','f8',('time',))
  t.units = 'seconds since 2003-01-01 00:00:00'

  # Meta data
  nc.history = 'Created ' + time.ctime(time.time()) + ' by ' + sys.argv[0]
  nc.creator = 'Carsten Lemmen <carsten.lemmen@hzg.de>'
  nc.license = 'Creative Commons share-alike (CCSA)'
  nc.copyright = 'Helmholtz-Zentrum Geesthacht'
  nc.Conventions = 'CF-1.7'

  # Create some variables, only consider time dependency here

  vars = {}
  vars['mole_concentration_of_ammonium'] = 3.0*np.ones((tnum,))
  vars['mole_concentration_of_nitrate'] = 13.0*np.ones((tnum,))
  vars['mole_concentration_of_phosphate'] = 1.0*np.ones((tnum,))
  vars['dissolved_oxygen'] = 250.0*np.ones((tnum,))
  vars['dissolved_reduced_substances'] = 0.0*np.ones((tnum,))

  vars['detritus_phosphorus'] = 10.0/16.0/86400.*np.asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2])
  vars['detritus_labile_carbon'] = 10.0/86400.*np.asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2])
  vars['detritus_semilabile_carbon'] = 10.0/86400.*np.asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2])

  vars['detritus_labile_carbon_z_velocity'] = -1.0*np.ones((tnum,))
  vars['detritus_semilabile_carbon_z_velocity'] = -1.0*np.ones((tnum,))
  vars['detritus_phosphorus_z_velocity'] = -1.0*np.ones((tnum,))

  # Physical boundary conditions (par might need different values)

  vars['photosynthetically_active_radiation'] = np.asarray([5.0,10.0,20.0,50.0,100.0,150.0,150.0,100.0,50.0,20.0,10.,5.0])
  vars['temperature'] = np.asarray([5.0,3.0,5.0,7.0,10.0,14.0,17.0,19.0,17.0,14.0,10.,7.0])



  for varname,data in vars.iteritems():
    vv = nc.createVariable(varname+'_at_soil_surface','f4',('time','lon','lat'))
    vv.long_name = varname
    vv.missing_value = -1.0E30
    vv[:] = vv.missing_value
    for i in range(0,nlon):
      for j in range(0,nlat):
          vv[:,i,j] = data[:]
    nc.sync()

  # Now perturb variables, e.g. systematic carbon variation
  for i in range(0,nlon):
      nc.variables['detritus_semilabile_carbon_at_soil_surface'][:,i,:] = (i*2.0)/nlon * nc.variables['detritus_semilabile_carbon_at_soil_surface'][:,i,:]
  for j in range(0,nlat):
      nc.variables['detritus_labile_carbon_at_soil_surface'][:,:,j] = (j*2.0)/nlat * nc.variables['detritus_labile_carbon_at_soil_surface'][:,:,j]

  d = np.asarray([31,28,31,30,31,30,31,31,30,31,30,31])
  t[:] = 86400.*(d.cumsum()-1.0*d)

  ilon=np.array(range(0,nlon))
  jlat=np.array(range(0,nlat))

  lon[:]=ll_lon+(ilon+0.5)*delta_lon
  lat[:]=ll_lat+(jlat+0.5)*delta_lat
  lon_bnds[:,0]=lon[:]-0.5*delta_lon
  lon_bnds[:,1]=lon[:]+0.5*delta_lon
  lat_bnds[:,0]=lat[:]-0.5*delta_lat
  lat_bnds[:,1]=lat[:]+0.5*delta_lat

  nc.close()
