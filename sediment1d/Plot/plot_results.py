import netCDF4
from pylab import *

nc = netCDF4.Dataset('sediment1d.nc')
ncv = nc.variables

z = ncv['layer_center_depth_in_soil'][:]
zz = -z[0].squeeze()*100.
dz = ncv['layer_height_in_soil'][:]
por = ncv['porosity_in_soil'][:]
days = ncv['time'][:]/86400.

f = figure(figsize=(10,10))

xoff=0.15
dxleft=0.5
xoff_right=xoff+dxleft+0.05
dxright=0.2
dyprof=0.27
ocol=(0.2,0.8,0.3)
ncol=(0.9,0.1,0.3)
ymin=-4.0

# plot boundary conditions
poc_flux = (ncv['detritus_semilabile_carbon_upward_flux_at_soil_surface'][:] + \
           ncv['detritus_labile_carbon_upward_flux_at_soil_surface'][:]).squeeze()
#nit_flux = ncv['mole_concentration_of_nitrate_upward_flux_at_soil_surface'][:]
#amm_flux = ncv['mole_concentration_of_ammonium_upward_flux_at_soil_surface'][:]
oxy_flux = ncv['dissolved_oxygen_upward_flux_at_soil_surface'][:].squeeze()

ax = axes([xoff,0.1+2*dyprof+0.1,dxleft,0.2])
plot(days,-poc_flux*86400./12.,'k-',lw=2.0,label=u'POC flux [mgC/m\u00b2/d]')
plot(days,-oxy_flux*86400.,'-',color=ocol,lw=2.0,label=u'O2 flux [mmolO2/m\u00b2/d]')
legend(loc='upper right',frameon=False,fontsize=12.,bbox_to_anchor=(0.95,0.6))
ax.set_xticklabels([])
ax.grid(linestyle='--',lw=0.2)

# plot denitrification rate
denit = (ncv['denitrification_rate_in_soil'][:]*por).squeeze()
ax = axes([xoff,0.1+dyprof+0.05,dxleft,dyprof])
ct = contourf(days,zz,denit.T,10,cmap=cm.YlOrBr)
#clim(0.0,1.0)
ylim(ymin,0.0)
ax.set_xticklabels([])
ax.grid(linestyle='--',lw=0.2)
ylabel('z [cm]')

cax = axes([xoff+0.05,0.1+dyprof+0.05+0.07,0.25,0.02])
colorbar(ct,cax=cax,orientation='horizontal',label=u'denitrification [mmolN/m\u00b3/s]')

# plot phosphate
pho = ncv['mole_concentration_of_phosphate_in_soil'][:].squeeze()
ax = axes([xoff,0.1,dxleft,dyprof])
ct = contourf(days,zz,pho.T,10,cmap=cm.RdYlGn_r)
clim(0.0,150.0)
ylim(ymin,0.0)
ax.grid(linestyle='--',lw=0.2)
xlabel('days')
ylabel('z [cm]')

cax = axes([xoff_right,0.1,0.02,dyprof])
colorbar(ct,cax=cax,label=u'phosphate [mmolP/m\u00b3]')

# plot average oxygen, nitrate
oxy = ncv['dissolved_oxygen_in_soil'][:].squeeze()
nit = ncv['mole_concentration_of_nitrate_in_soil'][:].squeeze()
ax = axes([xoff_right,0.1+dyprof+0.05,dxright,dyprof])
plot(oxy.mean(axis=0),zz,'-',color=ocol,lw=2.0,label=u'oxygen\n[mmol/m\u00b3]')
plot(5*nit.mean(axis=0),zz,'-',color=ncol,lw=2.0,label=u'nitrate\n[0.2 mmol/m\u00b3]')
ylim(ymin,0.0)
ax.set_yticklabels([])
ax.grid(linestyle='--',lw=0.2)
legend(loc='lower right',fontsize=12.0,frameon=False)

# plot average PON

# plot denit vs resp
int_denit = (denit*dz.squeeze()).sum(axis=1)
ax=axes([xoff_right,0.1+2*dyprof+0.1,dxright,0.2])
ax.yaxis.tick_right()
ax.xaxis.tick_top()
scatter(-oxy_flux*86400.,int_denit,s=10.0,c='k',alpha=0.3)
ax.grid(linestyle='--',lw=0.2)
xlabel(u'respiration [mmolO/m\u00b2/d]')
ylabel(u'denitrification [mmolN/m\u00b2/d]')


show()
