!--------------
! run_nml
!
! numyears - number of years (y)
! dt - timestep (s)
! output - number of timesteps (#)
! numlayers - number of sediment layers (#)
! bcup_dissolved_variables - type of boundary condition
!                            for dissolved variables:
!                            1: fluxes given at boundary
!                            2: concentrations given ar boundary
! presimulation_years - number of years to pre-simulate (y)
!--------------
&run_nml
  numyears=4
  dt=3600.
  numlayers=24
  dzmin=0.004
  dt_min=10.
  ode_method=2
  bcup_dissolved_variables=2
  presimulation_years=5
  pel_NO3=12.0
  pel_NH4=3.0
  pel_PO4=1.0
  pel_O2=250.0
  pflux_lDetC=10.0
  pflux_sDetC=10.0
  pflux_lDetN=1.5
  pflux_sDetN=1.5
  pflux_lDetP=0.2
  pel_Temp=5.0
/

! bioturbation profile:
!   0 - constant
!   1 - linear decay
!      (bioturbation to bioturbation_min at depth bioturbation_depth)
!   2 - exponential decay
!   3 - dynamic bioturbation depending on TOC profiles

&sed_nml
  diffusivity   = 2.9
  bioturbation  = 2.5
  bioturbation_profile = 2
  bioturbation_depth=6.0
  bioturbation_min=0.1
  bioturb_k_l = 0.11
  bioturb_beta = 0.22
  bioturb_b = 1.334
  bioturb_L1 = 0.2
  bioturb_L2 = 0.6
  porosity_max  = 0.7
  porosity_fac  = 0.9
  k_par         = 0.002
/
