This folder contains a soil column setup for simulation of the soil with imposed
boundary conditions.

There is a 1D coupling configuration `sediment_io.yaml`  and a multidimensinoal one
called `matrix_sediment.yaml`.   

# 1D column

To create boundary conditions, consult the subfolder `Bdy` and the python script therein

# Multi-column

To create boundary conditions for the matrix, run the matrix_create.py script, then 
run the model.  Feel free to use or create (by manipulating the python script or the
file `matrix_forcing.nc`) different forcings.

	python matrix_create.py
	mossco matrix_sediment
