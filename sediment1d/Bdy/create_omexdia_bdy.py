import netCDF4
import sys
from numpy import asarray,arange,ones
from datetime import datetime,timedelta

if len(sys.argv)>1:
  print "usage: create_omexdia_bdy.py"
  sys.exit(1)

nc = netCDF4.Dataset('omexdia_boundary.nc','w',format='NETCDF3_CLASSIC')

tnum = 12
ynum = 1
xnum = 1
mv = -1.e30

nc.createDimension('time',tnum)
nc.createDimension('x',xnum)
nc.createDimension('y',ynum)

t = nc.createVariable('time','f8',('time',))
t.units = 'seconds since 2003-01-01 00:00:00'
d = asarray([31,28,31,30,31,30,31,31,30,31,30,31])
t[:] = 86400.*(d.cumsum()-1.0*d)

vars = {}
vars['mole_concentration_of_ammonium'] = 3.0*ones((tnum,))
vars['mole_concentration_of_nitrate'] = 13.0*ones((tnum,))
vars['mole_concentration_of_phosphate'] = 1.0*ones((tnum,))
vars['dissolved_oxygen'] = 250.0*ones((tnum,))
vars['dissolved_reduced_substances'] = 0.0*ones((tnum,))
vars['detritus_phosphorus'] = 10.0/16.0/86400.*asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2]) 
vars['detritus_phosphorus_z_velocity'] = -1.0*ones((tnum,))
vars['detritus_labile_carbon'] = 10.0/86400.*asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2])
vars['detritus_labile_carbon_z_velocity'] = -1.0*ones((tnum,))
vars['detritus_semilabile_carbon'] = 10.0/86400.*asarray([0.2,0.2,1.0,2.2,3.0,2.5,2.0,2.0,1.0,0.5,0.2,0.2])
vars['detritus_semilabile_carbon_z_velocity'] = -1.0*ones((tnum,))
vars['photosynthetically_active_radiation'] = 50.0*ones((tnum,))
vars['temperature'] = asarray([5.0,3.0,5.0,7.0,10.0,14.0,17.0,19.0,17.0,14.0,10.,7.0])


for varname,data in vars.iteritems():
  vv = nc.createVariable(varname+'_at_soil_surface','f4',('time','y','x'))
  vv.long_name = varname
  vv.missing_value = mv
  vv[:] = mv
  vv[:,0,0] = data
  nc.sync()

nc.close()
