# Setup: sediment

This setup is made for the simulation of marine sediments. So far, the namelists and forcing are made for 1d simulation with the omexdia_p model in FABM.

## examples to run here:

- 'esmf/sediment' running the fabm_sediment_component without any external forcing or output
- 'constant_fabmsed_netcdf' running the fabm_sediment_component with constant forcing at the soil surface and netcdf-output every 24h

## postprocessing of constant_fabmsed_netcdf:

The example usually writes out the file mosscosediment.000.nc. If you have the python package netCDF4 installed, you could simply use

	python Plot/plotsed1d.py mosscosediment.000.nc

for plotting. Otherwise, the netcdf file contains variables for the sediment state and the forcing. The state variables and diagnostic variables of the sediment component have the suffix '_in_soil', the boundary conditions have the suffix 'at_soil_surface', and the exchange fluxes have the suffix '_upward_flux'. You would always find 'temperature_in_soil' and 'porosity_in_soil' in the output file. As coordinate variables, the sediment component writes out 'layer_center_depth_in_soil' (the depth in meters below soil surface, this is used to plot vertical profiles), and 'layer_height_in_soil' (layer height or thickness in meters, needed to calculate bulk values or profile averages).
