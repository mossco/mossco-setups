from pylab import *
import netCDF4
from netcdftime import utime
import sys
#fh=open('t_prof_file.dat','a')

if (len(sys.argv)>2):
  ncfile=sys.argv[1]
  outfile=sys.argv[2]
else:
  ncfile='temperature.nc'
  outfile='t_profile_2000-2010.dat'

nc=netCDF4.Dataset(ncfile)
ncv=nc.variables

ut = utime(ncv['time'].units)
t  = ut.num2date(ncv['time'][:])
d  = ncv['depth'][:]
temp = ncv[ncfile.split('.')[0]][:]

curt = t[0]
tstart = 0
tnum = len(t)

profs=[]

while tstart<tnum-1:
  curt = t[tstart]
  tend = max(where(t == curt)[0])
  cuts = where(diff(d[tstart:tend+1])<0)[0]
  if len(cuts) == 0:
    ttstart=tstart
    ttend = tend+1
  else:
    cuts=list(cuts+1)
    cuts.insert(0,0)
    cuts.append(tend+1-tstart)
    cuts=asarray(cuts)
    dcuts = diff(cuts)
    maxidxs = where(dcuts == dcuts.max())[0]
    if len(maxidxs)>1:
      maxstart = int(maxidxs[0])
    else:
      maxstart = int(maxidxs)
    ttstart = tstart + cuts[maxstart]
    ttend = tstart + cuts[maxstart+1]
  if ttend-ttstart > 5:
    dmin=d[ttstart:ttend].min()
    dmax=d[ttstart:ttend].max()
    if dmax-dmin > 220.:
      print('%s: max = %0.1f, min = %0.1f'%(str(t[ttstart]),dmax,dmin))
      print(d[ttstart:ttend])
      profs.append([curt,ttstart,ttend])
  tstart = tend + 1

profs.sort()

fh = open(outfile,'w')
for curt,tstart,tend in profs:
  fh.write('%s %d 2\n'%(str(curt),tend-tstart))
  for ii in range(tstart,tend):
    fh.write(' %6.1f  %0.8f\n'%(-d[ii],temp[ii]))
fh.close()

