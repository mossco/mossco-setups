This directory contains the setup for the German Bight 300 m setup
that can be run with difficult MOSSCO-coupled applications.

GETM requires getm.inp which can be created by "make namelist_getm".
(existing getm.inp will be overwritten)

All forcing netcdf files must be obtained separately from a private repository (not public)
By issuing make input, you will be notified of missing files

Please adapt the run time needed in generated job scripts, or give the -Z option to the mossco command


If you want to stitch only a small part of the entire domain, we suggest

# For the 235p domain
stitch --out=elbe.nc --pet=12-14,22-23,30-34,43,55-56,69-73,86-91,103-105
stitch --out=elbeestuary.nc --pet=67-73,84-91,102-105,116-120,131-135
stitch --out=weser.nc --pet=3,6,9-11,17-21,26-29,39-42,50-54,63-68,80-85
stitch --out=helgoland.nc --pet=41-42,55-56

# For the 493p domain

stitch --out=weser.nc --pet=1,5,6,10,11,14,15,18-21,28-33,41-45,53-57,68-73,83-89,101-108,120-127,139-147
stitch --out=helgoham.nc --pet=28-35,41-49,53-63,68-75,83-91,99-110,118-129,
