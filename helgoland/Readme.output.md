Simulation output:
==================

PET0.Helgoland:
  logging information of the coupled setup

output.dat:
  results of the fabm_sediment_component

helgoland.nc:
  GOTM's netcdf output. Also containing output of the fabm_gotm component

fbp_exchange_state:
  output of the exchanged fields in the fabm_benthic_pelagic example configuration

netcdf_component.nc:
  output of the netcdf_component, as specified in your yaml configuration

benthic_geoecology.nc:
  output of the exchanged fields in the benthic_geoecology example configuration

delft_sediment_test.out:

