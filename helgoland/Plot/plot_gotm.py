import netCDF4 as nc4
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import re

if len(sys.argv)>1: 
    ncfile=sys.argv[1]
else:
    ncfile='../mossco_jfs.nc'
    
nc=nc4.Dataset(ncfile)
ncv=nc.variables

time=ncv['time'][:]/86400.

numyears=4

monthslen=[31,28,31,30,31,30,31,31,30,31,30,31]
monthsnames=['J','F','M','A','M','J','J','A','S','O','N','D']
ticks=np.cumsum([ i for i in [0]+(numyears-1)*monthslen+monthslen[:-1]])
tickl=[ '   '+s for s in numyears*monthsnames]

#varnames=['gotm_npzd_PAR','temp','salt','gotm_npzd_nut','gotm_npzd_phy','gotm_npzd_zoo','gotm_npzd_det']
if len(sys.argv)>2:
    varnames=sys.argv[2].split(',')
else:
    varnames=['nutrients_in_water']
numvar=len(varnames)

f=plt.figure(figsize=(6,10))
f.subplots_adjust(top=0.95,bottom=0.05,hspace=0.5)

for i,varn in enumerate(varnames):
   plt.subplot(numvar,1,i+1)

   if len(ncv[varnames[i]].shape)<4:
       dat=np.squeeze(ncv[varnames[i]][:])
   else:
       dat=np.squeeze(ncv[varnames[i]][:,0])
   plt.plot(time,dat,'r-')
   plt.title(ncv[varnames[i]].long_name+' [%s]'%ncv[varnames[i]].units,size=12.0)
   plt.xlim(0,1460)
   plt.gca().set_xticks(ticks)
   plt.gca().set_xticklabels([])

   ax  = plt.gca()
   yt  = ax.get_yticks()
   ytl = ax.get_yticklabels()
   ax.set_yticks([yt[0],yt[-1]])
   ax.set_yticklabels([str(yt[0]),str(yt[-1])])

   plt.gca().xaxis.grid(color='k',linestyle=':',linewidth=0.5)

plt.gca().set_xticklabels(tickl)
plt.xlabel('')

outfilename = re.sub('.nc','_gotm.pdf',os.path.basename(ncfile))

plt.savefig(outfilename)
#plt.show()

nc.close()
