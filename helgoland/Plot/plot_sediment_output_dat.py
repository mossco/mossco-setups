from pylab import *

def read_data(filename='output.dat'):
  f=open(filename,'r')

  fluxes=[]
  time=[]
  timedata=[]
  data=[]
  z=[]
  dz=[]
  data_col=[]
  z_col=[]
  dz_col=[]
  por=[]
  por_col=[]

  meta = f.readline()
  column_names = meta.split()
  varnames = [ s.split('(')[0].strip() for s in column_names ]
  varnames = [ s.replace('hzg_omexdia_p_','') for s in varnames]
  por_idx = varnames.index('porosity')
  denit_idx = varnames.index('denit')
  z_idx = varnames.index('depth')
  dz_idx = varnames.index('layer-height')

  for line in f.readlines():
    dat = line.split()
    if dat[1] == 'fluxes':
        time.append(float(dat[0])/86400./365.)
        if len(fluxes)>0:
            fluxes[-1][-1] = sum(asarray(dz_col)*asarray(denit))
        fluxes.append([ float(item)*86400. for item in dat[2:10]])
        fluxes[-1].append(0.0)
        denit=[]
        z.append(z_col)
        data.append(data_col)
        dz.append(dz_col)
        por.append(por_col)
        data_col=[]
        z_col=[]
        dz_col=[]
        por_col=[]
    else:
        dz_col.append(float(dat[dz_idx]))
        z_col.append(float(dat[z_idx]))
        por_col.append(float(dat[por_idx]))
        data_col.append([float(dd) for dd in dat[por_idx+1:]])
        denit.append(float(dat[denit_idx]))

  fluxes[-1][-1] = sum(asarray(dz_col)*asarray(denit)*asarray(por_col))
  z.append(z_col)
  data.append(data_col)
  dz.append(dz_col)
  por.append(por_col)
  denit_idx=denit_idx-por_idx-1

  fluxes=asarray(fluxes)
  time=asarray(time)
  z=asarray(z[1:])*100
  data=asarray(data[1:])
  dz=asarray(dz[1:])
  por=asarray(por[1:])

  fluxes[:,6] += -fluxes[:,7]
  fluxes[:,6] = fluxes[:,6]/10.

  f.close()
  return (varnames[por_idx+1:],time,z,dz,por,data,fluxes)

if __name__ == '__main__':

  varnames,time,z,dz,por,data,fluxes = read_data()

  fig=figure(figsize=(8,10))

  subplot(511)
  contourf(time,-z[0],data[:,:,varnames.index('po4')].T,levels=range(0,40,4));ylim(-4.0,-0.15); xlim(0,4)
  text(0.5,-3.7,u'phosphate [mmol-P/m\u00b3]',color='k'); colorbar()
  grid()

  subplot(512)
  contourf(time,-z[0],data[:,:,varnames.index('no3')].T);ylim(-4.0,-0.15); xlim(0,4)
  text(0.5,-3.7,u'nitrate [mmol-N/m\u00b3]',color='w'); colorbar()
  grid()

  subplot(513)
  contourf(time,-z[0],data[:,:,varnames.index('oxy')].T);ylim(-2.0,-0.15); xlim(0,4); text(0.5,-1.7,u'oxygen [mmol-O2/m\u00b3]',color='w'); colorbar()
  grid()

  subplot(514)
  contourf(time,-z[0],data[:,:,varnames.index('denit')].T*por.T);ylim(-4.0,-0.15); xlim(0,4); text(0.5,-3.7,u'denitrification [mmol-N/m\u00b3/d]',color='w'); colorbar()
  grid()
  bb=gca().get_position()

  ylabel('depth [cm]')
  xlabel('years')

  ax=axes([bb.xmin,0.05,bb.xmax-bb.xmin,bb.ymax-bb.ymin])
  plot(time,16*fluxes[:,3],'r-',lw=2.0,label='16* DIP flux')
  plot(time,fluxes[:,4]+fluxes[:,5],'k-',lw=2.0,label='DIN flux')
  plot(time,-16*fluxes[:,2],'r-',lw=1.0,label='-16* POP flux')
  legend(frameon=False)
  #ylim(-2,0.1)

  show()
