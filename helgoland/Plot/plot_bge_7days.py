import netCDF4
from pylab import *
import sys

# get directory with files
if len(sys.argv)>1:
  directory=sys.argv[1]
else:
  directory='.'

# open netcdf files
gotmfile = directory+'/helgoland.nc'
cfile = directory+'/benthic_geoecology.nc'

ncg = netCDF4.Dataset(gotmfile)
gnc=ncg.variables
ncc = netCDF4.Dataset(cfile)
cnc=ncc.variables

# get time data
gt=gnc['time'][:]/3600./24.
ct=cnc['time'][:]/3600./24.

# in GOTM output
#tsm = squeeze(gnc['iow_spm01_spm'][:,1,0,0] + gnc['iow_spm02_spm'][:,1,0,0])
#u_bott=sqrt(gnc['u'][:,1,0,0]**2+gnc['v'][:,1,0,0]**2)

# in netcdf_component output
tsm = squeeze(cnc['concentration_of_SPM_in_water_001'][:]) + squeeze(cnc['concentration_of_SPM_in_water_002'][:])
u_bott=squeeze(sqrt(cnc['x_velocity_in_water'][:,0]**2 + cnc['y_velocity_in_water'][:,0]**2))

flux1 = squeeze(cnc['concentration_of_SPM_upward_flux_at_soil_surface_001'][:])
flux2 = squeeze(cnc['concentration_of_SPM_upward_flux_at_soil_surface_002'][:])

# close netcdf files
ncg.close()
ncc.close()

# set plot boundaries in days
x0=2
x1=9

# set spm minimum and maximum
spmmin=4.5
spmmax=6

# start plotting
fig=figure(figsize=(10,6))

subplot(311)
plot(ct,u_bott,'b-',lw=2.0)
xlim(x0,x1)
title('current velocity [m/s]')
gca().set_xticklabels([])
grid('on',axis='x')

subplot(312)
tsm[where(tsm>spmmax)]=spmmax
tsm[where(tsm<spmmin)]=spmmin
cnt=contourf(ct,arange(12,0,-1)/12.*-25,tsm.T,15, \
             cmap=cm.YlOrBr, \
#             norm=matplotlib.colors.Normalize(vmin=spmmin,vmax=spmmax,clip=True), \
             extend='neither')
xlim(x0,x1)
title('SPM in watercolumn [mg/l]')
ylabel('z [m]')
grid('on',axis='x')
gca().set_xticklabels([])

cax=axes([0.6,0.55,0.2,0.02])
colorbar(cnt,cax=cax,orientation='horizontal', ticks=linspace(spmmin,spmmax,4))
xt=cax.get_xticks()
cax.set_xlim(xt.min(),xt.max())

subplot(313)
plot(ct,flux2+flux1,'k',lw=2.0, label='total flux')
#plot(ct,flux2,'r--',lw=1.0,label='flux class 2')
xlim(x0,x1)
#ylim(-1.0,1.0)
#legend(frameon=False)
title(u'spm upward flux [g/m\u00b2]')
xlabel('time [d]')
grid('on',axis='x')

savefig(directory+'/spm_vel_flux.pdf')
show()
