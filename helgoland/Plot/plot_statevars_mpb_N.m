
%close all;
clear all;

saveFig = (0==0);
%saveFig = (0==1);

showPAR = (0==0);
%showPAR = (0==1);

resDir = '../.';
%ncfile = 'helgoland_maecs+mpb_CNP.nc';
ncfile = 'mossco_jfs.nc';

titleTXT = 'statevariables_mpb-N';
figname = sprintf('%s_%s',ncfile(1:end-3),titleTXT);

secs_per_day = 86400;
time  = ncread(fullfile(resDir,ncfile),'time')./secs_per_day; % seconds -> days
datefmt = 'mmm';
tmin = 2;
tmax = 1e99;

dz  = squeeze(ncread(fullfile(resDir,ncfile),'layer_height_in_soil'));
% dz_  = squeeze(ncread(fullfile(resDir,ncfile),'layer_height_in_soil'));
% dz  = squeeze(dz(:,1));
porosity = squeeze(ncread(fullfile(resDir,ncfile),'porosity_in_soil'));
dz = dz .* porosity;
%zeta = squeeze(ncread(fullfile(resDir,ncfile),'water_depth_at_soil_surface'));
zeta = sum(dz(:,:,:,1));

% float dissolved_reduced_substances_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float layer_center_depth_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;

varname = ['MicroPhytoBenthos_nitrogen_mpbN_in_soil'];
disp(varname);
mpbN  = squeeze(ncread(fullfile(resDir,ncfile),varname)); mpbN_=mpbN(end,:);
%mpbN_ = squeeze(sum(mpbN.*dz,1))./zeta;

% global variables from coupling
varname = ['detritus_labile_nitrogen_in_soil'];
disp(varname);
ldetN  = squeeze(ncread(fullfile(resDir,ncfile),varname)); ldetN_=ldetN(end,:);
%ldetN_ = squeeze(sum(ldetN.*dz,1))./zeta;

varname = ['mole_concentration_of_ammonium_in_soil'];
disp(varname);
nh4  = squeeze(ncread(fullfile(resDir,ncfile),varname)); nh4_=nh4(end,:);
%nh4_ = squeeze(sum(nh4.*dz,1))./zeta;

varname = ['mole_concentration_of_nitrate_in_soil'];
disp(varname);
no3  = squeeze(ncread(fullfile(resDir,ncfile),varname)); no3_=no3(end,:);
%no3_ = squeeze(sum(no3.*dz,1))./zeta;

totN_ = ldetN_ + mpbN_ + nh4_ + no3_;

% local variables from MPB
varname = ['MPB-CNP_nh4_in_soil'];
disp(varname);
MPBnh4  = squeeze(ncread(fullfile(resDir,ncfile),varname)); MPBnh4_=MPBnh4(end,:);
%MPBnh4_ = squeeze(sum(MPBnh4.*dz,1))./zeta;

varname = ['MPB-CNP_no3_in_soil'];
disp(varname);
MPBno3  = squeeze(ncread(fullfile(resDir,ncfile),varname)); MPBno3_=MPBno3(end,:);
%MPBno3_ = squeeze(sum(MPBno3.*dz,1))./zeta;

% varname = ['MPB-CNP_DIN_in_soil'];
% disp(varname);
% MPBdin  = squeeze(ncread(fullfile(resDir,ncfile),varname)); MPBdin_=MPBdin(end,:);
% MPBdin_ = squeeze(sum(MPBdin.*dz,3))./zeta;

MPBtotN_ = ldetN_ + mpbN_ + MPBnh4_ + MPBno3_;

%==========================================================
t0 = max([1            tmin]);
t1 = min([length(time) tmax]);

%==========================================================
figure('position',[680 600 672 504]);

% plot light
if (showPAR)
    varname = ['downwelling_photosynthetic_radiative_flux_at_soil_surface'];
    radS  = squeeze(ncread(fullfile(resDir,ncfile),varname));
    plot(time(t0:t1),radS(t0:t1),'color',[0.9922 0.9176 0.7961]); hold on;
    varname = ['photosynthetically_active_radiation_in_soil'];
    varname = ['MPB-CNP_photosynthetically_active_radiation_in_soil'];
    par  = squeeze(ncread(fullfile(resDir,ncfile),varname));
    plot(time(t0:t1),par(end,t0:t1),'color',[1.00 0.9686 0.9216]); hold on;
    xlim([time(t0)-1 time(t1)+1]);
    datetick('x',datefmt,'keeplimits'); %xlabel('time');
    ylabel('PAR at soil surface (W m-2)');
    set(gca,'Xaxislocation','top','XTickLabel',{}, ...
            'Yaxislocation','right','box','off');
    ax1 = axes('position',get(gca,'position'));
end

% plot variables
j=1;
lh(j)=plot(time(t0:t1),mpbN_(t0:t1),'-m'); hold on; legendtxt(j)={'mpbN'}; j=j+1;
lh(j)=plot(time(t0:t1),ldetN_(t0:t1),'-r'); hold on; legendtxt(j)={'ldetN'}; j=j+1;
lh(j)=plot(time(t0:t1),nh4_(t0:t1),  '-b'); hold on; legendtxt(j)={'nh4'};   j=j+1;
%%lh(j)=plot(time(t0:t1),MPBnh4_(t0:t1),  ':g'); hold on; legendtxt(j)={'MPBnh4'};   j=j+1;
lh(j)=plot(time(t0:t1),no3_(t0:t1),  '-g'); hold on; legendtxt(j)={'no3'};   j=j+1;
%%lh(j)=plot(time(t0:t1),MPBno3_(t0:t1),  ':b'); hold on; legendtxt(j)={'MPBno3'};   j=j+1;
lh(j)=plot(time(t0:t1),totN_(t0:t1), '-k'); hold on; legendtxt(j)={'totN'};  j=j+1;
%%lh(j)=plot(time(t0:t1),MPBtotN_(t0:t1), ':y'); hold on; legendtxt(j)={'MPBtotN'};  j=j+1;

legend(lh,legendtxt);%,'location','West');
xlim([time(t0)-1 time(t1)+1]);
datetick('x',datefmt,'keeplimits'); %xlabel('time');
%ylabel('vert. integrated (mmolN m-2)');
%ylabel('vert. average (mmolN m-3)');
ylabel('nitrogen in soil surface layer (mmolN m-3)');
if (exist('ax1')==1); set(ax1,'color','none','box','off'); end
titleTXT(titleTXT=='_') = ' ';
title(sprintf('%s ',titleTXT));


if saveFig
    set(gcf,'PaperType','A4')
    %orient_landscape;
    print([figname '.png'],'-dpng');
end
