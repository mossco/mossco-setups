
%close all;
clear all;

saveFig = (0==0);
%saveFig = (0==1);

resDir = '../.';
ncfile = 'helgoland_ICHE2014.nc';
ncfile = 'mossco_jfs.nc';

titleTXT = 'statevariables_maecs-N'
figname = sprintf('%s_%s',ncfile(1:end-3),titleTXT);

secs_per_day = 86400;
time  = ncread(fullfile(resDir,ncfile),'time')./secs_per_day; % seconds -> days
datefmt = 'mmm';
tmin = 2;
tmax = 1e99;

dz   = ncread(fullfile(resDir,ncfile),'layer_height_in_water');
zeta = squeeze(ncread(fullfile(resDir,ncfile),'water_depth_at_soil_surface'));

varname = ['Detritus_Nitrogen_detN_in_water'];
disp(varname);
det  = ncread(fullfile(resDir,ncfile),varname);
det_ = squeeze(sum(det.*dz,3))./zeta;

varname = ['Dissolved_Inorganic_Nitrogen_DIN_nutN_in_water'];
disp(varname);
nut  = ncread(fullfile(resDir,ncfile),varname);
nut_ = squeeze(sum(nut.*dz,3))./zeta;

varname = ['Phytplankton_Nitrogen_phyN_in_water'];
disp(varname);
phy  = ncread(fullfile(resDir,ncfile),varname);
phy_ = squeeze(sum(phy.*dz,3))./zeta;

varname = ['Zooplankton_Carbon_zooC_in_water'];
disp(varname);
zoo  = ncread(fullfile(resDir,ncfile),varname) .*0.3;
zoo_ = squeeze(sum(zoo.*dz,3))./zeta;

varname = ['Dissolved_Organic_Nitrogen_domN_in_water'];
disp(varname);
don  = ncread(fullfile(resDir,ncfile),varname);
don_ = squeeze(sum(don.*dz,3))./zeta;

totN_ = nut_ + phy_ + zoo_ + det_ + don_;

%==========================================================
t0 = max([1            tmin]);
t1 = min([length(time) tmax]);

%==========================================================
figure('position',[680 600 672 504]);
j=1;
lh(j)=plot(time(t0:t1),nut_(t0:t1),'-b'); hold on;legendtxt(j)={'nut'}; j=j+1;
lh(j)=plot(time(t0:t1),phy_(t0:t1),'-g'); hold on;legendtxt(j)={'phy'}; j=j+1;
lh(j)=plot(time(t0:t1),zoo_(t0:t1),'-r'); hold on;legendtxt(j)={'zoo'}; j=j+1;
lh(j)=plot(time(t0:t1),det_(t0:t1),'-m'); hold on;legendtxt(j)={'det'}; j=j+1;
lh(j)=plot(time(t0:t1),don_(t0:t1),'-c'); hold on;legendtxt(j)={'don'}; j=j+1;
lh(j)=plot(time(t0:t1),totN_(t0:t1),'-k');hold on;legendtxt(j)={'totN'};j=j+1;

legend(lh,legendtxt);%,'location','West');
xlim([time(t0)-1 time(t1)+1]);
datetick('x',datefmt,'keeplimits'); %xlabel('time');
%ylabel('vert. integrated (mmolN m-2)'); %ylabel('DIN (mmol m-3)');
ylabel('vert. average (mmolN m-3)'); %ylabel('DIN (mmol m-3)');
titleTXT(titleTXT=='_') = ' ';
title(sprintf('%s',titleTXT));

if saveFig
    set(gcf,'PaperType','A4')
    %orient_landscape;
    print([figname '.png'],'-dpng');
end
