from pylab import *
import netCDF4
import sys
import numpy as np
import matplotlib.pyplot as plt

def read_ncdf(ncfile):
  nc = netCDF4.Dataset(ncfile)
  ncv = nc.variables
  z = np.squeeze(ncv['layer_center_depth_in_soil'][:])
  dz = np.squeeze(ncv['layer_height_in_soil'][:])
  por = np.squeeze(ncv['porosity_in_soil'][:])
  longnames = ['mole_concentration_of_ammonium_in_soil','mole_concentration_of_nitrate_in_soil',
   'dissolved_oxygen_in_soil','dissolved_reduced_substances_in_soil','mole_concentration_of_phosphate_in_soil',
   'detritus_labile_carbon_in_soil','detritus_semilabile_carbon_in_soil','detritus_phosphorus_in_soil','denitrification_rate_in_soil']
  varnames=['nh3','no3','oxy','odu','po4','ldet','sdet','pdet','denit']
  data=[]
  for lname in longnames:
      data.append(np.squeeze(ncv[lname][:]))
  data=np.asarray(data)
  time=np.squeeze(ncv['time'][:])
  return varnames,time,z,dz,por,data


if __name__ == '__main__':

  if len(sys.argv)>1:
    ncfile=sys.argv[1]
  else:
    ncfile='mossco_1d.nc'
  varnames,time,z,dz,por,data = read_ncdf(ncfile)
  time=time/86400./365.0
  z=z*100.

  lsize=14.0
  fig=plt.figure(figsize=(10,10))

  plt.subplot(411)
  plt.contourf(time,-z[0],data[varnames.index('po4'),:,:].T,levels=range(0,70,4));
  plt.ylim(-4.0,-0.15); xlim(0,4)
  plt.text(0.5,-1.5,u'phosphate [mmol-P/m\u00b3]',size=lsize,color='k'); colorbar()
  plt.grid()

  plt.subplot(412)
  plt.contourf(time,-z[0],data[varnames.index('no3'),:,:].T,cmap=cm.pink_r);ylim(-4.0,-0.15); xlim(0,4)
  plt.text(0.5,-3.7,u'nitrate [mmol-N/m\u00b3]',size=lsize,color='k'); colorbar()
  plt.grid()

  plt.subplot(413)
  plt.contourf(time,-z[0],data[varnames.index('oxy'),:,:].T,cmap=cm.PuBuGn,levels=range(0,140,20));ylim(-2.0,-0.15); xlim(0,4); text(0.5,-1.7,u'oxygen [mmol-O2/m\u00b3]',size=lsize,color='k'); colorbar()
  plt.grid()

  plt.subplot(414)
  plt.contourf(time,-z[0],data[varnames.index('denit'),:,:].T*por.T,cmap=cm.pink_r);ylim(-4.0,-0.15); xlim(0,4); text(0.5,-3.7,u'denitrification [mmol-N/m\u00b3/d]',size=lsize,color='k'); colorbar()
  plt.grid()
  bb=plt.gca().get_position()

  plt.ylabel('depth [cm]')
  plt.xlabel('years')

#  ax=axes([bb.xmin,0.05,bb.xmax-bb.xmin,bb.ymax-bb.ymin])
#  plot(time,16*fluxes[:,3],'r-',lw=2.0,label='16* DIP flux')
#  plot(time,fluxes[:,4]+fluxes[:,5],'k-',lw=2.0,label='DIN flux')
#  plot(time,-16*fluxes[:,2],'r-',lw=1.0,label='-16* POP flux')
#  legend(frameon=False)
  #ylim(-2,0.1)

  plt.savefig('sediment_states.png',dpi=300)
  plt.show()
