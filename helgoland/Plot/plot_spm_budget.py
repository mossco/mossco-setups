import netCDF4
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl


if __name__ == '__main__':

    if len(sys.argv)>1:
        ncfile=sys.argv[1]
    else:
        ncfile='../mossco_jfsweb.nc'

    nc = netCDF4.Dataset(ncfile)
    ncv = nc.variables
    zmax = np.squeeze(ncv['water_depth_at_soil_surface'][1:])
    dz = np.squeeze(ncv['layer_height_in_water'][1:])

    longnames = []
    varnames = []
    nfrac = 0
    for key, value in ncv.items():
        if key.startswith('concentration_of_SPM_in_water'):
            longnames.append(key)
            varnames.append('SPM')
            nfrac = nfrac + 1
        elif key.startswith('concentration_of_SPM_upward_flux'):
            longnames.append(key)
            varnames.append('SPM' + key[-1] + ' flux ')

        elif key.startswith('sediment_mass_in'):
            longnames.append(key)
            varnames.append('mass')

    time=np.squeeze(ncv['time'][1:])
    time=time/3600.0

    z = dz * 0.0
    z[:,0] = 0.5*dz[:,0]
    z[:,1:] = 0.5*(np.cumsum(dz[:,1:], axis=1) + np.cumsum(dz[:,:-1], axis=-1))

    labelsize = 20-nfrac

    fig, ax = plt.subplots(nfrac, sharex=True, figsize=(10,10))

    ifrac=[-1]*nfrac
    exti=[-1]*nfrac
    for i in range(0,nfrac):
        if (nfrac > 1):
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface_00' + str(i+1)
        else:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface'
        exti[i]=ncv[varname].external_index
        ifrac[i]=ncv[varname].erosed_fraction
        #print varname, i, exti[i],ifrac[i]

    for i in range(0,nfrac):
        if (nfrac > 1):
            varname = 'concentration_of_SPM_in_water_00' + str(i+1)
        else:
            varname = 'concentration_of_SPM_in_water'
        if i==0:
            data = np.squeeze(ncv[varname][1:]).T
        else:
            data = data + np.squeeze(ncv[varname][1:]).T

        # concentration is g / m-3, dz is meter, integral is then
        # g m-2, devidid by 1000 is kg m-2 (as bedmass)
        pelagic_mass = np.sum(np.squeeze(ncv[varname][1:,:])*dz,axis=1)/1000.
        pelagic_mass_change = pelagic_mass[1:]-pelagic_mass[:-1]
        time_change = 0.5*(time[1:]+time[:-1])

        if (nfrac == 1):
            tmp_ax = ax
            ax = []
            ax.append(tmp_ax)
            
        ax[i].set_xlim(12,60)
        #ax[i].set_ylim(-0.007,0.007)

        ax[i].scatter(time_change,pelagic_mass_change,lw=0.5,label=u'$\Delta$mass$_p$',color='blue',linestyle='--',marker='o')

        if (nfrac > 1):
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface_00' + str(i+1)
        else:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface'

        idx = longnames.index(varname)
        data = np.squeeze(ncv[varname][1:])

        ax[i].plot(time,data*3.6,'k-',lw=0.5,label=varnames[idx],color='red')

        varname = 'sediment_mass_in_bed'
        idx = longnames.index(varname)
        data = np.squeeze(ncv[varname][1:,ifrac[i]-1])
        sediment_mass_change = data[1:]-data[:-1]

        ax[i].scatter(time_change,-sediment_mass_change,lw=0.5,label=u'$-\Delta$mass$_b$',color='green',linestyle='--',marker='d')

        xrange=ax[i].get_xlim()
        yrange=ax[i].get_ylim()
        xpos=xrange[0]+0.1*(xrange[1]-xrange[0])
        ypos=yrange[1]-0.2*(yrange[1]-yrange[0])
        ax[i].set_ylabel('kg m-2',color='blue')

        label='SPM{}, bed {}'.format(exti[i], ifrac[i])
        ax[i].text(xpos,ypos,label, size=labelsize, color='k');
        ax[i].legend()

    ax[nfrac-1].set_xlabel('Time (h)')
    nc.close()
    plt.savefig('spm_budget.png',dpi=300)
