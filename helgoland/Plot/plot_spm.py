import netCDF4
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl


if __name__ == '__main__':

    if len(sys.argv)>1:
        ncfile=sys.argv[1]
    else:
        ncfile='../mossco_jfsweb.nc'

    nc = netCDF4.Dataset(ncfile)
    ncv = nc.variables
    zmax = np.squeeze(ncv['water_depth_at_soil_surface'][1:])
    dz = np.squeeze(ncv['layer_height_in_water'][1:])

    longnames = []
    varnames = []
    nfrac = 0
    for key, value in ncv.items():
        if key.startswith('concentration_of_SPM_in_water'):
            longnames.append(key)
            varnames.append('SPM')
            nfrac = nfrac + 1
        elif key.startswith('concentration_of_SPM_upward_flux'):
            longnames.append(key)
            varnames.append('SPM' + key[-1] + ' flux ')

        elif key.startswith('sediment_mass_in'):
            longnames.append(key)
            varnames.append('mass')

        elif key.startswith('Effect'):
            longnames.append(key)
            varname='X'
            if 'MPB' in key: varname = 'phyto_'
            elif 'Mbalth' in key: varname = 'zoo_'
            if 'erodi' in key: varname = varname + 'ero'
            elif 'shear' in key: varname = varname + 'shear'
            varnames.append(varname)

        elif key.startswith('shear_stress_noncohesive'):
            longnames.append(key)
            varname = u'$\{tau}_noncoh.$'
            varnames.append(varname)

        elif key.startswith('shear_stress_cohesive'):
            longnames.append(key)
            varname = u'$\{tau}_coh.$'
            varnames.append(varname)

    print varnames
    time=np.squeeze(ncv['time'][1:])
    time=time/86400./365.0

    z = dz * 0.0
    z[:,0] = 0.5*dz[:,0]
    z[:,1:] = 0.5*(np.cumsum(dz[:,1:], axis=1) + np.cumsum(dz[:,:-1], axis=-1))

    labelsize = 20-nfrac

    fig, ax = plt.subplots(nfrac+2, 2, sharex=True, figsize=(10,10))

    for i in range(0,nfrac):
        if nfrac == 1: varname = 'concentration_of_SPM_in_water'
        else:
            varname = 'concentration_of_SPM_in_water_00' + str(i+1)
        if i==0:
            data = np.squeeze(ncv[varname][1:]).T
        else:
            data = data + np.squeeze(ncv[varname][1:]).T

    levels=np.unique(np.percentile(data/nfrac,[0,50,75,90,95,99]))

    ifrac=[-1]*nfrac
    exti=[-1]*nfrac
    for i in range(0,nfrac):
        if nfrac == 1:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface'
        else:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface_00' + str(i+1)
        exti[i]=ncv[varname].external_index
        ifrac[i]=ncv[varname].erosed_fraction

    print exti, ifrac

    for i in range(0,nfrac):

        if nfrac == 1:
            varname = 'concentration_of_SPM_in_water'
        else:
            varname = 'concentration_of_SPM_in_water_00' + str(i+1)

        # concentration is g / m-3, dz is meter, integral is then
        # g m-2, devidid by 1000 is kg m-2 (as bedmass)
        pelagic_mass = np.sum(np.squeeze(ncv[varname][1:,:])*dz,axis=1)/1000.

        idx = longnames.index(varname)
        layout = 100*(nfrac+1) + 21 + 2*i

        data = np.squeeze(ncv[varname][1:]).T
        cmap = cm.pink_r
        ctr=ax[i,0].contourf(time, z[0], data, levels=levels, cmap=cmap);

        xrange=ax[i,0].get_xlim()
        yrange=ax[i,0].get_ylim()
        xpos=xrange[0]+0.1*(xrange[1]-xrange[0])
        ypos=yrange[1]-0.3*(yrange[1]-yrange[0])

        label='{}{}/{} {}'.format(varnames[idx],exti[i],ifrac[i],
                        ncv[varname].mean_particle_diameter*1000)
        ax[i,0].text(xpos,ypos,label,size=labelsize,color='k')

        plt.colorbar(ctr, ax=ax[i,0])
#        mpl.colorbar.ColorbarBase(ax[i,0], cmap=cmap, orientation='vertical')

        #plt.colorbar(ctr)
        ax[i,0].grid()
        ax[i,0].set_ylabel('height (m)')

        if nfrac == 1:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface'
        else:
            varname = 'concentration_of_SPM_upward_flux_at_soil_surface_00' + str(i+1)

        idx = longnames.index(varname)
        data = np.squeeze(ncv[varname][1:])

        #ax[i,1].scatter(time,data,0.1,label=varnames[idx],color='blue')
        ax[i,1].plot(time,data*3.6,'k-',lw=0.5,label=varnames[idx],color='blue')
        xrange=ax[i,1].get_xlim()
        yrange=ax[i,1].get_ylim()
        xpos=xrange[0]+0.1*(xrange[1]-xrange[0])
        ypos=yrange[1]-0.2*(yrange[1]-yrange[0])
        ax[i,1].set_ylabel('kg m-2 h-1',color='blue')
        ax[i,1].spines['left'].set_color('blue')

        label='{}{}/{}'.format(varnames[idx],exti[i],
                        ifrac[i])

        ax[i,1].text(xpos,ypos,label, size=labelsize, color='blue');

        varname = 'sediment_mass_in_bed'
        idx = longnames.index(varname)
        print(ifrac)
        data = np.squeeze(ncv[varname][1:,ifrac[i]-1])
        gca=plt.twinx(ax[i,1])
        #gca.scatter(time,data,0.1,label=varnames[idx],color='orange')
        gca.plot(time,data,'k-',lw=0.5,label=varnames[idx],color='red')
        gca.plot(time,pelagic_mass.T,'k-',lw=0.5,label='SPM mass',color='orange')
        gca.plot(time,data + pelagic_mass.T,'k-',lw=0.5,label='Total mass',color='black')

        xrange=gca.get_xlim()
        yrange=gca.get_ylim()
        xpos=xrange[0]+0.1*(xrange[1]-xrange[0])
        ypos=yrange[1]-0.4*(yrange[1]-yrange[0])


        gca.text(xpos,ypos,varnames[idx], size=labelsize, color='red');

        #plt.bar(time, data, 0.02, color='k')
    #plt.contourf(time,-z[0],data[varnames.index('no3'),:,:].T,cmap=cm.pink_r);ylim(-4.0,-0.15); xlim(0,4)
    #plt.text(0.5,-3.7,u'nitrate [mmol-N/m\u00b3]',size=lsize,color='k');

    #plt.contourf(time,-z[0],data[varnames.index('oxy'),:,:].T,cmap=cm.PuBuGn,levels=range(0,140,20));ylim(-2.0,-0.15); xlim(0,4); text(0.5,-1.7,u'oxygen [mmol-O2/m\u00b3]',size=lsize,color='k');

    #plt.contourf(time,-z[0],data[varnames.index('denit'),:,:].T*por.T,cmap=cm.pink_r);ylim(-4.0,-0.15); xlim(0,4); text(0.5,-3.7,u'denitrification [mmol-N/m\u00b3/d]',size=lsize,color='k');

    varname = 'Effect_of_MPB_on_sediment_erodibility_at_soil_surface'
    idx = longnames.index(varname)
    data = np.squeeze(ncv[varname][1:])

    ax[nfrac,0].scatter(time,data,0.1,label=varnames[idx],linestyle='-')

    varname = 'Effect_of_Mbalthica_on_sediment_erodibility_at_soil_surface'
    idx = longnames.index(varname)
    data = np.squeeze(ncv[varname][1:])

    ax[nfrac,0].scatter(time,data,0.1,label=varnames[idx],linestyle='-')
    ax[nfrac,0].set_xlabel('years')
    ax[nfrac,0].set_ylabel('factor')
    ax[nfrac,0].legend(frameon=True)

    varname = 'Effect_of_MPB_on_critical_bed_shearstress_at_soil_surface'
    idx = longnames.index(varname)
    data = np.squeeze(ncv[varname][1:])

    ax[nfrac,1].scatter(time,data,0.1,label=varnames[idx],linestyle='-')

    varname = 'Effect_of_Mbalthica_on_critical_bed_shearstress_at_soil_surface'
    idx = longnames.index(varname)
    data = np.squeeze(ncv[varname][1:])

    ax[nfrac,1].scatter(time,data,0.1,label=varnames[idx],linestyle='-')
    gca = ax[nfrac,1].twinx()

    try:
        varname = 'shear_stress_noncohesive_at_soil_surface'
        idx = longnames.index(varname)
        data = np.squeeze(ncv[varname][1:])
        gca.scatter(time,data,0.1,label=varnames[idx],linestyle=':')
    except:
        pass

    try:
        varname = 'shear_stress_cohesive_at_soil_surface'
        idx = longnames.index(varname)
        data = np.squeeze(ncv[varname][1:])
        gca.scatter(time,data,0.1,label=varnames[idx],linestyle=';')
    except:
        pass

    ax[nfrac,1].legend(frameon=True)

    varname = 'sediment_mass_in_bed'
    idx = longnames.index(varname)
    data = np.squeeze(ncv[varname][1:,:])
    if len(data.shape) > 1:
        data = np.mean(np.squeeze(ncv[varname][1:,:]),axis=1)
    colors='rgbcmy'
    styles=['--','-.',':','--','-.',':']
    ax[nfrac+1,0].plot(time,data,'k-',lw=1.5,label='bed mass/' + str(nfrac),color='grey',alpha=0.5)

    ax[nfrac+1,0].plot(time,data+pelagic_mass.T,'k-',lw=1.5,label='total mass/' + str(nfrac),color='grey',alpha=0.2)

    for i in range(0,nfrac):
        data = np.squeeze(ncv[varname][1:,ifrac[i]-1])
        ax[nfrac+1,0].plot(time,data,'k-',lw=1-0.6*i/nfrac,label='frac {}'.format(exti[i]),color=colors[i],linestyle=styles[i])
    ax[nfrac+1,0].legend(frameon=True)
    ax[nfrac+1,0].set_xlabel('years')
    ax[nfrac+1,0].set_ylabel('kg m-2')



    #legend(frameon=False)
    nc.close()


#  ax=axes([bb.xmin,0.05,bb.xmax-bb.xmin,bb.ymax-bb.ymin])
#  plot(time,16*fluxes[:,3],'r-',lw=2.0,label='16* DIP flux')
#  plot(time,fluxes[:,4]+fluxes[:,5],'k-',lw=2.0,label='DIN flux')
#  plot(time,-16*fluxes[:,2],'r-',lw=1.0,label='-16* POP flux')
#  legend(frameon=False)
  #ylim(-2,0.1)

    plt.savefig('spm.png',dpi=300)
    #plt.show()
