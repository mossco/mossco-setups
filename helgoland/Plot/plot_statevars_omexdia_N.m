
%close all;
clear all;

saveFig = (0==0);
%saveFig = (0==1);

resDir = '../.';
ncfile = 'helgoland_ICHE2014.nc';
% ncfile = 'mossco_jfs.nc';

titleTXT = 'statevariables_omexdia-N';
figname = sprintf('%s_%s',ncfile(1:end-3),titleTXT);

secs_per_day = 86400;
time  = ncread(fullfile(resDir,ncfile),'time')./secs_per_day; % seconds -> days
datefmt = 'mmm';
tmin = 2;
tmax = 1e99;

dz  = squeeze(ncread(fullfile(resDir,ncfile),'layer_height_in_soil'));
% dz_  = squeeze(dz(:,1));
porosity = squeeze(ncread(fullfile(resDir,ncfile),'porosity_in_soil'));
dz = dz .* porosity;
%zeta = squeeze(ncread(fullfile(resDir,ncfile),'water_depth_at_soil_surface'));
zeta = sum(dz(:,:,:,1));

% float dissolved_reduced_substances_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float layer_center_depth_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float photosynthetically_active_radiation_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;

varname = ['detritus_labile_carbon_in_soil']; disp(varname);
ldetC  = squeeze(ncread(fullfile(resDir,ncfile),varname)); ldetC_ = ldetC(end,:);
ldetC_ = squeeze(sum(ldetC.*dz,1))./zeta;
ldetN_ = ldetC_ .* 0.22;

varname = ['detritus_semilabile_carbon_in_soil']; disp(varname);
sdetC  = squeeze(ncread(fullfile(resDir,ncfile),varname)); sdetC_ = sdetC(end,:);
sdetC_ = squeeze(sum(sdetC.*dz,1))./zeta;
sdetN_ = sdetC_ .* 0.005;

varname = ['mole_concentration_of_ammonium_in_soil']; disp(varname);
nh4  = squeeze(ncread(fullfile(resDir,ncfile),varname)); nh4_ = nh4(end,:);
nh4_ = squeeze(sum(nh4.*dz,1))./zeta;

varname = ['mole_concentration_of_nitrate_in_soil']; disp(varname);
no3  = squeeze(ncread(fullfile(resDir,ncfile),varname)); no3_ = no3(end,:);
no3_ = squeeze(sum(no3.*dz,1))./zeta;

totN_ = ldetN_ + sdetN_ + nh4_ + no3_;

%==========================================================
t0 = max([1            tmin]);
t1 = min([length(time) tmax]);

%==========================================================
figure('position',[680 600 672 504]);
j=1;
lh(j)=plot(time(t0:t1),nh4_(t0:t1),  '-b'); hold on; legendtxt(j)={'nh4'};   j=j+1;
lh(j)=plot(time(t0:t1),no3_(t0:t1),  '-g'); hold on; legendtxt(j)={'no3'};   j=j+1;
lh(j)=plot(time(t0:t1),ldetN_(t0:t1),'-r'); hold on; legendtxt(j)={'ldetN'}; j=j+1;
lh(j)=plot(time(t0:t1),sdetN_(t0:t1),'-m'); hold on; legendtxt(j)={'sdetN'}; j=j+1;
lh(j)=plot(time(t0:t1),totN_(t0:t1), '-k'); hold on; legendtxt(j)={'totN'};  j=j+1;

legend(lh,legendtxt);%,'location','West');
xlim([time(t0)-1 time(t1)+1]);
datetick('x',datefmt,'keeplimits'); %xlabel('time');
%ylabel('vert. integrated (mmolN m-2)'); %ylabel('DIN (mmol m-3)');
ylabel('vert. average (mmolN m-3)'); %ylabel('DIN (mmol m-3)');
titleTXT(titleTXT=='_') = ' ';
title(sprintf('%s ',titleTXT));

if saveFig
    set(gcf,'PaperType','A4')
    %orient_landscape;
    print([figname '.png'],'-dpng');
end
