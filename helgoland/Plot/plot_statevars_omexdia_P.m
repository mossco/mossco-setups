
%close all;
clear all;

saveFig = (0==0);
%saveFig = (0==1);

resDir = '../.';
ncfile = 'helgoland_ICHE2014.nc';
% ncfile = 'mossco_jfs.nc';

titleTXT = 'statevariables_omexdia-P';
figname = sprintf('%s_%s',ncfile(1:end-3),titleTXT);

secs_per_day = 86400;
time  = ncread(fullfile(resDir,ncfile),'time')./secs_per_day; % seconds -> days
datefmt = 'mmm';
tmin = 2;
tmax = 1e99;

dz  = squeeze(ncread(fullfile(resDir,ncfile),'layer_height_in_soil'));
% dz_  = squeeze(dz(:,1));
porosity = squeeze(ncread(fullfile(resDir,ncfile),'porosity_in_soil'));
dz = dz .* porosity;
%zeta = squeeze(ncread(fullfile(resDir,ncfile),'water_depth_at_soil_surface'));
zeta = sum(dz(:,:,:,1));

% float dissolved_reduced_substances_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float layer_center_depth_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float photosynthetically_active_radiation_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;

varname = ['detritus_phosphorus_in_soil'];
disp(varname);
ldetP  = squeeze(ncread(fullfile(resDir,ncfile),varname)); ldetP_ = ldetP(end,:);
ldetP_ = squeeze(sum(ldetP.*dz,1))./zeta;

% varname = ['dissolved_oxygen_in_soil'];
% disp(varname);
% oxy  = squeeze(ncread(fullfile(resDir,ncfile),varname)); oxy_ = oxy(end,:);
% oxy_ = squeeze(sum(oxy.*dz,1))./zeta;

varname = ['mole_concentration_of_phosphate_in_soil'];
disp(varname);
po4  = squeeze(ncread(fullfile(resDir,ncfile),varname)); po4_ = po4(end,:);
po4_ = squeeze(sum(po4.*dz,1))./zeta;

varname = ['phosphate_adsorption_in_soil'];
disp(varname);
pads  = squeeze(ncread(fullfile(resDir,ncfile),varname)); pads_ = pads(end,:);
pads_ = squeeze(sum(pads.*dz,1))./zeta;


totP_ = ldetP_ + po4_ ;%+ pads_;

%==========================================================
t0 = max([1            tmin]);
t1 = min([length(time) tmax]);

%==========================================================
figure('position',[680 600 672 504]);
j=1;
lh(j)=plot(time(t0:t1),po4_(t0:t1),  '-b'); hold on; legendtxt(j)={'po4'};   j=j+1;
%lh(j)=plot(time(t0:t1),no3_(t0:t1),  '-g'); hold on; legendtxt(j)={'no3'};   j=j+1;
lh(j)=plot(time(t0:t1),ldetP_(t0:t1),'-r'); hold on; legendtxt(j)={'ldetP'}; j=j+1;
%lh(j)=plot(time(t0:t1),sdetN_(t0:t1),'-m'); hold on; legendtxt(j)={'sdetN'}; j=j+1;
lh(j)=plot(time(t0:t1),totP_(t0:t1), '-k'); hold on; legendtxt(j)={'totP'};  j=j+1;

legend(lh,legendtxt);%,'location','West');
xlim([time(t0)-1 time(t1)+1]);
datetick('x',datefmt,'keeplimits'); %xlabel('time');
%ylabel('vert. integrated (mmolP m-2)'); %ylabel('DIP (mmol m-3)');
ylabel('vert. average (mmolP m-3)'); %ylabel('DIP (mmol m-3)');
titleTXT(titleTXT=='_') = ' ';
title(sprintf('%s ',titleTXT));

if saveFig
    set(gcf,'PaperType','A4')
    %orient_landscape;
    print([figname '.png'],'-dpng');
end
