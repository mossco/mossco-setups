from pylab import *
import netCDF4
from plot_sediment import read_data
import sys
import os

def read_ncdf(ncfile):
  nc = netCDF4.Dataset(ncfile)
  ncv = nc.variables
  z = squeeze(ncv['layer_center_depth_in_soil'][:])
  dz = squeeze(ncv['layer_height_in_soil'][:])
  por = squeeze(ncv['porosity_in_soil'][:])
  longnames = ['mole_concentration_of_ammonium_in_soil','mole_concentration_of_nitrate_in_soil','dissolved_oxygen_in_soil','dissolved_reduced_substances_in_soil','mole_concentration_of_phosphate_in_soil','fast_detritus_C_in_soil','slow_detritus_C_in_soil','detritus-P_in_soil']
  varnames=['nh3','no3','oxy','odu','po4','ldet','sdet','pdet']
  data=[]
  for lname in longnames:
      data.append(squeeze(ncv[lname][:,:,0,0]))
  data=asarray(data)
  time=squeeze(ncv['time'][:])
  nc.close()
  return varnames,time,z,dz,por,data

if len(sys.argv) > 1:
    ncfile=sys.argv[1]
    if ncfile.endswith('.nc'): 
        path=os.path.dirname(ncfile)
    else:
        path=sys.argv[1]
else:
    path='../'

sedtxt=False
if sedtxt:
  varnames,time,z,dz,por,data = read_data(path+'/output.dat')
elif ncfile.endswith('.nc'):
  varnames,time,z,dz,por,data = read_ncdf(ncfile)
else:
  varnames,time,z,dz,por,data = read_ncdf(path+'/fbp_exchange_state.000.nc')

nc = netCDF4.Dataset(path+'/helgoland.nc'); ncv=nc.variables
tpel = ncv['time'][:]
h = squeeze(ncv['h'][:])

maecs=False
npzd=False
maecs_with_zooC=False
if 'hzg_maecs_nutN' in ncv:
    maecs=True
elif 'gotm_npzd_nut' in ncv:
    npzd=True
if 'hzg_maecs_zooC' in ncv:
    maecs_with_zooC = True

if maecs:
  pvars = ['hzg_maecs_nutP','hzg_maecs_domP','hzg_maecs_detP','hzg_maecs_phyP']
  nvars = ['hzg_maecs_nutN','hzg_maecs_domN','hzg_maecs_detN','hzg_maecs_phyN']
elif npzd:
  nvars = ['gotm_npzd_nut','gotm_npzd_det','gotm_npzd_zoo','gotm_npzd_phy']
  pvars = []



sedp_t=[]
pelp_t=[]
sedn_t=[]
peln_t=[]

tpel_idx=0

for tidx in range(len(time)):

  checktime=time[tidx]

  sedp =  sum(dz[tidx,:]*por[tidx,:]*data[varnames.index('po4'),tidx,:])
  sedp += sum(dz[tidx,:]*por[tidx,:]*data[varnames.index('pdet'),tidx,:])

  sedn =  sum(dz[tidx,:]*por[tidx,:]*data[varnames.index('no3'),tidx,:])
  sedn += sum(dz[tidx,:]*por[tidx,:]*0.04*data[varnames.index('sdet'),tidx,:])
  sedn += sum(dz[tidx,:]*por[tidx,:]*0.20*data[varnames.index('ldet'),tidx,:])
  sedn += sum(dz[tidx,:]*por[tidx,:]*data[varnames.index('nh3'),tidx,:])

  try:
      tpel_idx = int(where(tpel == checktime)[0])
  except:
      print('tpel_idx stays constant: %d'%tpel_idx)

  pelp=0.0
  peln=0.0

  for pvar in pvars:
    pelp += sum(h[tpel_idx,:]*squeeze(ncv[pvar][tpel_idx,:,0,0]))

  for nvar in nvars:
    peln += sum(h[tpel_idx,:]*squeeze(ncv[nvar][tpel_idx,:,0,0]))

  if maecs_with_zooC:
    peln += 0.3*sum(h[tpel_idx,:]*squeeze(ncv['hzg_maecs_zooC'][tpel_idx,:,0,0]))
    pelp += 0.025*sum(h[tpel_idx,:]*squeeze(ncv['hzg_maecs_zooC'][tpel_idx,:,0,0]))

  if npzd:
    pelp = 1.0/16.0*peln

  sedp_t.append(sedp)
  sedn_t.append(sedn)
  pelp_t.append(pelp)
  peln_t.append(peln)

if maecs:
  detn=squeeze(ncv['hzg_maecs_detN'][:,1,0,0])
  detc=squeeze(ncv['hzg_maecs_detC'][:,1,0,0])
  detp=squeeze(ncv['hzg_maecs_detP'][:,1,0,0])
  QNdet=detn/detc

nc.close()
sedp_t=asarray(sedp_t)
sedn_t=asarray(sedn_t)
pelp_t=asarray(pelp_t)
peln_t=asarray(peln_t)

totn_diff = peln_t[-2]-peln_t[1] + sedn_t[-2] - sedn_t[1]
totp_diff = pelp_t[-2]-pelp_t[1] + sedp_t[-2] - sedp_t[1]

print('Phosphorus budget = %f mmolP/m**2'%(totp_diff))
print('Nitrogen   budget = %f mmolN/m**2'%(totn_diff))
print('pel. Phosphorus budget = %f mmolP/m**2'%(pelp_t[-2]-pelp_t[1]))
print('pel. Nitrogen   budget = %f mmolN/m**2'%(peln_t[-2]-peln_t[1]))
print('sed. Phosphorus budget = %f mmolP/m**2'%(sedp_t[-2]-sedp_t[1]))
print('sed. Nitrogen   budget = %f mmolN/m**2'%(sedn_t[-2]-sedn_t[1]))

figure()

if maecs:
    offset=100
elif npzd:
    offset=0

#subplot(411)
subplot(211+offset)
plot(time,sedp_t,'r-',lw=1.0,label='sediment')
plot(time,sedp_t+pelp_t,'k-',lw=1.0,label='total')
#subplot(412)
plot(time,pelp_t,'b-',lw=1.0,label='pelagic')
title('phosphorus')
legend(loc='lower right')

#subplot(413)
subplot(212+offset)
plot(time,sedn_t-sedn_t[1],'r-',lw=1.0)
plot(time,sedn_t-sedn_t[1]+peln_t-peln_t[1],'k-',lw=1.0)
#subplot(414)
plot(time,peln_t-peln_t[1],'b-',lw=1.0)
title('nitrogen')

if maecs:
  subplot(313)
  plot(tpel/86400./365.,detp)

show()

