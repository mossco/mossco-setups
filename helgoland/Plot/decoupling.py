import netCDF4 as nc4
import sys
import matplotlib.pyplot as plt
import numpy as np

def running_mean(a,window=20):
    anew = a.copy()
    for i in range(len(a)):
        anew[i] = a[np.max(0,i-window/2):np.min(len(a),i+window/2)].mean()
    return anew
    
if len(sys.argv)>1:   
    nc=nc4.Dataset(sys.argv[1])
else:
   nc=nc4.Dataset('../mossco_jfs.nc')
    
ncv=nc.variables
time=ncv['time'][:]/86400. #days since 2002-01-01

pname='hzg_maecs_nutP'
nname='hzg_maecs_nutN'
p_factor=1.0

pname='gotm_npzd_nut'
nname='gotm_npzd_nut'
p_factor=1./16.

#pname='hzg_n2pzdq_nut_N'
#nname='hzg_n2pzdq_nut_P'

try:
    onc=nc4.Dataset('/home/hofmeist/MOSSCO/mossco-setups/helgoland/Plot/Diatoms_0205.nc')   
    oncv=onc.variables
    otime=oncv['time'][:] #days since 2002-01-01
except:
    pass
    otime=[]

numyears=1
if len(sys.argv) > 2: 
    year=int(sys.argv[2])
else:
    year=2002

monthslen=[31,28,31,30,31,30,31,31,30,31,30,31]
monthsnames=['J','F','M','A','M','J','J','A','S','O','N','D']
monthsnames=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
ticks=np.cumsum([ i for i in [0]+(numyears-1)*monthslen+monthslen[:-1]])
tickl=[ '   '+s for s in numyears*monthsnames]

po4 = np.squeeze(oncv['PO4'][:])
po4 = ma.masked_where(isnan(po4),po4)
mpo4 = running_mean(po4,window=14)

no3 = np.squeeze(oncv['NO3'][:])
no3 = ma.masked_where(isnan(no3),no3)
mno3 = running_mean(no3,window=14)

#get time frame
tmin=np.min(np.where(time>=(year-2002)*365)[0])
tmax=np.max(np.where(time<=(year+1-2002)*365)[0])
tslice=slice(tmin,tmax)
if len(otime)>0:
    otmin=np.where(otime>=(year-2002)*365)[0].min()
    otmax=np.where(otime<=(year+1-2002)*365)[0].max()
    otslice=np.slice(otmin,otmax)

f=plt.figure(figsize=(6,6))
f.subplots_adjust(top=0.95,bottom=0.05,hspace=0.5)

nutP=np.squeeze(ncv[pname][:,9,0,0])*p_factor
nutN=np.squeeze(ncv[nname][:,9,0,0])

axn=plt.axes()
axp=axn.twinx()

axn.plot(time[tslice],nutN[tslice],'-',lw=3.,color='red',label='nitrate [mmol-N/m3]')
if len(otime)>0: axn.plot(otime[otslice],no3[otslice],'o',color='red')
axn.set_ylabel('nitrate [mmol-N/m3]',color='r')

axp.plot(time[tslice],nutP[tslice],'-',lw=3.,color='blue',label='phosphate [mmol-P/m3]')
if len(otime)>0: axp.plot(otime[otslice],po4[otslice],'o',color='blue')
axp.set_ylim(0,4)
axp.set_ylabel('phosphate [mmol-P/m3]',color='b')

axn.set_xticks(ticks+365)
axn.set_xticklabels(tickl)
axn.set_xlim(time[tmin],time[tmax])
#axn.legend(frameon=False)


show()

nc.close()
onc.close()
