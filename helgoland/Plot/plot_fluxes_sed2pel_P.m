
%close all;
clear all;

saveFig = (0==0);
%saveFig = (0==1);

resDir = '../.';
ncfile = 'helgoland_ICHE2014.nc';
% ncfile = 'mossco_jfs.nc';

titleTXT = 'fluxes_sed2pel-P';
figname = sprintf('%s_%s',ncfile(1:end-3),titleTXT);

secs_per_day = 86400;
time  = ncread(fullfile(resDir,ncfile),'time')./secs_per_day; % seconds -> days
datefmt = 'mmm';
tmin = 2;
tmax = 1e99;

% float dissolved_reduced_substances_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float layer_center_depth_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float photosynthetically_active_radiation_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;
% float temperature_in_soil(time, ungridded00024, helgoland2d_2, helgoland2d_1) ;

dz_  = ncread(fullfile(resDir,ncfile),'layer_height_in_soil');
% dz_  = squeeze(ncread(fullfile(resDir,ncfile),'layer_height_in_soil'));
% dz  = squeeze(dz(:,1));
porosity = ncread(fullfile(resDir,ncfile),'porosity_in_soil');
dz = dz_ .* porosity;
%zeta = squeeze(ncread(fullfile(resDir,ncfile),'water_depth_at_soil_surface'));
zeta = sum(dz(:,:,:,1));

varname = ['phosphate_adsorption_in_soil'];
pads  = ncread(fullfile(resDir,ncfile),varname);
pads_ = squeeze(sum(pads.*dz,3)); %./secs_per_day; % stimmt das mit secs_per_day?

varname = ['detritus_phosphorus_upward_flux_at_soil_surface'];
disp(varname);
ldetP_ = squeeze(ncread(fullfile(resDir,ncfile),varname));

varname = ['mole_concentration_of_phosphate_upward_flux_at_soil_surface'];
disp(varname);
po4_ = squeeze(ncread(fullfile(resDir,ncfile),varname));

% varname = ['dissolved_oxygen_upward_flux_at_soil_surface'];
% disp(varname);
% oxy_ = squeeze(ncread(fullfile(resDir,ncfile),varname));


totP_ = ldetP_ - po4_;

%==========================================================
t0 = max([1            tmin]);
t1 = min([length(time) tmax]);

%==========================================================
figure('position',[680 600 672 504]);
j=1;
%lh(j)=plot(time(t0:t1),pads_(t0:t1),'-y'); hold on;  legendtxt(j)={'pads'};  j=j+1;
lh(j)=plot(time(t0:t1),po4_(t0:t1),  '-b'); hold on; legendtxt(j)={'po4'};   j=j+1;
%lh(j)=plot(time(t0:t1),no3_(t0:t1),  '-g'); hold on; legendtxt(j)={'no3'};   j=j+1;
lh(j)=plot(time(t0:t1),ldetP_(t0:t1),'-r'); hold on; legendtxt(j)={'ldetP'}; j=j+1;
%lh(j)=plot(time(t0:t1),sdetN_(t0:t1),'-m'); hold on; legendtxt(j)={'sdetN'}; j=j+1;
%lh(j)=plot(time(t0:t1),totP_(t0:t1), '-k'); hold on; legendtxt(j)={'totP'};  j=j+1;

legend(lh,legendtxt);%,'location','West');
xlim([time(t0)-1 time(t1)+1]);
datetick('x',datefmt,'keeplimits'); %xlabel('time');
%ylabel('vert. integrated (mmolP m-2)'); %ylabel('DIP (mmol m-3)');
ylabel('vert. average (mmolP m-3)'); %ylabel('DIP (mmol m-3)');
titleTXT(titleTXT=='_') = ' ';
title(sprintf('%s ',titleTXT));

if saveFig
    set(gcf,'PaperType','A4')
    %orient_landscape;
    print([figname '.png'],'-dpng');
end
