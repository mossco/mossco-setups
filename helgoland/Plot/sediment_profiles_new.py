from matplotlib import rc
rc('font',**{'family':'serif','serif':['FreeSans']})
import matplotlib.pyplot as plt
import netCDF4
from netcdftime import utime
from datetime import datetime
import sys
import numpy as np
import os

def set_subaxes(fig,ax,level=1):
  axn = fig.add_axes(ax.get_position())
  axn.set_frame_on(True)
  axn.patch.set_visible(False)
  axn.xaxis.set_ticks_position('bottom')
  axn.xaxis.set_label_position('bottom')
  axn.spines['bottom'].set_position(('outward', level*50))
  return axn

if len(sys.argv) > 1:
    ncfile=sys.argv[1]
else:
    ncfile='../mossco_jfs.nc'
    
if len(sys.argv)>2:
  tstr = sys.argv[2]
else:
  tstr='2002-03-01 00:00:00'

nc=netCDF4.Dataset(ncfile)
ncv=nc.variables
tv=ncv['time']

ut=utime(tv.units)
time = ut.num2date(tv[:])
#print(ncv.keys())

nh4 = ncv['mole_concentration_of_ammonium_in_soil'][:]
no3 = ncv['mole_concentration_of_nitrate_in_soil'][:]
o2  = ncv['dissolved_oxygen_in_soil'][:]
sdet = ncv['detritus_labile_carbon_in_soil'][:]
ldet = ncv['detritus_semilabile_carbon_in_soil'][:]
pdet = ncv['detritus_phosphorus_in_soil'][:]
odu = ncv['dissolved_reduced_substances_in_soil'][:]
po4 = ncv['mole_concentration_of_phosphate_in_soil'][:]
denit = ncv['denitrification_rate_in_soil'][:]
h = ncv['layer_height_in_soil'][:]
z = ncv['layer_center_depth_in_soil'][:]
por = ncv['porosity_in_soil'][:]

pon=0.2*ldet# + 0.04*sdet
pos = ldet+sdet

tidx=np.int(np.where(ut.date2num(time) == ut.date2num(num2date(datestr2num(tstr))))[0])
zz = z[tidx,:,0,0]
pori = por[tidx,:,0,0]
lw=2.0

fig=figure(figsize=(10,10))

ax = fig.add_axes([0.15,0.3,0.3,0.65])
plt.plot(nh4[tidx,:,0,0],-zz,'k-',lw=lw,label='ammonium')
plt.xlabel('ammonium [mmol-N/m3]')
plt.ylim(-0.16,0)
#xlim(0,6000)

ax2 = set_subaxes(fig,ax,level=1)
plt.plot(no3[tidx,:,0,0],-zz,'r-',lw=lw,label='nitrate')
plt.xlabel('nitrate [mmol-N/m3)',color='r')
plt.ylim(-0.16,0)
#xlim(0,30)

ax3 = set_subaxes(fig,ax,level=2)
plt.plot(pdet[tidx,:,0,0]*pori/1000.,-zz,'g-',lw=lw,label='POP')
plt.xlabel('POP [mol-P/m3]',color='g')
plt.ylim(-0.16,0)
#xlim(100,220)

ax35 = set_subaxes(fig,ax,level=3)
plt.plot(po4[tidx,:,0,0],-zz,'b-',lw=lw,label='phosphate')
plt.xlabel('phosphate [mmol-P/m3]',color='b')
plt.ylim(-0.16,0)

plt.ylabel('z [cm]')
plt.title('sediment state at '+str(time[tidx])+'\n')
plt.grid(axis='y')

#axp = fig.add_axes([0.6,0.3,0.3,0.65])
#plot(po4[tidx,:,0,0],-zz,'k-',lw=lw,label='phosphate')
#xlabel('phosphate [mmol-P/m3]')
#ylim(-0.16,0)
#xlim(0,50)

axp = fig.add_axes([0.6,0.3,0.3,0.65])
plt.plot(odu[tidx,:,0,0],-zz,'k-',lw=lw,label='reduced substances')
plt.xlabel('reduced substanced [mmol-O2/m3]')
plt.ylim(-0.16,0)
#xlim(0,30000)

ax4 = set_subaxes(fig,axp,level=1)
plt.plot(o2[tidx,:,0,0],-zz,'b-',lw=lw,label='oxygen')
plt.xlabel('ogygen [mmol-O2/m3)',color='b')
plt.ylim(-0.16,0)
#xlim(0,180)

ax5 = set_subaxes(fig,axp,level=2)
plt.plot(denit[tidx,:,0,0]*pori,-zz,color='orange',lw=lw,label='denitrification')
plt.xlabel('denitrification [mmol-N/m3/d)',color='orange')
plt.ylim(-0.16,0)
#xlim(0,80)
plt.grid(axis='y')


#show()
plt.savefig('sediment_state_'+tstr[:10]+'.png',dpi=300)










