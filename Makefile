# This Makefile is part of MOSSCO
#
# @copyright (C) 2017 Helmholtz-Zentrum Geesthacht
# @author Carsten Lemmen, Helmholtz-Zentrum Geesthacht
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#

export MOSSCO_DATE=$(shell date "+%Y%m%d")

SUBDIRS = external

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

archive:
	@git archive --format=tar.gz --prefix=mossco-setups-$(MOSSCO_DATE)/ HEAD > $(MOSSCO_SETUPDIR)/../mossco-setups-$(MOSSCO_DATE).tar.gz
ifdef MOSSCO_SF_USER
	rsync -e ssh -t $(MOSSCO_SETUPDIR)/../mossco-setups-$(MOSSCO_DATE).tar.gz $(MOSSCO_SF_USER)@frs.sf.net:/home/pfs/p/mossco/Snapshots/
else
	@echo "To upload, set the environment variable MOSSCO_SF_USER to your sourceforge user name."
endif
