from pylab import *
import netCDF4
import sys


varn  = ['phosphate','nitrate','ammonium','oxygen/10','ODU']
units = [u'mmolP/m\u00b2/d',u'mmolN/m\u00b2/d',u'mmolN/m\u00b2/d',u'mmolO2/m\u00b2/d',u'-mmolO2/m\u00b2/d']
cfnames = ['mole_concentration_of_phosphate_upward_flux', \
           'mole_concentration_of_nitrate_upward_flux',   \
           'mole_concentration_of_ammonium_upward_flux',  \
           'dissolved_oxygen_upward_flux',                \
           'dissolved_reduced_substances_upward_flux']

nc  = netCDF4.Dataset(sys.argv[1])
ncv = nc.variables

time = nc.variables['time'][:]/(86400.0*365.0)

#get variables
fluxes=[]
for i in range(len(varn)):
  fluxes.append(squeeze(ncv[cfnames[i]][:])*-86400.0)

# add oxygen
fluxes[3]=(fluxes[3]-fluxes[4])/10.

dz = squeeze(ncv['layer_height_in_soil'][:])
denit = squeeze(ncv['denitrification_rate_in_soil'][:])

fluxes.append( (dz*denit).sum(axis=1) )

fig=figure(figsize=(8,8))
for i in range(len(varn)-1):

    plot(time,fluxes[i][:],lw=1.5,label=varn[i]+' -> %5.2f [%s]'%(fluxes[i][-1],units[i]))

plot(time,-fluxes[-1][:],lw=1.5,label='denitrification -> %5.2f [%s]'%(fluxes[-1][-1],u'mmolN/m\u00b2/d'))

ylim(-2,2)
xlim(0,10)
ylabel(u'fluxes [mmol/m\u00b2/d]')
xlabel('years')
legend()

show()
