from pylab import *
import sys
import netCDF4
from matplotlib.collections import PolyCollection
import numpy as np

def read_ugrid_ncdf(ncfile):
  nc = netCDF4.Dataset(ncfile,'r')
  ncv = nc.variables
  
  
  for var in nc.variables:
      
      try:
        node_connectivity=ncv[var].face_node_connectivity
      except:
        pass

      try:
        node_coordinates=ncv[var].node_coordinates
      except:
        pass

  print 'Found node connectivity in ' + node_connectivity
  print 'Node coordinates are ' + node_coordinates  
          
  nv  = np.squeeze(ncv[node_connectivity][:])  
  lon = np.squeeze(ncv[node_coordinates.split()[0]][:])  
  lat = np.squeeze(ncv[node_coordinates.split()[1]][:])
  try:
    H = squeeze(ncv['bathymetry'][:])
  except:
    H = 12.3*ones((nv.shape[0],))  

  nc.close()

  return nv,lon,lat,H


if __name__ == '__main__':

  if len(sys.argv) < 2:
      filename = '/Users/lemmen/devel/GIS/Triangle/topo.1_ugrid.nc'
      filename = '/Users/lemmen/devel/ESMF/esmf-code/src/Superstructure/PreESMFMod/tests/data/BT42_ugrid.nc'
      filename = '/Users/lemmen/devel/lutes/data/north_america/outline.2_ugrid.nc'
      filename = '/Users/lemmen/devel/MOSSCO/omex/src/test/ugrid_sediment.nc'
      filename = '/Users/lemmen/devel/mossco/setups/regrid/getmriver_ugrid.nc'
      filename = '/Users/lemmen/devel/mossco/setups/regrid/ices_boxes_ugrid.nc'
  else:
      filename = sys.argv[1]

  nv, lon, lat, H = read_ugrid_ncdf(filename)
  first_index=0

  fig=figure(figsize=(8,10))
  ax=gca()
  verts = []
  
  # Loop over all elements
  for i in range(0,nv.shape[0]):
      #plot(lon[nv[i,[0]-1],lat[nv[i,:]-1],'r-')
      #print nv[i,:]
      ivalid=np.where(nv[i,:]>=first_index)[0]
      x=lon[nv[i,ivalid]].tolist()
      x.append(lon[nv[i][0]-first_index])
      y=lat[nv[i,ivalid]].tolist()
      y.append(lat[nv[i][0]-first_index])
      print i,x
      plot (x,y)
      for j in range(0,ivalid.size):
          text(x[j],y[j],str(nv[i,ivalid[j]]))
      verts.append(zip(x,y))

  coll = PolyCollection(verts, array=H, cmap=cm.jet, edgecolors='none')
  ax.add_collection(coll)
  ax.autoscale_view()
  ylabel('Latitude')
  xlabel('Longitude')
  colorbar(coll)

#  ax=axes([bb.xmin,0.05,bb.xmax-bb.xmin,bb.ymax-bb.ymin])
#  plot(time,16*fluxes[:,3],'r-',lw=2.0,label='16* DIP flux')
#  plot(time,fluxes[:,4]+fluxes[:,5],'k-',lw=2.0,label='DIN flux')
#  plot(time,-16*fluxes[:,2],'r-',lw=1.0,label='-16* POP flux')
#  legend(frameon=False)
  #ylim(-2,0.1)

  show()
