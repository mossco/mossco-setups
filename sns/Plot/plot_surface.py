from pylab import *
from mpl_toolkits.basemap import Basemap
import os,sys
import numpy as np
import pickle
import netCDF4
import netcdftime

# set map boundaries
lonb=[-1., 10.]
latb=[51., 55.5]

# try to load projection object, if not present
# re-initiate projection object
# and save as proj.pickle for later usage
if os.path.isfile('proj.pickle'):
    (proj,)=np.load('proj.pickle')
else:
    proj=Basemap(projection="merc", lat_ts=0.5*(latb[1]+latb[0]), resolution="h",llcrnrlon=lonb[0], urcrnrlon=lonb[1], llcrnrlat=latb[0], urcrnrlat=latb[1])
    f=open('proj.pickle','wb')
    pickle.dump((proj,),f)
    f.close()

# get commandline options:
#  python plot_surface.py ncfile var1,var2 minval,maxval timestep_start-timstep_end
if len(sys.argv)>1:
    ncfile=sys.argv[1]
else:
    ncfile='cut_stiched.nc'


if len(sys.argv)>2:
    varns=sys.argv[2].split(',')
else:
    varns=['Chl_chl_in_water']

if len(sys.argv)>3:
    minmax=sys.argv[3].split(',')
else:
    minmax=[]

# open data file and create variables object
print(ncfile)
nc=netCDF4.Dataset(ncfile)
ncv=nc.variables

# time-related issues
# read time as python datetime object
tv = ncv['time']
utime=netcdftime.utime(tv.units)
time = utime.num2date(tv[:])
tnum=len(time)

if len(sys.argv)>4:
    asplit=sys.argv[4].split('-')
    if len(asplit)>1:
        tmin=int(asplit[0])
        tmax=int(asplit[1])
    else:
        tmin=int(sys.argv[4])
        tmax=tmin+1
else:
    tmin=0
    tmax=tnum

# get coordinate names
coords = ncv[varns[0]].coordinates.split(' ')
#lonname = coords[0]
#latname = coords[1]
lonname = coords[-1]
latname = coords[-2]

# read coodinates in the range
#  specific to the gb_curv setup
lons=ncv[lonname][:,:]
lats=ncv[latname][:,:]

# central plotting switches (CHANGE_HERE):
dpi=96
xx,yx = proj(lons,lats)

for t in range(tmin,tmax):
    
    for varn in varns:
      
      var=squeeze(ncv[varn][t,-1,:,:])
      short=varn
      cbtitle=varn+'\n[%s]\n'%(ncv[varn].units)
      os.system('mkdir -p %s'%short)

      f=figure(figsize=(9,6),dpi=dpi)
      f.subplots_adjust(left=0.0,right=1.0,bottom=0.0,top=1.0)

      pcf=proj.pcolormesh(xx,yx,var,cmap=cm.jet)
      if len(minmax)==2:
         clim(minmax[0],minmax[1])

      proj.drawcoastlines()
      proj.fillcontinents((0.8,0.9,0.8))

      xt,yt=proj(-0.5,55.3)
      text(xt,yt,str(time[t]),size=12.)

      cax=axes([0.8,0.06,0.02,0.3])
      colorbar(pcf,cax=cax)
      cax.set_title(cbtitle,size=10.)

      savefig('%s/%s_%04d.png'%(short,short,t),dpi=dpi)
      close()
