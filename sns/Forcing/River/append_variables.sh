#!/bin/bash

vars="Phytplankton_Nitrogen_phyN_flux_in_water Phytplankton_Carbon_phyC_flux_in_water Phytplankton_Phosphorus_phyP_flux_in_water fraction_of_Rubisco_Rub_flux_in_water Chl_chl_flux_in_water Zooplankton_Carbon_zooC_flux_in_water Phytplankton_Silicon_phyS_flux_in_water "
#default concentration is 0.0
conc=0.0

ncapstr=""

for var in $vars ; do

  if [ "$var" == "Phytplankton_Nitrogen_phyN_flux_in_water" ]; then
    conc=0.16
  fi
  if [ "$var" == "Phytplankton_Silicon_phyS_flux_in_water" ]; then
    conc=0.16
  fi
  if [ "$var" == "Phytplankton_Carbon_phyC_flux_in_water" ]; then
    conc=1.06
  fi
  if [ "$var" == "Phytplankton_Phosphorus_phyP_flux_in_water" ]; then
    conc=0.01
  fi
  if [ "$var" == "fraction_of_Rubisco_Rub_flux_in_water" ]; then
    conc=0.20
  fi
  if [ "$var" == "Chl_chl_flux_in_water" ]; then
    conc=0.20
  fi
  if [ "$var" == "Zooplankton_Carbon_zooC_flux_in_water" ]; then
    conc=0.01
  fi

  echo "  $var = $conc"

  ncapstr="$ncapstr $var=$conc*volume_flux_in_water;"

done

echo " "
echo "In order to add the variables, please run the following command:"
echo "(You would have to set no_river_dilution=.false. for these variables)"
echo "---"
echo "ncap -s \"$ncapstr\" river_grid_fluxes.nc river_grid_fluxes_phy.nc"

