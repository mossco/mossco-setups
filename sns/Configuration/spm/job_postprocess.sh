#!/bin/bash
#

touch stop_mossco
RunDir=`pwd`

this='job_postprocess.sh'
executable=$1
NP=$2

#---------------------------------
On=1
Off=0
#---------------------------------

SED=${SED:-$(which gsed 2> /dev/null )}
SED=${SED:-$(which sed 2> /dev/null )}

function doesAnyFileExist {
   local arg="$*"
   local files=($arg)
   [ ${#files[@]} -gt 1 ] || [ ${#files[@]} -eq 1 ] && [ -e "${files[0]}" ]
}


# get start-/stopdate from mossco_run.nml
starttime=$(cat mossco_run.nml | grep start | awk -F"'" '{print $2}')
stoptime=$( cat mossco_run.nml | grep stop  | awk -F"'" '{print $2}')
startdate=$(echo $starttime | sed 's/\T/ /g' | awk -F" " '{print $1}')
stopdate=$( echo $stoptime  | sed 's/\T/ /g' | awk -F" " '{print $1}')

# check wheter dates fit to netcdf data
source=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
pattern=${source}"*.nc"
if doesAnyFileExist $pattern; then

  NF=`ls ${pattern} | grep -v '.2d.' | wc -l`
  if [[ "$NF" -ne "$NP" ]]; then
    echo '======================================================'
    echo ' ERROR: number of files does not match $NP '
    echo '======================================================'
    exit
  fi

  if [[ "$NP" -ge 10 ]]; then
    file=${source}.00.nc
  else
    if [[ "$NP" -gt 1 ]]; then
      file=${source}.0.nc
    else
      file=${source}.nc
    fi
  fi

  ncstarttime=$(cdo -s showtimestamp ${file} | awk '{print $1}'  | sed 's/\T/ /g')
  ncstoptime=$( cdo -s showtimestamp ${file} | awk '{print $NF}' | sed 's/\T/ /g')
  #if [[ $starttime ]]; then ncstarttime=$( echo ${ncstarttime} | sed 's/\T/ /g'); fi
  #if [[ $stoptime ]]; then ncstoptime=$( echo ${ncstoptime} | sed 's/\T/ /g'); fi
  starttime=$( echo ${starttime} | sed 's/\T/ /g')
  stoptime=$(  echo ${stoptime}  | sed 's/\T/ /g')
  if [[ $starttime != $ncstarttime ]] || [[ $stoptime != $ncstoptime  ]]; then
    echo '======================================================'
    echo ' ERROR: dates found in mossco_run.nml don''t match '
    echo '        dates found in netcdf file !'
    echo ' start-/stoptime:     '$starttime $stoptime
    echo ' ncstart-/ncstoptime: '$ncstarttime $ncstoptime
    echo '======================================================'
    exit
  fi
  timestring=$( cdo -s showtimestamp ${file} | awk '{print $NF}')

else
  echo '======================================================'
  echo ' ERROR: no files found matching pattern '$pattern
  echo '======================================================'
  exit
fi

# year is defined by stopdate
year=$(echo $stopdate | awk -F"-" '{print $1}')
# adjust year if stopdate is new year's day
mm=$(echo $stopdate | awk -F"-" '{print $2}')
dd=$(echo $stopdate | awk -F"-" '{print $3}')
if [[ $mm == '01' && $dd == '01' ]]; then
  ((year--))
fi
#echo $starttime $stoptime $startdate $stopdate $year $mm $dd; exit

# check if current year folder exists
if [ -d $year ]; then

  # check if any subfolder exists
  nbdir=0
  for f in $year/*; do
    if [ -d $f ] ; then
      ((nbdir++))
    fi
  done

  if [[ $nbdir > 0 ]]; then
    # get previous result folder
    counter=1
    printf -v jID "%02d" $counter
    while [[ -d $year/$jID ]]; do
      printf -v jID "%02d" $counter
      ((counter++))
    done
    resultDir=$year/$jID
  else
    # check whether subfolders will be required
    mm=$(echo $stopdate | awk -F"-" '{print $2}')
    dd=$(echo $stopdate | awk -F"-" '{print $3}')
    if [[ $mm == '01' && $dd == '01' ]]; then
      resultDir=$year
    else
      resultDir=$year/01
    fi
  fi
else
  # check whether subfolders will be required
  mm=$(echo $stopdate | awk -F"-" '{print $2}')
  dd=$(echo $stopdate | awk -F"-" '{print $3}')
  if [[ $mm == '01' && $dd == '01' ]]; then
    resultDir=$year
  else
    resultDir=$year/01
  fi
fi

if [[ ! -d $resultDir ]]; then
  echo '------------------------------------------------------'
  echo ' create folder '$resultDir' for results'
  echo '------------------------------------------------------'
  mkdir -p $resultDir
fi


pattern="PET*"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
pattern="*.stderr"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
pattern="*.stdout"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi

#"GETM-native" output
pattern="*.2d.????.nc"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
pattern="*.3d.????.nc"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
pattern="restart.*.out"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
# MOSSCO output
pattern=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1"*.nc"}')
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi
pattern="restart_out*.nc"
if doesAnyFileExist $pattern; then
  echo ' move files '$pattern
  mv $pattern $resultDir/.
fi

if [[ -f $executable ]]; then
  cp $executable $resultDir/.
fi
pattern="*.nml"
if doesAnyFileExist $pattern; then
  echo ' copy files '$pattern
  cp $pattern $resultDir/.
fi
pattern="*.cfg"
if doesAnyFileExist $pattern; then
  echo ' copy files '$pattern
  cp $pattern $resultDir/.
fi

pattern="slurm-?????.out"
if doesAnyFileExist $pattern; then
  echo ' copy files '$pattern
  mv $pattern $resultDir/.
fi

#cp *.inp $resultDir/.
#cp *.dat $resultDir/.
#cp *.txt $resultDir/.

echo '------------------------------------------------------'
echo ' run extract_restart.sh for '$executable' '$timestring

cd $resultDir
bash $RunDir/extract_restart.sh $executable $timestring > extract_restart.log
if [[ -f stop_mossco ]]; then exit; fi
cd $RunDir

echo '------------------------------------------------------'
echo ' INFO: '$this' teminated regularly !'
echo '------------------------------------------------------'
if [[ -f stop_mossco ]]; then rm stop_mossco; fi
exit
