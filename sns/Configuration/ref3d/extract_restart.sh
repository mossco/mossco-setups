#!/bin/bash
#

touch stop_mossco

this='extract_restart.sh'
executable=$1
timestring=$2

#---------------------------------
On=1
Off=0
#---------------------------------

SED=${SED:-$(which gsed 2> /dev/null )}
SED=${SED:-$(which sed 2> /dev/null )}

function doesAnyFileExist {
   local arg="$*"
   local files=($arg)
   [ ${#files[@]} -gt 1 ] || [ ${#files[@]} -eq 1 ] && [ -e "${files[0]}" ]
}

if [[ ! -f mossco_${executable}.cfg ]]; then
  echo '======================================================'
  echo ' ERROR: missing file: mossco_'$executable'.cfg !'
  echo '======================================================'
  exit
fi

startdate=$(echo $timestring | awk -F"T" '{print $1}')
starthour=$(echo $timestring | awk -F"T" '{print $2}')
if [[ 'x'$startdate == 'x' || 'x'$starthour == 'x' ]]; then
  echo "###################################################################"
  echo " ERROR : bad formated startstring: "$timestring
  echo "         please use format yyyy-mm-ddThh:mm:ss"
  echo "###################################################################"
  exit
fi

NF=0
#source=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
source=$(cat restart_out.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
if doesAnyFileExist "${source}*.nc"; then
  NF=`ls ${source}*.nc | grep -v '.2d.' | wc -l`
fi

if [[ "$NF" -lt 1 ]]; then
  echo '======================================================'
  echo ' ERROR: no files found matching pattern '$source'*.nc'
  echo '======================================================'
  exit
fi

if [[ -f _restart_water.cfg ]]; then mv _restart_water.cfg restart_water.cfg; fi
if [[ -f _restart_soil.cfg ]]; then mv _restart_soil.cfg restart_soil.cfg; fi
if [[ -f _restart_erosed.cfg ]]; then mv _restart_erosed.cfg restart_erosed.cfg; fi

# fabm_pelagic
if [[ -f restart_water.cfg ]]; then

  echo '------------------------------------------------------'
  echo ' creating restart files for fabm_pelagic'
  echo '------------------------------------------------------'

  fabm_pelagic_prefix=$(cat restart_water.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
  pattern=$fabm_pelagic_prefix"*.nc"
  if doesAnyFileExist $pattern; then
      echo '... remove existing files '$pattern
      rm $pattern
  fi

  #source=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
  source=$(cat restart_out.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
  if doesAnyFileExist "${source}*.nc"; then

    vars='*_in_water'
    #vars='none'

    counter=0
    for i in ${source}*.nc ; do
      if [[ "$NF" -gt 1 ]]; then
        printf -v target $fabm_pelagic_prefix".%04d.nc" $counter
      else
        printf -v target $fabm_pelagic_prefix".nc" $counter
      fi

      #echo 'debug: cdo -selname,'${vars}' -setreftime,'${startdate}','${starthour}',seconds -seldate,'${timestring}' '$i' '$target
      cdo -selname,${vars} -setreftime,${startdate},${starthour},seconds -seldate,${timestring} $i $target
      if [[ -f $target ]]; then
        time_unit='seconds since '${startdate}' '${starthour}
        #echo $time_unit
        echo "ncatted -O -h -a units,time,o,c,$time_unit $target"
        ncatted -O -h -a units,time,o,c,"${time_unit}" $target
        #echo 'debug: cdo -selname,'${vars}' -seldate,'${timestring}' '$i' '$target
        #cdo -selname,${vars} -seldate,${timestring} $i $target
      else
        break
      fi
      ((counter++))
    done

    echo '------------------------------------------------------'
    echo ' tag restart files by md5sum'
    echo '------------------------------------------------------'
    md5sum $fabm_pelagic_prefix*.nc

  fi

else
  echo '======================================================'
  echo ' WARNING: missing file: restart_water.cfg !'
  echo '======================================================'
  #exit
fi

# soil
if [[ -f restart_soil.cfg ]]; then

  echo '------------------------------------------------------'
  echo ' creating restart files for soil'
  echo '------------------------------------------------------'

  soil_prefix=$(cat restart_soil.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
  pattern=$soil_prefix"*.nc"
  if doesAnyFileExist $pattern; then
      echo '... remove existing files '$pattern
      rm $pattern
  fi

  #source=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
  source=$(cat restart_out.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
  if doesAnyFileExist "${source}*.nc"; then

    vars='*_in_soil'
    #vars='none'

    counter=0
    for i in ${source}*.nc ; do
      if [[ "$NF" -gt 1 ]]; then
        printf -v target $soil_prefix".%04d.nc" $counter
      else
        printf -v target $soil_prefix".nc" $counter
      fi
      #echo 'debug: cdo -selname,'${vars}' -setreftime,'${startdate}','${starthour}',seconds -seldate,'${timestring}' '$i' '$target
      cdo -selname,${vars} -setreftime,${startdate},${starthour},seconds -seldate,${timestring} $i $target
      if [[ -f $target ]]; then
        time_unit='seconds since '${startdate}' '${starthour}
        #echo $time_unit
        echo "ncatted -O -h -a units,time,o,c,$time_unit $target"
        ncatted -O -h -a units,time,o,c,"${time_unit}" $target
        #echo 'debug: cdo -selname,'${vars}' -seldate,'${timestring}' '$i' '$target
        #cdo -selname,${vars} -seldate,${timestring} $i $target
      else
        break
      fi
      ((counter++))
    done

    echo '------------------------------------------------------'
    echo ' tag restart files by md5sum'
    echo '------------------------------------------------------'
    md5sum $soil_prefix*.nc

  fi

else
  echo '======================================================'
  echo ' WARNING: missing file: restart_soil.cfg !'
  echo '======================================================'
  #exit
fi

# erosed
if [[ -f restart_erosed.cfg ]]; then

  echo '------------------------------------------------------'
  echo ' creating restart files for erosed'
  echo '------------------------------------------------------'

  erosed_prefix=$(cat restart_erosed.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
  pattern=$erosed_prefix"*.nc"
  if doesAnyFileExist $pattern; then
      echo '... remove existing files '$pattern
      rm $pattern
  fi

  #source=$(cat mossco_${executable}.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
  source=$(cat restart_out.cfg | grep filename | awk '{print $2}' | awk -F".nc" '{print $1}')
  if doesAnyFileExist "${source}*.nc"; then

    vars='sediment_mass_in_bed'
    #vars='none'

    counter=0
    for i in ${source}*.nc ; do
      if [[ "$NF" -gt 1 ]]; then
        printf -v target $erosed_prefix".%04d.nc" $counter
      else
        printf -v target $erosed_prefix".nc" $counter
      fi
      #echo 'debug: cdo -selname,'${vars}' -setreftime,'${startdate}','${starthour}',seconds -seldate,'${timestring}' '$i' '$target
      cdo -selname,${vars} -setreftime,${startdate},${starthour},seconds -seldate,${timestring} $i $target
      if [[ -f $target ]]; then
        time_unit='seconds since '${startdate}' '${starthour}
        #echo $time_unit
        echo "ncatted -O -h -a units,time,o,c,$time_unit $target"
        ncatted -O -h -a units,time,o,c,"${time_unit}" $target
        #echo 'debug: cdo -selname,'${vars}' -seldate,'${timestring}' '$i' '$target
        #cdo -selname,${vars} -seldate,${timestring} $i $target
      else
        break
      fi
      ((counter++))
    done

    echo '------------------------------------------------------'
    echo ' tag restart files by md5sum'
    echo '------------------------------------------------------'
    md5sum $erosed_prefix*.nc

  fi

else
  echo '======================================================'
  echo ' WARNING: missing file: restart_erosed.cfg !'
  echo '======================================================'
  #exit
fi

echo '------------------------------------------------------'
echo ' INFO: '$this' teminated regularly !'
echo '------------------------------------------------------'
if [[ -f stop_mossco ]]; then rm stop_mossco; fi
exit

