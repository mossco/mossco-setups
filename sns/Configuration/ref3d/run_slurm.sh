#!/bin/bash
#
#---------------------------------------
# parallel job on kronos
#---------------------------------------
#
#SBATCH --job-name=rXXX            # Job name
#
#SBATCH --wckey=B3955.02.04.70224  # Projektnummer
#SBATCH --partition=blades         # Run the job on this partition
#SBATCH --ntasks=1                 # Number of Processes
#SBATCH --nodes=1                  # Number of Nodes, max. 250; 256 -> 260 Metrik: 20: -> 13
##SBATCH --ntasks-per-node=1        # Processes Per Node
#SBATCH --exclusive                # Use the nodes exclusive
#
#---------------------------------------

function doesAnyFileExist {
   local arg="$*"
   local files=($arg)
   [ ${#files[@]} -gt 1 ] || [ ${#files[@]} -eq 1 ] && [ -e "${files[0]}" ]
}

source $HOME/.mossco_gcc720

submit_cmd=sbatch
#submit_cmd=bash; SLURM_JOB_NAME='rXXX'; SLURM_NTASKS=1

this='run_slurm.sh'
MPI_PREFIX='mpirun'
EXE='ref3d'
NP=$SLURM_NTASKS

if [[ "x${SLURM_JOB_NAME}" != "x" ]] ; then
  RunID=${SLURM_JOB_NAME}
else
  RunID=$1
fi
title=$RunID'_sns_'$EXE
yearStart=2000
yearEnd=2000
increment=1m

#---------------------------------
On=1
Off=0
StartAgain=$On
ReadYear=$On
ReadMonth=$On
Warmstart=$Off
SimpleJob=$Off
#---------------------------------

SED=${SED:-$(which gsed 2> /dev/null )}
SED=${SED:-$(which sed 2> /dev/null )}

if [[ "x${SLURM_SUBMIT_DIR}" != "x" ]] ; then
  RunDir=${SLURM_SUBMIT_DIR}
else
  RunDir=`pwd`
fi
echo  ${RunDir}
cd ${RunDir}
if doesAnyFileExist "PET*"; then rm PET*; fi

# prepare job
if [[ -f stop_mossco ]]; then rm stop_mossco; fi
# - set run year
if [[ -e ${RunDir}/Year.${RunID} && ${ReadYear} ]]; then
  year=$(cat ${RunDir}/Year.${RunID})
else
  year=${yearStart}
fi

# - set run month
if [[ -e ${RunDir}/Month.${RunID} && ${ReadMonth} ]]; then
  number=$(cat ${RunDir}/Month.${RunID})
  mm=${number#${number%%[!0]*}}
  month=$( printf '%02d' $(( $mm )) )
else
  month='01'
fi

echo '------------------------------------------------------'
echo ' INFO: '$this' for RunID '$RunID' and '${month}'/'${year}' started at '
echo '       '$(date)
echo '------------------------------------------------------'

if [[ ${increment} == "1m" ]]; then
  nextYear=$year
  mm=${month#${month%%[!0]*}}
  nextMonth=$( printf '%02d' $(( $mm+1 )) )
  if [[ ${nextMonth} == 13 ]]; then
    nextYear=$(( $year+1 ))
    nextMonth='01'
  fi
else
  nextYear=$(( $year+1 ))
  nextMonth='01'
fi
startstring=$year'-'$month'-01T00:00:00'
stopstring=$nextYear'-'$nextMonth'-01T00:00:00'

# adjust timestrings in mossco_run.nml
if [[ ! -x ${SED} ]]; then
  echo
  echo "ERROR: Cannot execute the sed program $SED"
  exit
fi

if [[ -f mossco_run.nml ]]; then
  if [[ "x${title}" != "x" ]] ; then ${SED} -i "s/title *=.*/title = '${title}',/" mossco_run.nml; fi
  if [[ "x${startstring}" != "x" ]] ; then ${SED} -i "s/start *=.*/start = '${startstring}',/" mossco_run.nml; fi
  if [[ "x${stopstring}" != "x" ]]  ; then ${SED} -i "s/stop *=.*/stop  = '${stopstring}',/" mossco_run.nml;  fi
else
  echo
  echo "ERROR: Cannot find mossco_run.nml"
  exit
fi

name=mossco_${EXE}.cfg
if [[ -f ${name} ]]; then
  if [[ "x${title}" != "x" ]] ; then ${SED} -i "s/filename.*/filename: ${title}.nc/" ${name} ; fi
else
  echo
  echo "ERROR: Cannot find "${name}
  exit
fi

if [[ -f getm.inp ]]; then
  if [[ "x${title}" != "x" ]] ; then ${SED} -i "s/runid *=.*/runid = '${title}',/" getm.inp; fi
fi

# link restart files
if [[ ${Warmstart} == ${On} ]]; then
  #${SED} -i "s/hotstart*=*.false.*/hotstart = .true.,/" getm.inp
  ${SED} -i "0,/hotstart =/{s/hotstart = .*/hotstart = .true.,/}" getm.inp
  if [[ -f _restart_water.cfg ]]; then mv _restart_water.cfg restart_water.cfg; fi
  if [[ -f _restart_soil.cfg ]]; then mv _restart_soil.cfg restart_soil.cfg; fi
  if [[ -f _restart_erosed.cfg ]]; then mv _restart_erosed.cfg restart_erosed.cfg; fi
  if [[ ${SimpleJob} == ${Off} ]]; then
    echo ' - link input data for '${startstring}
    #echo 'DUBUG: bash link_restart_files.sh '${EXE}' '${NP}' '${startstring}
    bash link_restart_files.sh ${EXE} ${NP} ${startstring}
    if [[ -f stop_mossco ]]; then rm stop_mossco; exit; fi
  fi
else
  echo ' - running coldstart'
  #${SED} -i "s/hotstart*=*.true.*/hotstart = .false.,/" getm.inp
  ${SED} -i "0,/hotstart =/{s/hotstart = .*/hotstart = .false.,/}" getm.inp
  if [[ -f restart_water.cfg ]]; then mv restart_water.cfg _restart_water.cfg; fi
  if [[ -f restart_soil.cfg ]]; then mv restart_soil.cfg _restart_soil.cfg; fi
  if [[ -f restart_erosed.cfg ]]; then mv restart_erosed.cfg _restart_erosed.cfg; fi
fi

# track md5sum
echo ""
echo "#############################################################"
find -maxdepth 1 -type l \( -iname "*.*" \) -print0  | xargs -0 md5sum

# run model
sim_starttime=$(date +%s)
echo ""
echo "#############################################################"
echo "#### $EXE started at $(date) "
if [ $NP -gt 1 ] ; then
    echo "#### job running on $NP threads "
else
    echo "#### job running on $NP thread "
fi
echo "#############################################################"

cd ${RunDir}

#echo ${MPI_PREFIX}' -n '${NP}' '${EXE}
${MPI_PREFIX} -n ${NP} ${EXE}
#${MPI_PREFIX} ${EXE}

cd $SLURM_SUBMIT_DIR
echo "#############################################################"
echo 'Working Directory     : '$SLURM_SUBMIT_DIR
echo 'Job-ID                : '$SLURM_JOB_ID
echo 'Job-Name              : '$SLURM_JOB_NAME
echo 'Shell                 : '$SHELL
#echo ''
echo 'Nodes allocated       : '$SLURM_JOB_NUM_NODES
echo '# of cores/node       : '$SLURM_CPUS_ON_NODE
echo 'Total number of cores : '$SLURM_NTASKS
echo 'nodelist              : '$SLURM_JOB_NODELIST

sim_endtime=$(date +%s)
sim_time=$((sim_endtime-sim_starttime))

echo "#############################################################"
echo "#### $EXE finished at $(date) "
echo "#### job completed after $sim_time seconds "
echo "#############################################################"
echo ""

# postprocess job
if [[ ${SimpleJob} == ${Off} ]]; then
  echo ' - do postprocessing and prepare next job'
  #echo 'bash job_postprocess.sh '${EXE}' '${NP}
  bash job_postprocess.sh ${EXE} ${NP} #> job_postprocess_${year}.log
  if [[ -f stop_mossco ]]; then exit; fi
fi

# prepare and submit next job
if [[ ${StartAgain} == ${On}  && ${nextYear} -le ${yearEnd} ]]; then
  cd ${RunDir}
  #if [[ -f $this ]]; then ${SED} -i "s/Warmstar=\$Off/Warmstar=\$On/" $this; fi
  if [[ -f $this ]]; then ${SED} -i "s/Warmstart=\$Off/Warmstart=\$On/" $this; fi
  if [[ -f Year.$RunID ]]; then rm Year.$RunID; fi
  if [[ -f Month.$RunID ]]; then rm Month.$RunID; fi
  if [[ ${increment} == "1m" ]]; then echo ${nextMonth} > Month.$RunID; fi
  #echo ${nextYear}' > Year.'$RunID
  echo ${nextYear} > Year.$RunID
  #cat Year.$RunID

  if [[ ${submit_cmd} == "bash" ]]; then
    echo ${submit_cmd}' '${this}' > run_'${startstring}'-'${stopstring}'.log'
    $submit_cmd $this > run_${startstring}-${stopstring}.log &
  else
    $submit_cmd $this
  fi

  if [[ ${increment} == "1m" ]]; then
    echo ' >> next job for year '${nextMonth}'/'${nextYear}' submitted !'
  else
    echo ' >> next job for year '${nextYear}' submitted !'
  fi
fi

echo '------------------------------------------------------'
echo ' INFO: '$this' teminated regularly !'
echo '       '$(date)
echo '------------------------------------------------------'
exit
