#!/bin/bash
#

touch stop_mossco

this='link_restart_files.sh'
executable=$1
NP=$2
timestring=$3

#---------------------------------
On=1
Off=0
#---------------------------------

SED=${SED:-$(which gsed 2> /dev/null )}
SED=${SED:-$(which sed 2> /dev/null )}

function doesAnyFileExist {
   local arg="$*"
   local files=($arg)
   [ ${#files[@]} -gt 1 ] || [ ${#files[@]} -eq 1 ] && [ -e "${files[0]}" ]
}

startdate=$(echo $timestring | awk -F"T" '{print $1}')
starthour=$(echo $timestring | awk -F"T" '{print $2}')
if [[ 'x'$startdate == 'x' || 'x'$starthour == 'x' ]]; then
  echo "###################################################################"
  echo " ERROR : bad formated startstring: "$timestring
  echo "         please use format yyyy-mm-ddThh:mm:ss"
  echo "###################################################################"
  touch stop_mossco
  exit
fi

# get current year
year=$(echo $startdate | awk -F"-" '{print $1}')

# check if previous year is required
mm=$(echo $startdate | awk -F"-" '{print $2}')
dd=$(echo $startdate | awk -F"-" '{print $3}')
if [[ $mm == '01' && $dd == '01' ]]; then
  ((year--))
fi

#echo $startdate $year $mm $dd; exit

# check if current year folder exists
if [ -d $year ]; then

  # check if any subfolder exists
  nbdir=0
  for f in $year/*; do
    if [ -d $f ] ; then
      ((nbdir++))
    fi
  done

  if [[ $nbdir > 0 ]]; then
    # get previous result folder
    counter=1
    printf -v jID "%02d" $counter
    while [[ -d $year/$jID ]]; do
      ((counter++))
      printf -v jID "%02d" $counter
      #echo $counter $jID
    done
    ((counter--))
    printf -v jID "%02d" $counter
    prevJob=$year/$jID
  else
    prevJob=$year
  fi

else
  echo '======================================================'
  echo ' ERROR: folder with restart files missing!'
  echo '======================================================'
  exit

fi

if [[ -d ${prevJob} ]]; then

  # getm restart
  echo '------------------------------------------------------'
  echo '-linking restart files for getm'
  echo '------------------------------------------------------'

  pattern='restart.*.in'
  if doesAnyFileExist $pattern; then
    echo '... remove existing files '$pattern
    rm $pattern
  fi
  if doesAnyFileExist "${prevJob}/restart.*.out"; then
    NF=`ls ${prevJob}/restart.*.out | wc -l`
    if [[ $NP == $NF ]]; then
      counter=0
      for i in $prevJob/restart.*.out ; do
        printf -v target "restart.%04d.in" $counter
        ln -s $i $target
        ((counter++))
      done
      md5sum restart.*.in
    else
      echo '======================================================'
      echo ' ERROR: number of restart files does not match nproc !'
      echo '======================================================'
      exit
    fi
  fi

  if [[ -f _restart_water.cfg ]]; then mv _restart_water.cfg restart_water.cfg; fi
  if [[ -f _restart_soil.cfg ]]; then mv _restart_soil.cfg restart_soil.cfg; fi
  if [[ -f _restart_erosed.cfg ]]; then mv _restart_erosed.cfg restart_erosed.cfg; fi

  # fabm_pelagic
  if [[ -f restart_water.cfg ]]; then

    echo '------------------------------------------------------'
    echo '-linking restart files for fabm_pelagic'
    echo '------------------------------------------------------'

    fabm_pelagic_prefix=$(cat restart_water.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
    if [[ "$NP" -eq 1 ]]; then
      pattern=$fabm_pelagic_prefix".nc"
    else
      pattern=$fabm_pelagic_prefix"*.nc"
    fi

    if doesAnyFileExist $pattern; then
        echo '... remove existing files '$pattern
        rm $pattern
    fi

    fname=$(cat restart_water.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
    if doesAnyFileExist "${prevJob}/$fname*.nc"; then
      NF=`ls ${prevJob}/$fname*.nc | wc -l`
      if [[ "$NP" -ne "$NF" ]]; then
        echo '======================================================'
        echo ' ERROR: number of restart files does not match nproc !'
        echo '======================================================'
        exit
      fi

      if [[ "$NP" -gt 1 ]]; then
        counter=0
        for i in $prevJob/$fname.*.nc ; do
          printf -v target $fabm_pelagic_prefix".%04d.nc" $counter
          ln -s $i $target
          ((counter++))
        done
      else
        ln -s ${prevJob}/${fname}.nc ${fabm_pelagic_prefix}.nc
      fi
      md5sum $fabm_pelagic_prefix*.nc

    else
      echo '======================================================'
      echo ' WARNING: fabm_pelagic restart files missing in '$prevJob
      echo '======================================================'
      #exit

    fi

#   else
#     echo '======================================================'
#     echo ' WARNING: missing file: restart_water.cfg !'
#     echo '======================================================'
#     #exit
  fi

  # soil
  if [[ -f restart_soil.cfg ]]; then

    echo '------------------------------------------------------'
    echo '-linking restart files for soil'
    echo '------------------------------------------------------'

    soil_prefix=$(cat restart_soil.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
    if [[ "$NP" -eq 1 ]]; then
      pattern=$soil_prefix".nc"
    else
      pattern=$soil_prefix"*.nc"
    fi

    if doesAnyFileExist $pattern; then
        echo '... remove existing files '$pattern
        rm $pattern
    fi

    fname=$(cat restart_soil.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
    if doesAnyFileExist "${prevJob}/$fname*.nc"; then
      NF=`ls ${prevJob}/$fname*.nc | wc -l`
      if [[ "$NP" -ne "$NF" ]]; then
        echo '======================================================'
        echo ' ERROR: number of restart files does not match nproc !'
        echo '======================================================'
        exit
      fi

      if [[ "$NP" -gt 1 ]]; then
        counter=0
        for i in $prevJob/$fname*.nc ; do
          printf -v target $soil_prefix".%04d.nc" $counter
          ln -s $i $target
          ((counter++))
        done
      else
        ln -s ${prevJob}/${fname}.nc ${soil_prefix}.nc
      fi
      md5sum $soil_prefix*.nc

    else
      echo '======================================================'
      echo ' WARNING: soil restart files missing in '$prevJob
      echo '======================================================'
      #exit

    fi

#   else
#     echo '======================================================'
#     echo ' WARNING: missing file: restart_soil.cfg !'
#     echo '======================================================'
#     #exit
  fi

  # erosed
  if [[ -f restart_erosed.cfg ]]; then

    echo '------------------------------------------------------'
    echo '-linking restart files for erosed'
    echo '------------------------------------------------------'

    erosed_prefix=$(cat restart_erosed.cfg | grep filename | awk '{print $2}' | awk -F"." '{print $1}')
    if [[ "$NP" -eq 1 ]]; then
      pattern=$erosed_prefix".nc"
    else
      pattern=$erosed_prefix"*.nc"
    fi

    if doesAnyFileExist $pattern; then
        echo '... remove existing files '$pattern
        rm $pattern
    fi

    fname=$(cat restart_erosed.cfg | grep filename | awk '{print $NF}' | awk -F"." '{print $1}')
    if doesAnyFileExist "${prevJob}/$fname*.nc"; then
      NF=`ls ${prevJob}/$fname*.nc | wc -l`
      if [[ "$NP" -ne "$NF" ]]; then
        echo '======================================================'
        echo ' ERROR: number of restart files does not match nproc !'
        echo '======================================================'
        exit
      fi

      if [[ "$NP" -gt 1 ]]; then
        counter=0
        for i in $prevJob/$fname*.nc ; do
          printf -v target $erosed_prefix".%04d.nc" $counter
          ln -s $i $target
          ((counter++))
        done
      else
        ln -s ${prevJob}/${fname}.nc ${erosed_prefix}.nc
      fi
      md5sum $erosed_prefix*.nc

    else
      echo '======================================================'
      echo ' WARNING: erosed restart files missing in '$prevJob
      echo '======================================================'
      #exit

    fi

#   else
#     echo '======================================================'
#     echo ' WARNING: missing file: restart_erosed.cfg !'
#     echo '======================================================'
#     #exit
  fi

else
  echo '======================================================'
  echo ' ERROR: folder with restart data missing: '$prevJob
  echo '======================================================'
  exit
fi

echo '------------------------------------------------------'
echo ' INFO: '$this' teminated regularly !'
echo '------------------------------------------------------'
rm stop_mossco
exit
