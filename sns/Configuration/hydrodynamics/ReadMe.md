---
author: Carsten Lemmen
date: 15 Sep 2016
title: The SNS hydrodynamics configuration
...

# Using the SNS hydrodynamics configuration

	cd $MOSSCO_SETUPDIR
	make hydrodynamics
	mkdir -p mossco_getm # for output
	# adjust your parallelization configuration (e.g. par_setup.dat)
	mossco getm

# Contents

- `getm.inp`  with increased number of layers (60)
- `getm.yaml` with coupling time step 2 h
- `mossco_getm.cfg` write to `mossco_getm/` subdirectory for output
- `mossco_run.nml` run from July 25 - Sep 5 of year 2013
- `ReadMe.md` this documentation
