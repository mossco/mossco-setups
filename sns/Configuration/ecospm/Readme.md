# Ecology-SPM fully coupled simulation

## Testing chain

1. Pelagic only, ecology only

getm.inp :: jerlov = 6
maecs_env.nml :: a_water = 1.0, a_spm = 0.002
maecs_switches.nml :: kwFzmaxmethod = 4

2. Pelagic ecology + SPM

getm.inp :: jerlov = 5
maecs_env.nml :: a_water = 0.0, a_spm = 0.0007
maecs_switches.nml :: kwFzmaxmethod = 1
