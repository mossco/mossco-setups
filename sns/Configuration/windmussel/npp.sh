in=$1

ncks -O -v Phytoplankton_C_Uptake_Rate__phyUR_in_water,Phytplankton_Carbon_phyC_in_water,layer_height_in_water,time  ${in}.nc ${in}_1.nc
ncap2 -O -s "npp=Phytoplankton_C_Uptake_Rate__phyUR_in_water*Phytplankton_Carbon_phyC_in_water*layer_height_in_water" ${in}_1.nc ${in}_2.nc 
ncwa -O -y ttl -a getmGrid3D_getm_3 -v npp ${in}_2.nc  ${in}_3.nc 
ncatted -a units,npp,m,c,'mmol C m-2 d-2'  ${in}_3.nc
ncra -O -y avg ${in}_3.nc ${in}_4.nc   # annual-average rate in mmol C m-2 d-1
ncap2 -A -s "npp_annual=npp*516*10/24"  ${in}_4.nc # annual-accumulated rate over 516 10 hour intervals in mmol C m-2 a-1
ncrename -d time,year  ${in}_4.nc
ncks -O --fix_rec_dmn year ${in}_4.nc ${in}_4.nc
ncap2 -A -s "npp=npp_annual*12.0107/1000.0"  ${in}_4.nc ${in}_5.nc # npp in g C m-2 a-1
ncks -A -v npp  ${in}_5.nc ${in}.nc
rm -f ${in}_[12345].nc






