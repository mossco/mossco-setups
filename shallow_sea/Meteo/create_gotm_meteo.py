from pylab import *
from datetime import datetime,timedelta

windu=6.0 # m/s
windv=6.0 # m/s
hum=30. # %
airp=1020. # hPa
cloud = 0.6 # rel. cloudiness

def dayofyear(time):
    dt= time - datetime(time.year,1,1,0,0,0)
    return dt.days+1

def t2(doy,method='sin'):
    if method == 'sin':
        return -18./2*cos(2*pi/365.*(doy-20.))+12.0


start = datetime(2011,1,1,0,0,0)
end   = datetime(2013,2,1,0,0,0)
dt = timedelta(days=1,seconds=0)

f = open('meteo.dat','w')
f.write('#time\twindu\twindv\tairp\tt2\thum\tcloud\n')

for tidx in range((end-start).days):
    t = start + dt*tidx

    f.write('%s\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\n'%(str(t),windu*rand(1),windv*rand(1),airp,t2(dayofyear(t)),hum,cloud))

f.close()
