from pylab import *
import netCDF4 as nc4

nc=nc4.Dataset('shallowsea.nc')
ncv=nc.variables

time=ncv['time'][:]/86400.

monthslen=[31,28,31,30,31,30,31,31,30,31,30,31]
monthsnames=['J','F','M','A','M','J','J','A','S','O','N','D']
ticks=cumsum([ i for i in [0]+monthslen+monthslen[:-1]])
tickl=[ '   '+s for s in monthsnames+monthsnames]

varnames=['gotm_npzd_PAR','temp','salt','gotm_npzd_nut','gotm_npzd_phy','gotm_npzd_zoo','gotm_npzd_det']
numvar=len(varnames)

f=figure(figsize=(6,10))
f.subplots_adjust(top=0.95,bottom=0.05,hspace=0.5)

for i,varn in enumerate(varnames):
   subplot(numvar,1,i+1)

   if varn=='swr':
       dat=squeeze(ncv[varnames[i]][:])
   else:
       dat=squeeze(ncv[varnames[i]][:,5])
   plot(time,dat,'r-')
   title(ncv[varnames[i]].long_name+' [%s]'%ncv[varnames[i]].units,size=12.0)
   xlim(0,730)
   gca().set_xticks(ticks)
   gca().set_xticklabels([])

   ax  = gca()
   yt  = ax.get_yticks()
   ytl = ax.get_yticklabels()
   ax.set_yticks([yt[0],yt[-1]])
   ax.set_yticklabels([str(yt[0]),str(yt[-1])])

   gca().xaxis.grid(color='k',linestyle=':',linewidth=0.5)

gca().set_xticklabels(tickl)
xlabel('')
show()

nc.close()
