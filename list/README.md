#The List setup

The List setup simulates a shallow, tidal sea station in the Wadden Sea. Measurements might be available from the AWI Sylt.

Initial Omexdia+P concentrations are (so far) best pre-adjusted with:
nitrate 20.0 mmolN/m3
ammonium 5.0 mmolN/m3
phosphate 1.0 mmolP/m3
oxygen 200 mmolO2/m3
ODU 0.0 mmolO2/m3
fast_detritus_C_upward_flux 2.0 mmolN/m2/d
slow_detritus_C_upward_flux 1.0 mmolN/m2/d
detritus-P_upward_flux 0.1 mmolN/m2/d

