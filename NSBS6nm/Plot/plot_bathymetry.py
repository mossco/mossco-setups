#!/usr/bin/env python
#> @brief script for making 2-D plot of bathymetry

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2015 Helmholtz Zentrum Geesthacht
#> @author Onur Kerimoglu <onur.kerimoglu@hzg.de>, Richard Hofmeister <richard.hofmeister@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

""" module plot_mossco2Dmap.py
example call from shell:

$ python plot_meteo.py setupdir

"""

import numpy as np
import netCDF4,pickle,netcdftime,os,re,sys
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.basemap import Basemap


def do_plotbat(fnames,setup):

    proj=getproj(setup)

    # read the nc-file
    #nc=netCDF4.Dataset('../Topo/NSBS6nm.v01.nc')
    nc=netCDF4.Dataset(fnames[0])
    #nc=netCDF4.Dataset('maecsomexdia_soil_stitched.nc')

    ncv=nc.variables
    h = ncv['bathymetry'][:]
    lon = ncv['lon'][:]
    lat = ncv['lat'][:]
    nc.close()
    h[np.where(h > 250.)]=250.

    LO,LA = np.meshgrid(lon,lat)
    x,y = proj(LO,LA)

    filename=os.path.join(fnames[1],os.path.basename(fnames[0]).split('.nc')[0]+'.png') #pdf
    titlestr='bathymetry\n'+' '

    plot2Dmap(x,y,h,proj,setup,filename,titlestr,cbarstr='m')
    

def plot2Dmap(x,y,v,proj,setup,filename,titlestr,cbarstr):
    f = plt.figure(figsize=(10,6), dpi=96)
    f.subplots_adjust(left=0.0,right=1.0,bottom=0.0,top=.9)


    clim=[np.amin(v),np.amax(v)] #[np.amax(v)*.8,np.amax(v)] #
    pcf=proj.pcolormesh(x,y,v,cmap=plt.get_cmap('Greens'),vmin=clim[0], vmax=clim[1]) #Greys

    plt.title(titlestr)

    #setup specific features
    if setup in ['NSBS','NSBSfull','SNS']:
        #coastlines, etc
        #proj.drawcoastlines(color=(0.7,0.7,0.7),linewidth=0.2)
        #proj.fillcontinents((0.7,0.7,0.7),lake_color=(0.9,0.9,1.0))
        proj.drawcoastlines(color=(0.3,0.3,0.3),linewidth=0.5)
        proj.fillcontinents((1.0,1.0,1.0),lake_color=(0.9,0.9,1.0))
        plt.gca().patch.set_facecolor((0.9,0.9,1.0))
        #retrieve the axes position to set the colorbar position
        pos1 = plt.gca().get_position()

    if setup=='NSBS':
        #some annotation
        #mark_stats(proj, lang='en', stations=True, seas=True)
        #colorbar
        poscbar = [pos1.x0 + pos1.width/3+0.1, pos1.y0+0.18,  pos1.width/2, 0.03]
        cbaxes=f.add_axes(poscbar)
        plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
    elif setup=='NSBSfull':
        #some annotation
        #mark_stats(proj, lang='en', stations=True, seas=True)
        #colorbar
        poscbar = [pos1.x0 + pos1.width/3, pos1.y0+0.08,  pos1.width/2, 0.03]
        cbaxes=f.add_axes(poscbar)
        plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
    elif setup=='SNS':
        #colorbar
        poscbar = [pos1.x0 + pos1.width/2+0.1, pos1.y0+0.18,  pos1.width/2.5, 0.03]
        cbaxes=f.add_axes(poscbar)
        plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
    elif setup=='deep_lake':
        #colorbar
        cbaxes=plt.colorbar(pcf)

    #set colorbar title
    cbaxes.set_title(cbarstr,size=12.)

    plt.savefig(filename,dpi=300)
    print 'saved: '+filename
    #s=show()
    plt.close(f)

def getproj(setup):

    if os.path.isfile('proj.'+setup+'.pickle'):
        print 'opening an existing projection: '+ 'proj.'+setup+'.pickle'
        #if a projection exists, just load it (fast)
        (proj,) = np.load('proj.'+setup+'.pickle')
    else:
        print 'projecting for: '+ setup
        if setup=='NSBS':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-1.0,
                   llcrnrlat=50.5,
                   urcrnrlon=26.0,
                   urcrnrlat=59.5,
                   lat_0=54.0,
                   lon_0=20.)
        elif setup=='NSBSfull':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-4.0,
                   llcrnrlat=48.0,
                   urcrnrlon=32.5,
                   urcrnrlat=66.0,
                   lat_0=54.0,
                   lon_0=20.)
        elif setup=='SNS':
                proj=Basemap(projection='lcc',
                       resolution='i',
                       llcrnrlon=0.5,
                       llcrnrlat=52.5, #51
                       urcrnrlon=9.5,
                       urcrnrlat=57.5,
                       lat_0=52.0,
                       lon_0=5.)
        elif setup=='deep_lake':
            proj=Basemap(projection='lcc',
                         resolution='i',
                         llcrnrlon=-7.0,
                         llcrnrlat=47.8,
                         urcrnrlon=-4.0,
                         urcrnrlat=49.4,
                         lat_0=48.0,
                         lon_0=-5.0)
        else:
            raise 'unknown setup. cannot produce projection.'

        #pickle for later use:
        f=open('proj.'+setup+'.pickle','wb')
        pickle.dump((proj,),f) #,protocol=-1
        f.close()

    return proj

def mark_stats(proj, lang='en', stations=False, seas=True):
    if lang=='de':
        if stations:
            plot_station(proj,7.892,54.189,'Helgoland Reede\n25 m')
            plot_station(proj,8.073,54.061,'NOAH Station C',right=True)
            plot_station(proj,8.4412,55.017,'List Koenigshafen\n10 m')
            plot_station(proj,20.05,57.33,'Gotland Station 271\n240 m')
            plot_station(proj,12.685,54.411,'Zingster Bodden\n5 m',unsure=True)
        if seas:
            xx,yy=proj(1.0,56.5)
            plt.text(xx,yy,'Nordsee',size=18.,color=(0.3,0.3,0.3))
            xx,yy=proj(16.,56.5)
            plt.text(xx,yy,'Ostsee',size=18.,color=(0.3,0.3,0.3),rotation=30.)
            filename='Referenzstationen_Karte.pdf'
    elif lang=='en':
        if stations:
            plot_station(proj,7.892,54.189,'Helgoland\n25 m')
            plot_station(proj,8.4412,55.017,'List Koenigshafen\n10 m')
            plot_station(proj,20.05,57.33,'Gotland Station 271\n240 m')
            plot_station(proj,12.685,54.411,'Zingst Bay\n5 m',unsure=True)
        if seas:
            xx,yy=proj(1.0,56.5)
            plt.text(xx,yy,'North Sea',size=18.,color=(0.3,0.3,0.3))
            xx,yy=proj(16.,56.5)
            plt.text(xx,yy,'Baltic Sea',size=18.,color=(0.3,0.3,0.3),rotation=30.)


def plot_station(proj,lon,lat,name,unsure=False,right=False):
    xx,yy = proj(lon,lat)
    if unsure:
      facecolor=(0.9,0.8,0.8)
      edgecolor=(0.3,0.3,0.3)
    else:
      facecolor='orange'
      edgecolor='red'
    color='k'
    proj.plot([xx,],[yy,],'o',markersize=10, \
            markeredgecolor=edgecolor,markerfacecolor=facecolor, \
            markeredgewidth=3.)
    if right:
        xoffset,yoffset=(25000,10000) # m

        plt.text(xx+xoffset,yy-yoffset,name,horizontalalignment='left', \
            verticalalignment='top',size=10,color=color)
    else:
        xoffset,yoffset=(25000,10000) # m

        plt.text(xx-xoffset,yy+yoffset,name,horizontalalignment='right', \
            verticalalignment='top',size=10,color=color)

if __name__=='__main__':
    if len(sys.argv)>1:
        setupdir=sys.argv[1]
    else:
        setupdir= '/home/onur/WORK/setups/mossco-setups/NSBS6nm/'
    fnames=[None]*3
    fnames[0]=os.path.join(setupdir,'Topo','NSBS6nm.v01.nc')
    fnames[1]=os.path.join(setupdir,'Plot')

    if len(sys.argv)>2:
        setup=sys.argv[2]
    else:
        setup='NSBS'
        #setup='deep_lake'

    do_plotbat(fnames,setup)
