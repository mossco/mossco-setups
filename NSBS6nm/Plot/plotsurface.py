#!/usr/bin/env python
#> @brief script for making 2-D plots of variables at water surface

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2015 Helmholtz Zentrum Geesthacht
#> @author Onur Kerimoglu <onur.kerimoglu@hzg.de>, Richard Hofmeister <richard.hofmeister@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

""" module plot_mossco2Dmap.py
example call from shell:

$ python plotsurface.py file.nc timeindex variablename minval,maxval setupname

netcdf filename and timeindex have to be specified always. Other variable default
values:
  variablename - Chl_chl_in_water
  minval,maxval - full data range
  setupname - NSBSfull

"""

import numpy as np
import netCDF4,pickle,netcdftime,os,re,sys
import matplotlib.pyplot as plt
from pylab import *
from mpl_toolkits.basemap import Basemap


def plot_variable(proj,lon,lat,data,setup='NSBS',cmin=None,cmax=None,unitstr=''):
  f = gcf()
  LO,LA=meshgrid(lon,lat)
  x,y=proj(LO,LA)

  pcf=proj.pcolormesh(x,y,data)
  if cmin==None:
    cmin=data.min()
  if cmax==None:
    cmax=data.max()
  clim(cmin,cmax)

  proj.drawcoastlines(color=(0.3,0.3,0.3),linewidth=0.5)
  proj.fillcontinents((1.0,1.0,1.0),lake_color=(0.9,0.9,1.0))
  plt.gca().patch.set_facecolor((0.9,0.9,1.0))
  #retrieve the axes position to set the colorbar position
  pos1 = plt.gca().get_position()

  if setup=='NSBS':
    poscbar = [pos1.x0 + pos1.width/3+0.1, pos1.y0+0.18,  pos1.width/2, 0.03]
    cbaxes=f.add_axes(poscbar)
    plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
  elif setup=='NSBSfull':
    poscbar = [pos1.x0 + pos1.width/3, pos1.y0+0.08,  pos1.width/2, 0.03]
    cbaxes=f.add_axes(poscbar)
    plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')

  #set colorbar title (todo: test for deep lake)
  cbartitle=unitstr
  cbaxes.set_title(cbartitle,size=12.)

def getproj(setup):

    if os.path.isfile('proj.'+setup+'.pickle'):
        print 'opening an existing projection: '+ 'proj.'+setup+'.pickle'
        #if a projection exists, just load it (fast)
        (proj,) = np.load('proj.'+setup+'.pickle')
    else:
        print 'projecting for: '+ setup
        if setup=='NSBS':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-1.0,
                   llcrnrlat=50.5,
                   urcrnrlon=26.0,
                   urcrnrlat=59.5,
                   lat_0=54.0,
                   lon_0=20.)
        elif setup=='NSBSfull':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-4.0,
                   llcrnrlat=48.0,
                   urcrnrlon=32.5,
                   urcrnrlat=66.0,
                   lat_0=54.0,
                   lon_0=20.)
        else:
            raise 'unknown setup. cannot produce projection.'

        #pickle for later use:
        f=open('proj.'+setup+'.pickle','wb')
        pickle.dump((proj,),f) #,protocol=-1
        f.close()

    return proj



if __name__=='__main__':
    filename = sys.argv[1]
    tidx = int(sys.argv[2])
    if len(sys.argv)>3:
      variable=sys.argv[3]
    else:
      variable='Chl_chl_in_water'
    if len(sys.argv)>4:
      cmin,cmax=[float(s) for s in sys.argv[4].split(',')]
    else:
      cmin=None
      cmax=None
    if len(sys.argv)>5:
      setup=sys.argv[5]
    else:
      setup='NSBSfull'

    proj = getproj(setup)
    nc=netCDF4.Dataset(filename)
    ncv=nc.variables

    lon=ncv['getmGrid3D_getm_lon'][:]
    lat=ncv['getmGrid3D_getm_lat'][:]
    data = ncv[variable][tidx,-1,:,:]

    figure()
    plot_variable(proj,lon,lat,data,setup,cmin=cmin,cmax=cmax,unitstr=ncv[variable].units)
    savefig('%s_%04d.png'%(variable,tidx),dpi=300)
    show()
    nc.close()
