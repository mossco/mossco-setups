#!/usr/bin/env python
#> @brief script for making 2-D plots of meteorological variables

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2015 Helmholtz Zentrum Geesthacht
#> @author Onur Kerimoglu <onur.kerimoglu@hzg.de>, Richard Hofmeister <richard.hofmeister@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

""" module plot_mossco2Dmap.py
example call from shell:

$ python plot_meteo.py setupdir 'var1','var2' day1,day2,day3

makes 2-D maps of filename.nc for var1,var2(,etc) and every day1,day2(,etc) of each month,
where vars are expected to have 2 dimensions. variable 'windspeed' is calculated according to
w=sqrt(u^2+v^2).

"""

import numpy as np
import netCDF4,pickle,netcdftime,os,re,sys
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from scipy import interpolate


def do_2Dplotmap(fnames, varnames,timeint,setup):

    # read bathymetry
    print 'opening:'+fnames[1]
    nc=netCDF4.Dataset(fnames[1])
    ncv=nc.variables
    h = ncv['bathymetry'][:]
    newlon = ncv['lon'][:]
    newlat = ncv['lat'][:]
    nc.close()

    proj=getproj(setup)

    #use lon-lat of bathymetry for proj
    LO,LA = np.meshgrid(newlon,newlat)
    x,y = proj(LO,LA)

    # read the nc-file
    #nc=netCDF4.Dataset('../Topo/NSBS6nm.v01.nc')
    print 'opening:'+fnames[0]
    nc=netCDF4.Dataset(fnames[0])
    #nc=netCDF4.Dataset('maecsomexdia_soil_stitched.nc')

    ncv=nc.variables

    #common variables
    lon=ncv['lon'][:]
    lat=ncv['lat'][:]
    #LO,LA = np.meshgrid(lon,lat)
    #x,y = proj(LO,LA)

    tv = nc.variables['time']
    utime=netcdftime.utime(tv.units)
    tvec=utime.num2date(tv[:])
    #days to plot:
    days=[tvec[ti].day for ti in range(0,len(tvec))]
    #(tind,)=np.where(np.in1d(days,timeint))
    #tind=np.arange(151,182,10)
    tind=[153,166,178]
    for varno,varname in enumerate(varnames):
        print varname,

        for i in tind:
            print str(i),

            if varname=='windspeed':
                cmap='GnBu'
                wu = ncv['u10'][i,:,:]
                wv = ncv['v10'][i,:,:]
                v=np.sqrt(wu*wu+wv*wv)
                unitstr=ncv['u10'].units
                longname='wind_speed_10m'
            else:
                cmap='YlOrRd'
                v = ncv[varname][i,:,:]
                unitstr=ncv[varname].units
                longname=varname
                #longname=ncv[varname].long_name.replace('_', ' ')

            LOm,LAm = np.meshgrid(lon,lat)
            v=nn_interpolation(LOm,LAm,v,LO,LA,extramask=h.mask,missing_value=-9999)

            f = plt.figure(figsize=(10,6), dpi=96)
            f.subplots_adjust(left=0.0,right=1.0,bottom=0.0,top=.9)

            if len(v.shape)==2: #directly plot the variable
                vI=v[:,:]
                suffix=''
            else:
                raise RuntimeError('ndim!=2')

            #plot
            #proj.contourf(x,y,h,levels=range(0,255,25),extend='both',cmap=cm.YlGnBu)
            #proj.contourf(x,y,h[i,:,:],extend='both',levels=np.arange(0.0,1.0,0.1),cmap=cm.YlOrRd) #cm.YlGnBu
            clim=[np.amin(vI),np.amax(vI)] #[np.amax(vI)*.8,np.amax(vI)] #
            pcf=proj.pcolormesh(x,y,vI,cmap=plt.get_cmap(cmap),vmin=clim[0], vmax=clim[1])

            plt.title(suffix+' '+longname+'\n'+str(tvec[i].date()))

            #setup specific features
            if setup in ['NSBS','NSBSfull','SNS']:
                #coastlines, etc
                #proj.drawcoastlines(color=(0.7,0.7,0.7),linewidth=0.2)
                #proj.fillcontinents((0.7,0.7,0.7),lake_color=(0.9,0.9,1.0))
                proj.drawcoastlines(color=(0.3,0.3,0.3),linewidth=0.5)
                proj.fillcontinents((1.0,1.0,1.0),lake_color=(0.9,0.9,1.0))
                plt.gca().patch.set_facecolor((0.9,0.9,1.0))
                #retrieve the axes position to set the colorbar position
                pos1 = plt.gca().get_position()

            if setup=='NSBS':
                #some annotation
                #mark_stats(proj, lang='en', stations=True, seas=True)
                #colorbar
                poscbar = [pos1.x0 + pos1.width/3+0.1, pos1.y0+0.18,  pos1.width/2, 0.03]
                cbaxes=f.add_axes(poscbar)
                plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
            elif setup=='NSBSfull':
                #some annotation
                #mark_stats(proj, lang='en', stations=True, seas=True)
                #colorbar
                poscbar = [pos1.x0 + pos1.width/3, pos1.y0+0.08,  pos1.width/2, 0.03]
                cbaxes=f.add_axes(poscbar)
                plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
            elif setup=='SNS':
                #colorbar
                poscbar = [pos1.x0 + pos1.width/2+0.1, pos1.y0+0.18,  pos1.width/2.5, 0.03]
                cbaxes=f.add_axes(poscbar)
                plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
            elif setup=='deep_lake':
                #colorbar
                cbaxes=plt.colorbar(pcf)

            #set colorbar title (todo: test for deep lake)
            cbartitle=unitstr
            cbaxes.set_title(cbartitle,size=12.)

            filename=os.path.basename(fnames[0]).split('.nc')[0]+'_'+varname+suffix+'_'+ str(tvec[i].date()) + '.png' #pdf
            plt.savefig(os.path.join(fnames[2],filename),dpi=300)
            #s=show()
            plt.close(f)
        print '.'

    #close the netcdf file
    nc.close()

def getproj(setup):

    if os.path.isfile('proj.'+setup+'.pickle'):
        print 'opening an existing projection: '+ 'proj.'+setup+'.pickle'
        #if a projection exists, just load it (fast)
        (proj,) = np.load('proj.'+setup+'.pickle')
    else:
        print 'projecting for: '+ setup
        if setup=='NSBS':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-1.0,
                   llcrnrlat=50.5,
                   urcrnrlon=26.0,
                   urcrnrlat=59.5,
                   lat_0=54.0,
                   lon_0=20.)
        elif setup=='NSBSfull':
            # initialise geographic projection (slow)
            proj=Basemap(projection='lcc',
                   resolution='i',
                   llcrnrlon=-4.0,
                   llcrnrlat=48.0,
                   urcrnrlon=32.5,
                   urcrnrlat=66.0,
                   lat_0=54.0,
                   lon_0=20.)
        elif setup=='SNS':
                proj=Basemap(projection='lcc',
                       resolution='i',
                       llcrnrlon=0.5,
                       llcrnrlat=52.5, #51
                       urcrnrlon=9.5,
                       urcrnrlat=57.5,
                       lat_0=52.0,
                       lon_0=5.)
        elif setup=='deep_lake':
            proj=Basemap(projection='lcc',
                         resolution='i',
                         llcrnrlon=-7.0,
                         llcrnrlat=47.8,
                         urcrnrlon=-4.0,
                         urcrnrlat=49.4,
                         lat_0=48.0,
                         lon_0=-5.0)
        else:
            raise 'unknown setup. cannot produce projection.'

        #pickle for later use:
        f=open('proj.'+setup+'.pickle','wb')
        pickle.dump((proj,),f) #,protocol=-1
        f.close()

    return proj

def mark_stats(proj, lang='en', stations=False, seas=True):
    if lang=='de':
        if stations:
            plot_station(proj,7.892,54.189,'Helgoland Reede\n25 m')
            plot_station(proj,8.073,54.061,'NOAH Station C',right=True)
            plot_station(proj,8.4412,55.017,'List Koenigshafen\n10 m')
            plot_station(proj,20.05,57.33,'Gotland Station 271\n240 m')
            plot_station(proj,12.685,54.411,'Zingster Bodden\n5 m',unsure=True)
        if seas:
            xx,yy=proj(1.0,56.5)
            plt.text(xx,yy,'Nordsee',size=18.,color=(0.3,0.3,0.3))
            xx,yy=proj(16.,56.5)
            plt.text(xx,yy,'Ostsee',size=18.,color=(0.3,0.3,0.3),rotation=30.)
            filename='Referenzstationen_Karte.pdf'
    elif lang=='en':
        if stations:
            plot_station(proj,7.892,54.189,'Helgoland\n25 m')
            plot_station(proj,8.4412,55.017,'List Koenigshafen\n10 m')
            plot_station(proj,20.05,57.33,'Gotland Station 271\n240 m')
            plot_station(proj,12.685,54.411,'Zingst Bay\n5 m',unsure=True)
        if seas:
            xx,yy=proj(1.0,56.5)
            plt.text(xx,yy,'North Sea',size=18.,color=(0.3,0.3,0.3))
            xx,yy=proj(16.,56.5)
            plt.text(xx,yy,'Baltic Sea',size=18.,color=(0.3,0.3,0.3),rotation=30.)


def plot_station(proj,lon,lat,name,unsure=False,right=False):
    xx,yy = proj(lon,lat)
    if unsure:
      facecolor=(0.9,0.8,0.8)
      edgecolor=(0.3,0.3,0.3)
    else:
      facecolor='orange'
      edgecolor='red'
    color='k'
    proj.plot([xx,],[yy,],'o',markersize=10, \
            markeredgecolor=edgecolor,markerfacecolor=facecolor, \
            markeredgewidth=3.)
    if right:
        xoffset,yoffset=(25000,10000) # m

        plt.text(xx+xoffset,yy-yoffset,name,horizontalalignment='left', \
            verticalalignment='top',size=10,color=color)
    else:
        xoffset,yoffset=(25000,10000) # m

        plt.text(xx-xoffset,yy+yoffset,name,horizontalalignment='right', \
            verticalalignment='top',size=10,color=color)

def nn_interpolation(lon,lat,values,newlon,newlat,extramask,missing_value=-9999):
    #take all as 1-D arrays
    lon1=np.reshape(lon,lon.size)
    lat1=np.reshape(lat,lon.size)
    val1=np.reshape(values,lon.size)

    #find indices to keep:
    ikeep=np.where((lon1>=newlon.min()-0.5) & (lon1<=newlon.max()+0.5) &
                   (lat1>=newlat.min()-0.5) & (lat1<=newlat.max()+0.5))
    #trim the arrays
    lon1=lon1[ikeep]
    lat1=lat1[ikeep]
    val1=val1[ikeep]

    if val1.size==0:
	return

    coords = np.asarray(zip(lon1,lat1))
    f = interpolate.NearestNDInterpolator(coords, val1)
    #f = interpolate.LinearNDInterpolator(coords, asarray(valuelist),fill_value=missing_value)
    res = f(newlon,newlat)
    #for whatever the reason, for indices falling out the data coverage,
    #interpolation doesn't result in nan by -8.??e+38
    maskdef=np.zeros(res.shape,dtype=bool)
    if hasattr(res,'mask'):
        maskdef=res.mask
    res[np.where((maskdef) | (np.isnan(res)) | (res<0) | (extramask) )]=missing_value
    return np.ma.masked_equal(res,missing_value)

if __name__=='__main__':
    if len(sys.argv)>1:
        setupdir=sys.argv[1]
    else:
        setupdir= '/home/onur/WORK/setups/mossco-setups/NSBS6nm/'
    fnames=[None]*3
    fnames[0]=os.path.join(setupdir,'Forcing','Meteo','CFSR.daymean.2009.nc')
    fnames[1]=os.path.join(setupdir,'Topo','NSBS6nm.v01.nc')
    fnames[2]=os.path.join(setupdir,'Plot','Meteo')
    if not os.path.exists(fnames[2]):
        os.mkdir(fnames[2])

    if len(sys.argv)>2:
        varnames=sys.argv[2].split(',')
    else:
        varnames=[#'gross_primary_production_GPPR_in_water',
                  #'Photosynthetically_Active_Radiation_dPAR_in_water',
                  'windspeed','sst']
        #varnames=['denitrification_rate_in_soil', 'dissolved_oxygen_in_soil']

    if len(sys.argv)>3:
        timeint=map(int, sys.argv[3].split(','))
    else:
        timeint=[1,15] #i.e, plot on every 1st and 15th of each month

    if len(sys.argv)>4:
        setup=sys.argv[4]
    else:
        setup='NSBS'
        #setup='deep_lake'

    do_2Dplotmap(fnames,varnames,timeint,setup)
