#!/bin/bash

S=mossco_gffn_out
R=6   # 8 rows, 22 * 8 = 176
C=8   # 6 columns, 36 * 6 = 213


for F in $S.??.nc ; do
  G=${F%%.nc}.surface.nc
  test -f $G && continue

  ncks -O -v time,getmGrid3D_getm_lon,getmGrid3D_getm_lat,getmGrid2D_getm_lat,getmGrid2D_getm_lon $F $G

  ncks -A -d getmGrid3D_getm_3,0,29,29 -v getmGrid3D_getm_layer,temperature_in_water,concentration_of_SPM_in_water_001 $F $G
  ncks -A -d getmGrid3D_getm_3,0,29,29 -v concentration_of_SPM_in_water_002,detritus_in_water,nutrients_in_water,photosynthetically_active_radiation_in_water $F $G
  ncks -A -d getmGrid3D_getm_3,0,29,29 -v zooplankton_in_water,phytoplankton_in_water $F $G
     
  ncks -A -v depth_averaged_x_velocity_in_water,depth_averaged_y_velocity_in_water,getmGrid2D_getm_lon,getmGrid2D_getm_lat $F $G
  ncks -A -v dissolved_oxygen_upward_flux_at_soil_surface,dissolved_reduced_substances_upward_flux_at_soil_surface,fast_detritus_C_upward_flux_at_soil_surface $F $G
  ncks -A -v water_depth_at_soil_surface,wave_direction,wave_height $F $G

  ncks -A -d ungridded00024,0,4,4 -v denitrification_rate_in_soil,detritus-P_in_soil,dissolved_oxygen_in_soil,dissolved_reduced_substances_in_soil $F $G
  ncks -A -d ungridded00024,0,4,4 -v fast_detritus_C_in_soil,slow_detritus_C_in_soil,mole_concentration_of_ammonium_in_soil,mole_concentration_of_nitrate_in_soil $F $G
  ncks -A -d ungridded00024,0,4,4 -v mole_concentration_of_phosphate_in_soil $F $G

done

exit

# Horizontal concatenation

for (( r= 0 ; r<$R ; r=$r + 1 )) ; do
  
  for (( c=0; c<$C ; c=$c + 1 )) ; do

    i=$(expr \( $R - 1 \) \* $r + $c )
    
    if [[ $i -lt 10 ]] ; then j=0$i ; else j=$i ; fi

    echo r=$r/$R, c=$c/$C, i=$i, j=$j
    
    if ! test -f $S.$j.nc ; then
      ncks -v temperature_at_soil_surface mossco_gfn_out.$j.nc $S.$j.nc
    fi

    if ! test -f $S.$j.r$r.c$c.nc ; then
      ncpdq -O --reorder getmGrid2D_getm_1,time $S.$j.nc $S.$j.r$r.c$c.nc  # Make x record dimension
    fi

  done

  rm -f $S.row-$r.nc
  ncrcat -A $S.??.r$r.c*.nc $S.row-$r.nc

  ncpdq -O --reorder getmGrid2D_getm_2,getmGrid2D_getm_1 $S.row-$r.nc $S.row-$r.nc

done

rm -f $S.tiled.nc
ncrcat -A $S.row-*.nc $S.tiled.nc

ncpdq -O --reorder time,getmGrid2D_getm_2 $S.tiled.nc $S.tiled.nc

