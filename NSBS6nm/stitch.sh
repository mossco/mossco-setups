#!/bin/bash

S=tsoil_gfn_out
R=6   # 8 rows, 22 * 8 = 176
C=8   # 6 columns, 36 * 6 = 213

# Horizontal concatenation

for (( r= 0 ; r<$R ; r=$r + 1 )) ; do
  
  for (( c=0; c<$C ; c=$c + 1 )) ; do

    i=$(expr \( $R - 1 \) \* $r + $c )
    
    if [[ $i -lt 10 ]] ; then j=0$i ; else j=$i ; fi

    echo r=$r/$R, c=$c/$C, i=$i, j=$j
    
    if ! test -f $S.$j.nc ; then
      ncks -v temperature_at_soil_surface mossco_gfn_out.$j.nc $S.$j.nc
    fi

    if ! test -f $S.$j.r$r.c$c.nc ; then
      ncpdq -O --reorder getmGrid2D_getm_1,time $S.$j.nc $S.$j.r$r.c$c.nc  # Make x record dimension
    fi

  done

  rm -f $S.row-$r.nc
  ncrcat -A $S.??.r$r.c*.nc $S.row-$r.nc

  ncpdq -O --reorder getmGrid2D_getm_2,getmGrid2D_getm_1 $S.row-$r.nc $S.row-$r.nc

done

rm -f $S.tiled.nc
ncrcat -A $S.row-*.nc $S.tiled.nc

ncpdq -O --reorder time,getmGrid2D_getm_2 $S.tiled.nc $S.tiled.nc

