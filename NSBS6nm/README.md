# NSBS6nm setup

This setup is a 6 nautical mile North Sea-Baltic-Sea realistic setup, downsampled
from the 1 nm IOW setup by Ulf Gräwe.

Not all files required reside in this directory, please obtain the link to the
getm setup from one of the MOSSCO team.  Export this directory as the variable
`NSBS6M_DIR`

Then run `make` to link relevant files into this directory.

## Input files

filename | component | purpose
---------|-----------|---------
parallel.inp | getm | parallelization control parameters
fabm.nml | fabm_pelgaic | fabm model parameters
fabm_pelagic.nml | fabm_pelagic | component runtime parameters
fabm_sed.nml | fabm_sediment  | fabm model parameters
run_sed.nml | fabm_sediment | component runtime parameters
benthic_pelagic_coupler.nml | benthic_pelagic_coupler | coupler runtime parameters
benthic.nml | erosed | component runtime parameters
mbalthica.nml | benthos | macrofauna parameters
microphyt.nml | benthos | mirophytobenthos parameters
constant.dat | constant | constants to be exported
