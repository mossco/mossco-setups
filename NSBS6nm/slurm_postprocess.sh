#!/bin/bash -x

#SBATCH --ntasks=1
#SBATCH --ntasks-per-core=1
#SBATCH --output=postprocess-%j.stdout
#SBATCH --error=postprocess-%j.stderr
#SBATCH --time=02:00:00
#SBATCH --partition=batch
#SBATCH --job-name=postprocess-%j

#srun tail -10 PET0*
srun $MOSSCO_DIR/src/scripts/postprocess/process_fabmout.sh $PWD mossco_gffn $MOSSCO_DIR
