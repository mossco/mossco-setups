# -*- coding: utf-8 -*-
"""
# This script is is part of MOSSCO. It schedules restarts of
# generic MOSSCO examples.
#
# @copyright (C) 2015 Helmholtz-Zentrum Geesthacht
# @author Richard Hofmeister, Carsten Lemmen
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#
"""
import os
import sys
import glob
from datetime import datetime,timedelta

start = '2009-01-02 00:00:00'
stop  = '2009-12-31 23:00:00'

number_of_processors = 153
runid = 'ns-m-h'
output_directory = os.path.join(os.getcwd(),'Results',runid)

# Tested examples
# 1. just getm
#mossco_example='getm--netcdf'
#output_component='netcdf'

# 2. getm + fabm_pelagic
#mossco_example = 'getm--fabm_pelagic--restart'
#output_component = 'mossco_gfr'

# 3. getm + fabm_sediment
#mossco_example = 'getm--fabm_sediment--restart'
#output_component = 'mossco_gfr'

# 3. getm + fabm_pelagic + fabm_sediment
mossco_example = 'gffrpr'
output_component = 'mossco_gffrpr'

# if restart_directory is not set, the first simulation perdiod
# will not restart, but initialize completely from namelist
# if restart_directory is set, the script assumes all restart
# input files in the restart_directory, you possibly want to set a
# different runid in order to avoid overwriting the previous results

#restart_directory = os.path.join(os.getcwd(),'Results/test/hot_001')

# ----------------------
# do not change below this line

def increment_months(T,months=1):
  if T.month+months>12:
    addyear = (months+T.month)/12
    newmonth = (months+T.month)%12
  else:
    addyear = 0
    newmonth = T.month + months
  return datetime(T.year+addyear,newmonth,T.day,T.hour,T.minute,T.second)

def replace_line(filename,pattern,newtext):
  backupfile=filename+'.bak'
  os.system('mv %s %s'%(filename,backupfile))
  f = open(backupfile,'r')
  fn = open(filename,'w')
  for l in f.readlines():
    if pattern in l:
      fn.write("   %s\n"%newtext)
    else:
      fn.write(l)
  f.close()
  fn.close()

def create_cfg(filename, content):
  f = open(filename,'w')
  f.write(content)
  f.close()

def create_mossco_run(start,stop):
  f = open('mossco_run.nml','w')
  f.write('''
&mossco_run
 title = "deep_lake-hotstart",
 start = "%s",
 stop= "%s",
 loglevel = 'trace',
 logflush = .false.,
/
  '''%(start.strftime(tformat),stop.strftime(tformat)))
  f.close()

def increment_days(T,days=1):
  return T + timedelta(days)

tformat = '%Y-%m-%d %H:%M:%S'

Tstart = datetime.strptime(start, tformat)
Tstop  = datetime.strptime(stop, tformat)
Tcurr = Tstart
i = 0

# prepare filesystem
os.system('mkdir -p %s'%(output_directory))

while Tcurr<Tstop:
  Tnext = increment_months(Tcurr,1)
  #Tnext = increment_days(Tcurr,1)
  if Tnext>Tstop:
    Tnext = Tstop

  print("run_hotstart.py: run from %s to %s"%(Tcurr.strftime(tformat),Tnext.strftime(tformat)))

  # write mossco_run.nml
  create_mossco_run(Tcurr,Tnext)

  # prepare filesystem
  outdir = '%s/hot_%03d'%(output_directory,i)
  os.system('mkdir -p %s'%(outdir))

  # write output component.cfg
  create_cfg(output_component+'.cfg','filename: %s/%s.nc\n'%(outdir,output_component))

  # set restart filename for fabm components in restart.cfg
  if Tcurr == Tstart and 'restart_directory' not in locals():
    rc=os.system('rm -f restart_soil.cfg restart_water.cfg')
    replace_line('getm.inp',' hotstart =','hotstart = .false.')
    replace_line('getm.inp',' save_initial =','save_initial = .true.')
    replace_line('getm.inp',' out_dir =',"out_dir = '%s'"%outdir)
    print("run_hotstart.py: starting from initial conditions")
  else:
    if not(Tcurr == Tstart):
      restart_directory = output_directory+'/hot_%03d'%(i-1)
      print("run_hotstart.py: starting from restart directory")
    # prepare _in_water and _in_soil restart files for fabm components
    rc=os.system('ncks -O -v .*_in_soil %s/%s.nc %s/restart_soil.nc'%(restart_directory,output_component,outdir))

    rc=os.system('ncks -O -v .*_in_water %s/%s.nc %s/restart_water.nc'%(restart_directory,output_component,outdir))
    text = 'filename: %s/restart_soil.nc\n'%outdir
    create_cfg('restart_soil.cfg',text)

    text = 'filename: %s/restart_water.nc\n'%outdir
    create_cfg('restart_water.cfg',text)

    # Do not spinup sediment in subsequent runs
    replace_line('run_sed.nml','presimulation_years=','  presimulation_years = 0')
    replace_line('run_sed.nml','presimulation_years =','  presimulation_years = 0')

    # prepare GETM's namelists:
    replace_line('getm.inp',' hotstart =','hotstart = .true.')
    replace_line('getm.inp',' save_initial =','save_initial = .false.')
    replace_line('getm.inp',' out_dir =',"out_dir = '%s'"%outdir)
    # copy GETM's restart files
    rc=os.system('cd %s; for f in `ls restart*.out`; do cp ${f} %s/${f%%\\.out}.in; done'%(restart_directory,outdir))

  # run MOSSCO
  rc=os.system('mossco -n%d  %s'%(number_of_processors,mossco_example))
  print rc

  # copy namelists
  os.system('cp mossco_run.nml getm.inp %s'%outdir)
  os.system('mv *%s.????.std??? %s'%(mossco_example, outdir))
  os.system('mv *%s.std??? %s'%(mossco_example, outdir))
  os.system('mv PET*%s %s'%(mossco_example, outdir))


  # done with the current period
  Tcurr = Tnext
  i = i+1

