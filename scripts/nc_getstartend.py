#> @brief get start and end dates of a netcdf data file

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2016 Helmholtz Zentrum Geesthacht
#> @author Richard Hofmeister <richard.hofmeister@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

import netCDF4
import netcdftime
import sys

if len(sys.argv) == 2:
  ncfile=sys.argv[1]
else:
  print(' usage: nc_getstartstop.py ncfile')
  exit()

nc = netCDF4.Dataset(ncfile)
ut = netcdftime.utime(nc.variables['time'].units)

print(' start: %s'%str(ut.num2date(nc.variables['time'][0])))
print('   end: %s'%str(ut.num2date(nc.variables['time'][-1])))
nc.close()

