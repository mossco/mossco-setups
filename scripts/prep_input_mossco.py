#!/usr/bin/env python
#> @brief Script to create input data for mossco

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2016 Helmholtz Zentrum Geesthacht
#> @author Onur Kerimoglu <onur.kerimoglu@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.


import numpy as np
import os,sys
from scipy import interpolate,interp
import netCDF4
import datetime
import pickle
import matplotlib.pyplot as plt

def main():
    plot=True
    regridmethod = 'linear'  # linear,NN (unnecessary regridding will be avoided automatically)
    tempmethod=''  #'clim' monthly climatology; '': none
    extrapolate_bw=True  #extrapolate backward in time to ensure that the model can run within the time interval bdy data exists
    zsuf0='_Z0'
    NumLayers=-1.0  #0.0 is the default, in which case real depths specified by zv0 will be used, otherwise Sigma-levels will be assumed with NumLayer number of layers
    zv0=[0.0,1.0,2.0,3.0,4.0,5.0,7.5,10.,12.5,15.0, 17.5,20.0,25.0,30.,35.0,40.0,45.0,50.,55.,60.]#output depth intervals

    inputtype = 'AtmDep-EMEP' #'BC-ECOHAM' 'BC-SPM' 'AtmDep-EMEP' 'spm' 'attenuation'

    mv_out=-9999.0 #not too critical, but needs to be consistent for each function so better to set here in main


    try:
        rootpath=os.path.join(os.environ['MOSSCO_SETUPDIR'],'sns')
    except:
        mossco_setupdir='/home/onur/setups/mossco-setups/sns/'
        # mossco_setupdir=='/Volumes/ifksrv05/devel/mossco/setups/scripts/'
        #mossco_setupdir=='/ocean-data/nasermoa/mossco-setups/sns/'
        #mossco_setupdir=='/net/themis/meer/nordsee/MOSSCO/03_Modelle/02_MOSSCO/mossco-setups/sns/'
        if os.path.exists(mossco_setupdir):
            rootpath=mossco_setupdir
        else:
            raise(ValueError('set a valid env.var MOSSCO_SETUPDIR, or mossco_setupdir inside the script'))

    topo=get_getm_bathymetry(fname=os.path.join(rootpath,'Topo', 'topo.nc'))

    newlon=topo['lons']
    newlat=topo['lats']
    proj=getproj(projpath=rootpath,setup='SNSfull')

    if inputtype in ['BC-ECOHAM','BC-SPM','attenuation','spm']: #both types have time and depth dimensions, so many methods apply to both
        if NumLayers==-1.0: #only one depth level
            zv=[-1.0] #just a dummy value
            zsuf=zsuf0
        elif NumLayers==0.0: #take the zv0 as the depths to be interpoalted
            zv = zv0
            zsuf = '_Z' + str(int(len(zv))) + 'Depths'
        elif NumLayers>1.0:
            zv=np.linspace(0,NumLayers-1,NumLayers)
            zsuf='_Z' + str(int(NumLayers)) + 'SigmaL'


        if inputtype in ['spm','attenuation']:
            originalbasefname = 'silt_johannes'
            # mask method: '' # if not '', multiply the resulting fields with horizontally heterogeneous masks
            maskmethod = ''  # 'WNconstfract'  # 'W1N-1I0' #simple test case

            fname_in = os.path.join(rootpath, 'Forcing', originalbasefname + '.nc')  # silt_sample.nc
            print fname_in
            varnames = {'spm': '1.00 silt'}

            units = ['g m-3']
            # dims in the input
            dims_in = {'t': 'time', 'z': 'depth', 'y': 'latitude', 'x': 'longitude'}
            # dims in the output
            dims = {'t': 'time', 'y': 'lat', 'x': 'lon'}
            mv_in = -9.999e+10

            # construct name suffixes to append the fnameout
            tempsuf = '_T' + tempmethod
            if tempmethod == '':
                tempsuf = ''
            masksuf = '_M' + maskmethod
            if maskmethod == '':
                masksuf = ''
            regridsuf = '_Rsns'  # + regridmethod
            if inputtype=='attenuation':
                inputsuf='_att'
            else:
                inputsuf=''
            fname_out = os.path.join(rootpath, 'Forcing',originalbasefname + zsuf + tempsuf + masksuf + regridsuf + inputsuf+'.nc')

        elif inputtype=='BC-SPM':
            originalbasefname='silt_sample'
            # mask method: '' # if not '', multiply the resulting fields with horizontally heterogeneous masks
            maskmethod = 'WNconstfract'  # 'WNconstfract'  # 'W1N-1I0' #simple test case
            if maskmethod != '':
                #create spatially heterogeneous masks
                masks=prescribe_boundary_mask(maskmethod,varnames=['SPM_001','SPM_002','SPM_003'],hdim=newlon.shape,mv_out=mv_out)

            fname_in = os.path.join(rootpath, 'Forcing', originalbasefname+'.nc') #silt_sample.nc
            print fname_in
            varnames={'SPM_001':'1.00 silt', #0.88
                      'SPM_002':'1.00 silt',#0.12
                      'SPM_003':'1.00 silt'}#0.00
            units=['g m-3']*len(varnames)
            #dims in the input
            dims_in={'t':'time','z':'depth','y':'latitude','x':'longitude'}
            #dims in the output
            dims={'t':'time','z':'level','y':'lat','x':'lon'}
            mv_in=-9.999e+10

            # construct name suffixes to append the fnameout
            tempsuf = '_T' + tempmethod
            if tempmethod == '':
                tempsuf = ''
            masksuf = '_M' + maskmethod
            if maskmethod == '':
                masksuf = ''
            regridsuf = '_Rsns' #+ regridmethod
            fname_out = os.path.join(rootpath, 'Forcing', originalbasefname + zsuf + tempsuf + masksuf + regridsuf + '.nc')

        if inputtype=='BC-ECOHAM':
            originalbasefname = 'bdy_3d_bio_ecoham_2000-2010_linear_ref'
            # mask method: '' # if not '', multiply the resulting fields with horizontally heterogeneous masks
            maskmethod = 'WNasis'
            if maskmethod!='':
                #create spatially heterogeneous masks
                masks=prescribe_boundary_mask(maskmethod,varnames=['hzg_maecs_nutN','hzg_maecs_nutP'],hdim=newlon.shape,mv_out=mv_out)

            #pre-processed:
            fname_in= os.path.join(rootpath, 'Forcing', originalbasefname+'.nc')
            varnames={'hzg_maecs_nutN':'1 hzg_maecs_nutN',
                      'hzg_maecs_nutP':'1 hzg_maecs_nutP'}
                      #'hzg_maecs_detC':'6.625 PON',
                      #'hzg_maecs_detN':'1 PON',
                      #'hzg_maecs_detP':'1 POP'
            # dims in the input:
            dims_in = {'t': 'time', 'z': 'zax', 'y': 'lat', 'x': 'lon'}

            #dims in the output:
            dims={'t':'time','z':'level','y':'lat','x':'lon'}
            units = ['mmol m-3'] * len(varnames)
            mv_in=-9.999e+10

            # construct name suffixes to append the fnameout
            tempsuf = '_T' + tempmethod
            if tempmethod == '':
                tempsuf = ''
            masksuf = '_M' + maskmethod
            if maskmethod == '':
                masksuf = ''
            regridsuf = '_Rsns' #+ regridmethod
            fname_out = os.path.join(rootpath, 'Forcing', originalbasefname+ zsuf + tempsuf + masksuf + regridsuf + '.nc')

        #create the fields
        newvals=[None]*len(varnames)
        varnames_out=[None]*len(varnames)
        longnames=[None]*len(varnames)

        for vari,varname_out in enumerate(varnames.keys()):
            print 'extracting: ' + varname_out
            var=get_fields_fromfile(fname_in, varnames[varname_out],dims_in,mv_in=mv_in,mv_out=-9999.0)

            #change the dim names
            for dimref,dimname in dims_in.iteritems():
                if dimref in dims.keys():
                    var[dims[dimref]] = var.pop(dimref)

            #extrapolate backward in time to ensure that the model can run within the time interval bdy data exists
            if extrapolate_bw:
                var=ext_bw(var,dims,days=31)

            #do the vertical interpolation, if necessary
            if NumLayers==-1.0: #take a single vertical layer
                var,zsuf=extract_Zlevel(var,'z',zsuf)
            elif 'z' in dims.keys():
                var=interp_vert(var,dims['z'],zv,NumLayers=NumLayers,extrapolate=True)

            #do the temporal interpolation, if necessary
            if tempmethod!='':
                tv=[datetime.date(2000,int(mon),15) for mon in np.arange(1,13,1)]
                var=interp_temp_loop(var,dims,tv,tempmethod)
            else:
                tv=var[dims['t']]

            #do the lateral re-gridding, if necessary
            if len(var[dims['y']].shape)==1:
                lon,lat = np.meshgrid(var[dims['x']],var[dims['y']])
            else:
                lon=var[dims['x']]
                lat=var[dims['y']]

            #dirty bug-fix: un-land the south-western boundary
            #topo['H'].mask[13:52, 0] = False
            newvals[vari]=regrid_loop(lon,lat,var,newlon,newlat,mv_out,extramask=topo['H'].mask,proj=proj,method=regridmethod)

            # apply masks, if requested
            if maskmethod!='':
                newvals[vari]=applymasks(masks[varname_out], newvals[vari], tv, zv,mv_out)

            varnames_out[vari]=varname_out
            longnames[vari]='original '+ inputtype +' variable:('+varnames[varname_out].split(' ')[1]+')*'+varnames[varname_out].split(' ')[0]+masksuf


        if inputtype == 'attenuation':
            newvals,varnames_out,longnames,units=calc_attcoefs(newvals,varnames_out,longnames,units,mv_out)

    elif inputtype == 'AtmDep-EMEP':
        originalbasefname ='atm_n_EMEP-hybridClim_NWCS20_2000-2010_timmean_cut'
        #originalbasefname = 'atm_n_EMEP-hybridClim_NWCS20_2000-2010_cut'
        mask_method= '' # if not '', multiply the resulting fields with horizontally heterogeneous masks
        regridmethod='linear' #linear,NN

        fname_in= os.path.join(rootpath, 'Forcing', originalbasefname+'.nc')
        varnames={'atmN':'1 atmN'}
                  #'hzg_maecs_nutP':'0.0625 atmN'}
        # dims in the input:
        dims_in = {'t': 'time', 'y': 'lat', 'x': 'lon'}

        #dims in the output:
        dims={'t':'time','y':'lat','x':'lon'}

        units = ['mmol m-2 s-1'] * len(varnames)
        mv_in=-999.

        #create the fields
        newvals=[None]*len(varnames)
        varnames_out=[None]*len(varnames)
        longnames=[None]*len(varnames)

        # construct name suffixes to append the fnameout
        tempsuf = '_T' + tempmethod
        if tempmethod == '':
            tempsuf = ''
        regridsuf = '_Rsns'  # + regridmethod
        fname_out = os.path.join(rootpath, 'Forcing', originalbasefname + tempsuf + regridsuf + '.nc')


        for vari,varname_out in enumerate(varnames.keys()):
            print 'extracting: ' + varname_out

            var=get_fields_fromfile(fname_in, varnames[varname_out],dims_in,mv_in=mv_in,mv_out=-9999.0)

            #change the dim names
            for dimref,dimname in dims_in.iteritems():
                var[dims[dimref]] = var.pop(dimref)

            # do the temporal interpolation, if necessary
            if tempmethod!='':
                tv = [datetime.date(2000, int(mon), 15) for mon in np.arange(1, 13, 1)]
                var = interp_temp_loop(var, dims, tv, tempmethod)
            else:
                tv = var[dims['t']]

            # do the lateral re-gridding, if necessary
            if len(var[dims['y']].shape) == 1:
                lon, lat = np.meshgrid(var[dims['x']], var[dims['y']])
            else:
                lon = var[dims['x']]
                lat = var[dims['y']]

            zv=0 #just a dummy value
            newvals[vari] = regrid_loop(lon, lat, var, newlon, newlat, mv_out, extramask=topo['H'].mask, proj=proj,
                                        method=regridmethod)

            varnames_out[vari] = varname_out
            longnames[vari] = 'original ' + inputtype + ' variable:(' + varnames[varname_out].split(' ')[1] + ')*' + \
                              varnames[varname_out].split(' ')[0]
    else:
        raise(ValueError('uknown input-type:%s',inputtype))


    #do the plot
    if plot:
        #if inputtype in ['BC-ECOHAM','BC-SPM',]:# such that the lat and lon are 2d fields,
        #    XX=newlon; YY=newlat
        #else: #such that the lat and lon are 1d arrays
        #    if len(var[dims['x']].shape)==1:
        #        XX,YY = np.meshgrid(var[dims['x']],var[dims['y']])
        plot_fields(newlon,newlat,tv,zv,newvals,varnames_out,longnames,units,fname_out,proj)

    #create the file
    create_ncfile(fname_out,newlon,newlat,tv,zv,newvals,varnames_out,longnames,units,dims,climatology=(tempmethod=='clim'),refdate=0,missing_value=mv_out,notify=True)

def calc_attcoefs(oldvals,varnames_out,longnames,units,mv_out):
    A = 0.58
    g1 = 0.35
    att_bg = 0.08596 #m-1, Devlin etal. 2008
    kc = 0.06729  # m2/g Devlin etal. 2008
    #att_bg=0.16 #m-1 tian etal 2009
    #kc=0.02 #m2/mg tian etal 2009
    spm = oldvals[varnames_out.index('spm')]
    att=att_bg+spm*kc #m-1
    g2 = 1.0/att #m

    sh=oldvals[0].shape
    newvars=['A','g1','g2']
    longnames=['fraction_of_red_light','abs_length_scale_of_red_light','abs_length_scale_of_blue-green_light']
    units=['-','m','m']

    newvals = [None] * len(newvars)
    newvals[0] = A * np.ones(sh)
    newvals[1] = g1 * np.ones(sh)
    newvals[2] = g2

    # set back the missing values
    mvi=oldvals[0] == mv_out
    newvals[0][mvi] = mv_out
    newvals[1][mvi] = mv_out
    newvals[2][mvi] = mv_out

    return (newvals,newvars,longnames,units)

def prescribe_boundary_mask(maskmethod,varnames,hdim,mv_out):
    if maskmethod == 'WNconstfract': #values to be updated
        varvals = {'SPM_001': [0.125, 0.88, np.nan, np.nan, mv_out],#W,N,E,S,inner
                   'SPM_002': [0.875, 0.12, np.nan, np.nan, mv_out],
                   'SPM_003': [0.0, 0.0, np.nan, np.nan, mv_out]
                   }

    elif  maskmethod == 'W1N-1I0': #simple test case for proof of concept
        varvals = {'SPM_001': [1.0, -1.0, np.nan, np.nan, 0.0],#W,N,E,S,inner
                   'SPM_002': [1.0, -1.0, np.nan, np.nan, 0.0],
                   'SPM_003': [1.0, -1.0, np.nan, np.nan, 0.0]
                   }
    elif maskmethod == 'WNasis':  # apply the west and north, multiplied by 1.0
        varvals={}
        for vari, varname in enumerate(varnames):
            varvals[varname]=[1.0, 1.0, np.nan, np.nan, mv_out]

    masks = {}
    w=1 #width of the valid (un-masked) frame
    for vari, varname in enumerate(varnames):
        masks[varname] = varvals[varname][4]*np.ones((hdim[0], hdim[1])) #default is missing value
        masks[varname][:, 1:w+1] = varvals[varname][0] #W #sns specific fix: skip the first column
        masks[varname][:, hdim[1]-w:hdim[1]+1] = varvals[varname][2] #E
        masks[varname][hdim[0]-1:hdim[0]+1, :] = varvals[varname][1]  # N
        masks[varname][0:w, :] = varvals[varname][3] #S

        #sns-fix: remask the western wash along the western boundary
        masks[varname][77:82,1:w+1] = varvals[varname][4]

    return (masks)

def applymasks(mask,vals,tv,zv,mv_out):
    print '  masking'
    newvals=vals.copy()
    for ti in range(len(tv)):
        for zi in range(len(zv)):
            slice=vals[ti, zi, :, :] * mask
            slice[np.where(mask==mv_out)]=mv_out #apply the mv's from mask
            newvals[ti,zi,:,:]=slice
    #above action might have distorted the missing-value flags of the original data. Restore.
    newvals[np.where( (vals==mv_out) | np.isnan(vals) )]=mv_out
    return(newvals)

def ext_bw(var,dims,days=0,date=datetime.date(1999,12,31)):

    #extend the time dim
    t0=var[dims['t']]
    if days==0:
        if t0[0]<date:
            raise(ValueError('first date:'+str(t0[0])+'is smaller than the prescribed backward extrapolation date:'+str(date)))
        var[dims['t']]=[date]
    else:
        var[dims['t']]=[t0[0]-datetime.timedelta(days=days)]
    var[dims['t']].extend(t0)

    #extend the values
    val0=var['val']
    var['val']=np.ndarray((val0.shape[0]+1,val0.shape[1],val0.shape[2],val0.shape[3]))
    var['val'][1:,:,:,:]=val0[:,:,:,:]
    var['val'][0,:,:,:]=val0[0,:,:,:]

    print 'extrapolated the data set backward to:'+str(var[dims['t']][0])

    return(var)

def prescribe_climatology(lon,lat,varname,method,tv,zv,sLow=0.1,varval=1.0):

    newval=np.ndarray((len(tv),len(zv),lat.shape[0],lat.shape[1]))

    #laterally homogeneous methods
    if method in ['const', 'WinterHigh_SummerLow']:
        if method=='const':
            clim=[varval*np.ones(12)]
        elif method=='WinterHigh_SummerLow':
            varvals={'hzg_maecs_nutN':10.,
                     'hzg_maecs_nutP':0.5}
            clim=[None]*len(zv)
            for zi,z in enumerate(zv):
                f=interpolate.interp1d(np.array([1,5,10,12]),np.array([1,sLow[zi],sLow[zi],1]))
                clim[zi]=f(tv)*varvals[varname]
        for ti,t in enumerate(tv):
            for zi,z in enumerate(zv):
                newval[ti,zi,:,:]=clim[zi][ti]

    #laterally heterogeneous methods
    if method == 'presc_ICES':
        varvals={'hzg_maecs_nutN':[2.,2.,2.,2.,
                                   20.,20.,20.,20.,
                                   20.,20.,20.,20.,
                                   10.,10.,10.,10.],
                 'hzg_maecs_nutP':[.1,.1,.1,.1,
                                   2.,2.,2.,2.,
                                   2.,2.,2.,2.,
                                   1.,1.,1.,1.]}
        coords=[[-2,57],[-2,56],[-1,56],[-1,57],
                [9,57],[9,56],[10,56],[10,57],
                [9,51],[9,50],[10,50],[10,51],
                [-2,51],[-2,50],[-1,50],[-1,51]]

    #depth=time interpolator
    for zi,z in enumerate(zv):
        f=interpolate.interp1d(np.array([1,5,10,12]),np.array([1,sLow[zi],sLow[zi],1]))
        ft=f(tv)
        for ti,t in enumerate(tv):
            #lateral interpolator
            values=[ft[ti]*varvals[varname][ci] for ci in range(len(coords)) ]
            newval[ti,zi,:,:]=interpolate.griddata(coords,values,(lon,lat),method='linear')

    #construct a suffix for the file name
    zsuf=''
    if len(zv)>1:
        zsuf='_z'+'-'.join([str(int(z)) for z in zv])

    return(newval,zsuf)

def interp_temp_loop(var,dims,tv,tempmethod):
    if tempmethod=='clim':
        print '  calculating climatology',
        values=var['val'][:]
        dates=var[dims['t']][:]
        if len(np.shape(values))==3:
            vals=calc_clim(values,dates,tv)
        elif len(np.shape(values))==4:
            d1len=np.shape(values)[1]
            vals=np.ndarray(shape=(len(tv),np.shape(values)[1],np.shape(values)[2],np.shape(values)[3]), dtype=float, order='F')
            print ' z#',
            for d1i in range(0,d1len):
                print str(d1i),
                vals[:, d1i, :, :] =calc_clim(values[:,d1i,:,:],dates,tv)
            print '.'

        var['val']=vals
        var[dims['t']] = tv
    else:
        ValueError('unknown temporal method')

    return (var)

def calc_clim(valsorg,dates,tv):

    months=np.array([dates[ti].month for ti in range(0,len(dates))])
    valsnew=np.ndarray((12,valsorg.shape[1],valsorg.shape[2]))
    tv_months=[t.month for t in tv]
    for m in tv_months:
        tind=np.where(months==m)[0]
        if len(tind)>1:
            valsnew[m-1,:,:]=np.mean(valsorg[tind,:,:], axis=0)
        elif len(tind)==1:
            valsnew[m-1,:,:]=valsorg[tind,:,:]
        else:
            valsnew[m-1,:,:]=valsorg[0,:,:]

    return(valsnew)

def extract_Zlevel(var,zdim,zsuf):
    print '  extracting depths:' + zsuf.split('_Z')[1]
    valsorg = var['val']
    valsorg[valsorg == var['mv']] = np.nan
    Zorg = var[zdim]

    #declare variable without depth-axis
    if len(valsorg.shape)==4:
        var['val']=np.ndarray((valsorg.shape[0],valsorg.shape[2],valsorg.shape[3]))
    elif len(valsorg.shape)==3:
        var['val']=np.ndarray((valsorg.shape[1],valsorg.shape[2]))

    dinfo=zsuf.split('_Z')[1]
    #if an interval is provided,calculate the average within the depth interval
    if '-' in dinfo:
        zS=float(dinfo.split('-')[0])
        zB=float(dinfo.split('-')[1])
        zi=(Zorg>=zS) & (Zorg<=zB)
        if len(valsorg.shape)==3:
            var['val'][:,:]=np.nanmean(valsorg[zi,:,:],axis=0)
        elif len(valsorg.shape)==4:
            for ti in range(valsorg.shape[0]):
                var['val'][ti,:,:]=np.nanmean(valsorg[ti,zi,:,:], axis=0)
    #otherwise find the closest depth to the requested depth
    else:
        z=float(dinfo)
        Zdif=[abs(Zorgi-z) for Zorgi in Zorg]
        zi=Zdif.index(min(Zdif))
        zsuf='_Z'+str(min(Zdif))
        if min(Zdif)!=z:
            print '  warning: requested depth:%s was not available. Extracting the closest:%s'%(z,min(Zdif))
        #zsuf.replace('.','')
        if len(valsorg.shape) == 3:
            var['val'][:, :] = valsorg[zi, :, :]
        elif len(valsorg.shape) == 4:
            for ti in range(valsorg.shape[0]):
                var['val'][ti, :, :] = valsorg[ti, zi, :, :]

    return((var,zsuf))

def interp_vert(var,zdim,zv,NumLayers=0.0,extrapolate=False):
    print '  interpolating depths. #layers:'+str(len(var[zdim]))+' -> '+str(len(zv))
    valsorg=var['val']
    valsorg[valsorg==var['mv']]=np.nan

    if len(zv)==1: #just calculate the average
        var['val']=np.ndarray((valsorg.shape[0],1,valsorg.shape[2],valsorg.shape[3]))
        var['val'][:,0,:,:]=np.nanmean(valsorg, axis=1)
    else:
        var['val']=np.ndarray((valsorg.shape[0],len(zv),valsorg.shape[2],valsorg.shape[3]))
        Zorg=var[zdim]
        var[zdim]=zv
        if not extrapolate:
            f_int=interpolate.interp1d(Zorg,valsorg,kind='linear',axis=1,fill_value=var['mv']) #,assume_sorted=True
            var['val']=f_int(zv)
        else:
            sh=valsorg.shape
            print '    extrapolating nans. frame#',
            for ti in range(0,sh[0]):
                print ti,
                for i in range(0,sh[2]):
                    for j in range(0,sh[3]):
                        vals=valsorg[ti,:,i,j]
                        validi= np.invert(np.isnan(vals))
                        if all(validi==False):
                            var['val'][ti,:,i,j]=np.nan
                        else:
                            if NumLayers != 0:
                                zv=np.linspace(np.max(Zorg[validi])/NumLayers/2.,np.max(Zorg[validi]),NumLayers)
                            #f_intext=interpolate.InterpolatedUnivariateSpline(Zorg[validi],vals[validi],k=1)
                            #var['val'][ti,:,i,j]=max(0.0,f_intext(zv))
                            var['val'][ti,:,i,j]=interp(zv,Zorg[validi],vals[validi])
            print ' done'

        #var['val']=np.ndarray((valsorg.shape[0],len(zv),valsorg.shape[2],valsorg.shape[3]))
        #var['val']=valsnew
        #var['val'][np.isnan(var['val'])]=var['mv']
    return(var)


def regrid_loop(lon,lat,var,newlon,newlat,mv_out,extramask=False,proj=0,method='linear'):
    #do lateral (x-y) regridding for time and/or z

    print '  lateral regridding',
    values=var['val'][:]
    if len(np.shape(values))==2:
        vals=regrid(lon,lat,values,newlon[:],newlat[:],var['mv'],mv_out,proj,extramask=extramask,method=method)
    if len(np.shape(values))==3:
        d0len=np.shape(values)[0]
        vals=np.ndarray(shape=(d0len,np.shape(newlon)[0],np.shape(newlon)[1]), dtype=float, order='F')
        for d0i in range(0,d0len):
            vals[d0i,:,:]=regrid(lon,lat,values[d0i,:,:],newlon[:],newlat[:],var['mv'],mv_out,proj,extramask=extramask,method=method)
    if len(np.shape(values))==4:
        d0len=np.shape(values)[0]
        d1len=np.shape(values)[1]
        vals=np.ndarray(shape=(d0len,d1len,np.shape(newlon)[0],np.shape(newlon)[1]), dtype=float, order='F')
        print 'frame#',
        for d0i in range(0,d0len):
            print str(d0i), #+'[z#',
            for d1i in range(0,d1len):
                #print(str(d1i)),
                vals[d0i,d1i,:,:]=regrid(lon,lat,values[d0i,d1i,:,:],newlon[:],newlat[:],var['mv'],mv_out,proj,extramask=extramask,method=method)
        print '.' #']'
    return(vals)

def regrid(lon,lat,values,lon2,lat2,mv_in,mv_out,proj,extramask=False,method='linear',trim=False):
    # compare the shapes to decide whether regridding is really necessary
    if np.array_equal(np.array(lon.shape),np.array(lon2.shape)):
        res = values
        # real dirty fix required as a result of buggy land-masking introduced during pre-interpolation
        #res[13:52,0]=res[13:52,1]
    else:
        # convert them to 1-D arrays
        lon1 = np.reshape(lon, lon.size)
        lat1 = np.reshape(lat, lon.size)
        val1=np.reshape(values,lon.size)
        #val1 = np.ma.masked_equal(np.reshape(values, lon.size), mv_in)
        #val1 = np.ma.masked_equal(np.reshape(values, lon.size), np.nan)

        # trim the spatially irrelevant portion
        spati= (lon1 >= lon2.min() - 0.5) & (lon1 <= lon2.max() + 0.5) &\
               (lat1 >= lat2.min() - 0.5) & (lat1 <= lat2.max() + 0.5)

        if len(np.where(spati))==0:
            'no overlap between the available & requested spatial range'

        #take only the valid values
        vali=np.invert(np.isnan(val1))
        #vali=np.invert(val1.mask))

        ikeep=np.where(spati & vali)

        lon1=lon1[ikeep]
        lat1=lat1[ikeep]
        val1=val1[ikeep]

        if val1.size==0: #this means no valid values, just fill the resulting array with mv_out, that will be masked later
            res=mv_out*np.ones((lon2.shape[0],lon2.shape[1]))
        else:
            #re-grid based on cartesian distances
            x1,y1=proj(lon1,lat1)
            coords = np.asarray(zip(x1,y1))
            if method=='NN':
                f = interpolate.NearestNDInterpolator(coords, val1)
            elif method=='linear':
                f = interpolate.LinearNDInterpolator(coords, val1,fill_value=mv_out)
            x2,y2=proj(lon2,lat2)
            res = f(x2,y2)

    #replace missing/invalid values with the specified mv_out
    maskdef=np.zeros(res.shape,dtype=bool)
    if hasattr(res,'mask'):
        maskdef=res.mask
    res[np.where( (maskdef) | (np.isnan(res)) | (res==mv_in) | (extramask) )]=mv_out

    return np.ma.masked_equal(res,mv_out)

def create_ncfile(fname,lon,lat,tvec,zvec,vals,names,longnames,units,dims,climatology=False,refdate=0,fill_value=-99.0,missing_value=-99,notify=False,add_lonlat=False):

    nc = netCDF4.Dataset(fname,'w',format='NETCDF3_CLASSIC')

    #DIMENSIONS

    #time
    if 't' in dims:
        if refdate == 0:
            refdate = datetime.datetime(2000, 1, 1)


        nc.createDimension(dims['t'],len(tvec))
        nctime=nc.createVariable(dims['t'],float,(dims['t']), fill_value=fill_value)
        if climatology:
            months=[t.month for t in tvec]
            nctime[:]=months
            nctime.units='climatological month'
        else:
            #if the tvec is defined as date objects, convert to datetime
            if type(tvec[0]) == datetime.date:
                tvec = [datetime.datetime.combine(t, datetime.datetime.min.time()) for t in tvec]
            deltadate=[t-refdate for t in tvec]
            tvec_s=[86400*deltadate[r].days +deltadate[r].seconds for r in range(len(deltadate))]
            nctime[:]=tvec_s
            nctime.units='seconds since '+str(refdate)

    #z
    if 'z' in dims:
        nc.createDimension(dims['z'],len(zvec))
        nczax=nc.createVariable(dims['z'],float,(dims['z']), fill_value=fill_value)
        nczax[:]=zvec
        if dims['z']=='level':
            nczax.units =''
        else:
            nczax.units='meters'

    #lat-lon
    if 'y' in dims:
        nc.createDimension('y',lat.shape[0])
        #nc.createDimension('y',lon.shape[0])
    if 'x' in dims:
        if len(lon.shape)==1:
            nc.createDimension('x',lon.shape[0])
        elif len(lon.shape)==2:
            nc.createDimension('x',lon.shape[1])

    if add_lonlat:
        if 'y' in dims:
            if len(lat.shape)==1:
                nclat=nc.createVariable(dims['y'],float,('y'), fill_value=fill_value)
            elif len(lat.shape)==2:
                #nclat=nc.createVariable(dims['y'],float,('y','x'), fill_value=fill_value)
                nclat=nc.createVariable(dims['y'],float,('y','x'), fill_value=fill_value)
            nclat[:]=lat
            nclat.units = 'degrees_north'

        if 'x' in dims:
            if len(lon.shape)==1:
                nclon=nc.createVariable(dims['x'],float,('x'), fill_value=fill_value)
            elif len(lon.shape)==2:
                #nclat=nc.createVariable(dims['y'],float,('y','x'), fill_value=fill_value)
                nclon=nc.createVariable(dims['x'],float,('y','x'), fill_value=fill_value)
            nclon[:]=lon
            nclon.units = 'degrees_east'

    #print 'filling in:'
    for varno in range(0,len(names)):
        #print names[varno],
        if 't' in dims:
            if 'z' in dims:
                #print '(t z y x)'
                ncvar=nc.createVariable(names[varno],float,(dims['t'],dims['z'],'y','x'), fill_value=fill_value)
                ncvar.coordinates=dims['t'] +' '+ dims['z'] +' '+ 'y' +' '+ 'x'
                #ncvar[0:len(tvec_s),:,:]=vals[varno]
            else:
                #print '(t y x)'
                ncvar=nc.createVariable(names[varno],float,(dims['t'],'y','x'), fill_value=fill_value)
                ncvar.coordinates= dims['t'] +' '+ 'y' +' '+ 'x'
                #ncvar[0:len(tvec_s),:,:]=vals[varno]
        else:
            if 'z' in dims:
                #print '(z, y, x)'
                ncvar=nc.createVariable(names[varno],float,(dims['z'],'y','x'), fill_value=fill_value)
                ncvar.coordinates=dims['z'] +' '+ 'y' +' '+ 'x'
            else:
                #print '(y x)'
                ncvar=nc.createVariable(names[varno],float,('y','x'), fill_value=fill_value)
                ncvar.coordinates='y' +' '+ 'x'

        ncvar[:]=vals[varno]
        ncvar.units=units[varno]
        ncvar.missing_value=missing_value
        ncvar.valid_min=0.0
        if longnames[varno]!='':
            ncvar.longname=longnames[varno]

    nc.sync()
    nc.close()
    if notify:
        print '\ndata written:'+fname

def get_fields_fromfile(fname,varname_mult,dims,mv_in=-99,mv_out=-99):
    import netcdftime

    nc=netCDF4.Dataset(fname)
    varname=varname_mult.split(' ')[1]
    mult=float(varname_mult.split(' ')[0])
    if ('+' in varname):
        varnames=varname.split('+')
        val=0
        for vari,varnamei in enumerate(varnames):
            vali=nc.variables[varnamei][:]*mult
            val=val+vali
        #val[vali==mv_in]=mv_out
        val[(val>(mv_in-1e-5)) * (val<(mv_in+1e-5)) *(val<-9999)]=np.nan
    else:
        val=nc.variables[varname][:]*mult
        nani=(val==mv_in) | (val.mask)
        val[val==nani]=np.nan

    try:
        unit=nc.variables[varname].units
    except:
        unit='1'
    try:
        longname=nc.variables[varname].long_name
    except:
        longname=varname

    var={'val':val,'unit':unit,'longname':longname,'mv':mv_out}

    #print '  dims:(',
    for dim_out,dim_in in dims.iteritems():
        if dim_out=='t':
            if nc.variables[dim_in].units=='climatological month':
                var[dim_out] = [datetime.date(2000, int(mon), 15) for mon in nc.variables[dim_in][:]]
            else:
                utime=netcdftime.utime(nc.variables[dim_in].units)
                var[dim_out]=utime.num2date(nc.variables[dim_in][:])
        else:
            var[dim_out]=nc.variables[dim_in][:]
    #print ')'
    #lat=nc.variables[dims[3]][:]
    #lon=nc.variables[dims[4]][:]

    #var={'time':time,'time_date':time_date,'lon':lon,'lat':lat,'val':val,'unit':unit,'longname':longname,'mv':-1}
    nc.close()
    return(var)

def plot_fields(lon,lat,tv,zv,vals,varnames,longnames,units,fname_out,proj,cmap='YlOrRd'):
    if len(vals[0].shape) == 3:
        valshape=vals[0][0,:,:].shape
    elif len(vals[0].shape) == 4:
        valshape= vals[0][0,0,:,:].shape

    if lon.shape == valshape:
        #re-define the lat-lon at boundaries, assuming they are originally provided at centers
        dlon=np.diff(lon,axis=1)/2.
        lonB=np.ndarray((lon.shape[0]+1,lon.shape[1]+1))
        for r in range(lon.shape[0]): #i.e., for each row (lat)
            lonB[r,:]=np.concatenate((np.array([lon[r,0]-dlon[r,0]]),lon[r,:-1]+dlon[r,:],np.array([lon[r,-1]+dlon[r,-1]])))
        lonB[r+1,:]=lonB[r,:]
        dlat=np.diff(lat,axis=0)/2.
        latB=np.ndarray((lat.shape[0]+1,lat.shape[1]+1))
        for c in range(lat.shape[1]): #i.e., for each column (lon
            latB[:,c]=np.concatenate((np.array([lat[0,c]-dlat[0,c]]),lat[:-1,c]+dlat[:,c],np.array([lat[-1,c]+dlat[-1,c]])))
        latB[:,c+1]=latB[:,c]
        x,y=proj(lonB,latB)
    else:
        x,y = proj(lon,lat)

    fh=8;fw=8;dpi=300;left=0.1;right=0.9;bottom=0.1;top=0.9; hsp=0.25; wsp=0.25

    for vari,varname in enumerate(varnames):
        uconv=1.0
        units=units[vari]
        if units=='mmol m-2 s-1':
            uconv=86400
            units='mmol m-2 d-1'

        if len(vals[vari].shape)==3:
            print '  '+varname+',frame #',
            f = plt.figure(figsize=(fw,fh), dpi=dpi)
            f.subplots_adjust(left=left,right=right,bottom=bottom,top=top, hspace=hsp, wspace=wsp)
            f.text(0.5,0.97,longnames[vari], horizontalalignment='center')
            #f.text(0.5,0.94,varnames[vari], horizontalalignment='center')
            for ti,t in enumerate(tv):
                print ti+1,
                if type(t)==np.float64:
                    title='month '+str(int(t))
                elif type(t)==datetime.date:
                    title=t
                elif type(t)==datetime.datetime:
                    title=t.date()
                ax=plt.subplot(np.ceil(len(tv)/3.0),min(3.0,len(tv)),ti+1)
                vmax=np.nanmax(vals[vari]*uconv)
                plot2Dmap(f,ax,[0,vmax],x[:,:],y[:,:],vals[vari][ti,:,:]*uconv,proj,cmap,titlestr=title,cbarlabel=units) #,cmap='YlOrRd',cbarpos='right',cbarlabel='Depth [m]'

                #plot2Dmap(f, ax, clim, x, y, V, proj, cmap='viridis', cbarpos='right', cbarlabel='Depth [m]',
                #          titlestr='',showparmer='True')

            filename=os.path.join(os.path.dirname(fname_out),os.path.basename(fname_out.split('.nc')[0])+'_'+varname+'.png')
            plt.savefig(filename,dpi=dpi)
            plt.close()
            print '.\n'+filename
        elif len(vals[vari].shape)==4:
            if len(zv)!=vals[vari].shape[1]:
                raise(Exception('var.shape[1] inconsistent with len(depths)'))

            print '  '+varname+'\nlayer# ',
            for zi,z in enumerate(zv):
                print str(zi+1)+', frame #',
                f = plt.figure(figsize=(fw,fh), dpi=dpi)
                f.subplots_adjust(left=left,right=right,bottom=bottom,top=top, hspace=hsp, wspace=wsp)
                f.text(0.5,0.97,longnames[vari], horizontalalignment='center')
                f.text(0.5,0.94,'depth='+str(z), horizontalalignment='center')

                for ti,t in enumerate(tv):
                    print ti+1,
                    if (type(t)==np.float64) or (type(t)==np.int64):
                        title='month '+str(int(t))
                    else:
                        title=datetime.date(t)
                    ax=plt.subplot(np.ceil(len(tv)/3.0),min(3.0,len(tv)),ti+1)

                    vmax= np.nanmax(vals[vari][:,zi,:,:]*uconv)
                    #std=np.nanstd(vals[vari][:,zi,:,:]) #(nanstd)
                    #vmax=vmax/std? todo
                    plot2Dmap(f,ax,[0,vmax],x[:,:],y[:,:],vals[vari][ti,zi,:,:]*uconv,proj,cmap,titlestr=title,cbarlabel=units) #,cmap='YlOrRd',cbarpos='right',cbarlabel='Depth [m]'
                Zsuf='_Z'+str(zi)
                filename=os.path.join(os.path.dirname(fname_out),os.path.basename(fname_out.split('.nc')[0])+'_'+varname+Zsuf+'.png')
                plt.savefig(filename,dpi=dpi)
                plt.close()
                print '.\n'+filename

def get_getm_bathymetry(fname='/Volumes/ifksrv05/devel/mossco/setups/sns/topo.nc'):
    ncB=netCDF4.Dataset(fname)
    ncBv=ncB.variables
    #bathymetry from topo_sns.nc
    lonx=ncBv['lonx'][:,:] #[99,139]
    latx=ncBv['latx'][:,:] #[99,139]
    lonc=0.25*(lonx[:-1,:-1]+lonx[:-1,1:]+lonx[1:,:-1]+lonx[1:,1:])
    latc=0.25*(latx[:-1,:-1]+latx[:-1,1:]+latx[1:,:-1]+latx[1:,1:])
    H = ncBv['bathymetry'][:,:] #[99,139])
    #topo={'H':H,'lats':latc, 'lons':lonc,'Hunit':ncBv['bathymetry'].units}
    topo={'H':H,'lats':latc, 'lons':lonc,'Hunit':'m'}
    ncB.close()
    return(topo)

def getproj(setup,projpath='/Volumes/ifksrv05/devel/mossco/setups/scripts/'):
    from mpl_toolkits.basemap import Basemap
    fname=os.path.join(projpath,'proj.'+setup+'.pickle')
    if os.path.isfile(fname):
        print 'opening an existing projection: '+ projpath+'proj.'+setup+'.pickle'
        #if a projection exists, just load it (fast)
        (proj,) = np.load(fname)
    else:
        print 'projecting for: '+ setup

        if setup=='SNSfull':
            proj=Basemap(projection='lcc',
                       resolution='i',
                       llcrnrlon=0.0,
                       llcrnrlat=51.0,
                       urcrnrlon=9.5,
                       urcrnrlat=56.0,
                       lat_0=52.0,
                       lon_0=5.)
        else:
            raise (Exception('unknown setup:' + setup + '. cannot produce projection.'))

        #pickle for later use:
        f=open(fname,'wb')
        pickle.dump((proj,),f) #,protocol=-1
        f.close()

    return proj

def plot2Dmap(f,ax,clim,x,y,V,proj,cmap='YlOrRd',cbarpos='right',cbarlabel='Depth [m]',titlestr='',showparmer='True'):

    S0d= V.shape[0] - x.shape[0]
    S1d= V.shape[1] - x.shape[1]
    if (abs(S0d)>1) or (abs(S1d)>1):
        raise(ValueError('shapes of lon/lat and values are unmatchable'))
    if S0d>0:
        xi=V.shape[0]-S0d
    else:
        xi=V.shape[0]
    if S1d>0:
        yi=V.shape[1]-S1d
    else:
        yi=V.shape[1]
    Vrs=V[0:xi,0:yi]
    #clim=[np.amin(v),np.amax(v)] #[np.amax(v)*.8,np.amax(v)] #
    Vm = np.ma.array(Vrs,mask=np.isnan(Vrs)) #mask

    if clim==0:
        #pcf=proj.pcolormesh(x,y,Vm[:,:-],cmap=plt.get_cmap(cmap))
        pcf=proj.pcolormesh(x,y,Vm,cmap=plt.get_cmap(cmap))
    else:
        #pcf=proj.pcolormesh(x,y,Vm[:,:-1],cmap=plt.get_cmap(cmap),vmin=clim[0], vmax=clim[1])
        pcf=proj.pcolormesh(x,y,Vm,cmap=plt.get_cmap(cmap),vmin=clim[0], vmax=clim[1])

    #coastlines, etc
    proj.drawcoastlines(color=(0.3,0.3,0.3),linewidth=0.5)
    proj.fillcontinents((1.0,1.0,1.0),lake_color=(0.9,0.9,1.0))
    ax.patch.set_facecolor((0.9,0.9,1.0))
    if showparmer:
        proj.drawparallels(np.arange(51.,57.,1.), labels=[1,0,0,0],fontsize=8)
        proj.drawmeridians(np.arange(-1.,10.,1.))
        proj.drawmeridians(np.arange(-1.,10.,2.), labels=[0,0,0,1],fontsize=8)

    #colorbar
    #retrieve the axes position to set the colorbar position
    pos1 = ax.get_position()
    if cbarpos=='right':
        cb=plt.colorbar(pcf,shrink=.8)
        cb.ax.tick_params(labelsize=8)
    if cbarpos=='bottom':
        cb=plt.colorbar(pcf,shrink=.6, orientation='horizontal')
        cb.ax.tick_params(labelsize=8)
    elif cbarpos=='inside':
        #
        poscbar = [pos1.x0 + pos1.width/2+0.1, pos1.y0+0.18,  pos1.width/3, 0.03]
        cbaxes=f.add_axes(poscbar)
        cbar=plt.colorbar(pcf, cax=cbaxes,orientation='horizontal')
        cbar.ax.tick_params(labelsize=8)
    cb.set_label(cbarlabel,size=8.)
    plt.title(titlestr,size=8,y=1.0)

if __name__=='__main__':
    main()
