#!/usr/bin/env python
#> @brief Script to create from a 3d boundary file a boundary or a collapsed boundary file with 
#    entries added from the Helgoland Roads time series

#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2016 Helmholtz Zentrum Geesthacht
#> @author Carsten Lemmen <carsten.lemmen@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#

import numpy as np
import netCDF4
import sys
import os

missing_value = -1e30 
collapse = False

def copyNetcdfContent(ncin, ncout):
 
  # Copy all globall attributes
  for att in ncin.ncattrs():
      ncout.setncattr(att,ncin.getncattr(att))

  # Copy all dimensions, reduce z axis
  for key,value in ncin.dimensions.iteritems():
      if (collapse and (key == 'zax' or key == 'z' or key == 'level')):
        ncout.createDimension(key, 1)
        collapsedDimension = key
      else:   
	collapsedDimension = 'none'
        ncout.createDimension(key, len(value))

  # Copy all variable infrastructure
  for key,value in ncin.variables.iteritems():
      ncout.createVariable(key,value.datatype,dimensions=value.dimensions,fill_value=value._FillValue)
      var=ncout.variables[key]
      for att in value.ncattrs():
          try:
              var.getncattr(att)
          except:
              var.setncattr(att,value.getncattr(att))
    
      try:
          var.getncattr('missing_value')
      except:
          var.setncattr('missing_value',missing_value)

      if (key == 'lon'):
        try:
          var.getncattr('axis')
        except:
          ncout.setncattr('axis','X')
      if (key == 'lat'):
        try:
          var.getncattr('axis')
        except:
          ncout.setncattr('axis','Y')
      if (key == 'zax'):
        try:
          var.getncattr('axis')
        except:
          ncout.setncattr('axis','Z')
 
  # Create variables for those dimension that have none
  for key,value in ncout.dimensions.iteritems():
      if ncout.variables.has_key(key): continue
 
      ncout.createVariable(key,'i4',(key))
      var=ncout.variables[key]
      var[:]=range(0,len(value))
      if (key == 'x'): var.axis='X'
      if (key == collapsedDimension): var.axis='Z'
      if (key == 'y'): var.axis='Y'
      if (key == 'time'): var.axis='T'

   
  # Copy variable values and collapse z dimension
  for key,value in ncin.variables.iteritems(): 
      var=ncout.variables[key]
      if (value.shape == var.shape):
          var[:] = value[:]
      else:
          i=var.dimensions.index(collapsedDimension)
          if (len(var.shape) == 1):
              if (i==0): var[:]=value[0]
          elif (len(var.shape) == 2):
            if (i==0): var[:,:]=value[0,:]
            elif (i==1): var[:,:]=value[:,0]
          elif (len(var.shape) == 3):
            if (i==0): var[:,:,:]=value[0,:,:]
            elif (i==1): var[:,:,:]=value[:,0,:]
            elif (i==3): var[:,:,:]=value[:,:,0]
          elif (len(var.shape) == 4):
            if (i==0): var[:,:,:,:]=value[0,:,:,:]
            elif (i==1): var[:,:,:,:]=value[:,0,:,:]
            elif (i==2): var[:,:,:,:]=value[:,:,0,:]
            elif (i==3): var[:,:,:,:]=value[:,:,:,0]
           
 
def createNetcdfShape(ncid, shapename):
    
  for key,value in ncid.variables.iteritems():
      
     if len(value.shape) != 4: continue

     if shapename == 'box':
         value[:,:,1:-1,1:-1] = value.missing_value
 
     if shapename == 'cross':
       temp=np.array(value[:])
       value[:] = value.missing_value
       maxij=np.min(value.shape[2:4])
       for i in range(0,maxij):
         value[:,:,i,i] = temp[:,:,i,i]
       for i in range(0,np.min(value.shape[2:4])):
         value[:,:,i,maxij-1-i] = temp[:,:,i,maxij-1-i]

     if shapename == 'helgoland':
         
       lat=np.array(ncid.variables['lat'][:])
       lon=np.array(ncid.variables['lon'][:])
       
       dist=(lat[:] - 54.181015)**2 + (lon[:] - 7.897501)**2
       (i,j)=np.where(dist == dist.min())       
       
       temp=np.array(value[:])
       value[:] = value.missing_value
       value[:,:,i-1:i+1,j-1:j+1] = temp[:,:,i-1:i+1,j-1:j+1]

    
if __name__ == '__main__':

  if len(sys.argv)>1:
        infilename=sys.argv[1]
  else:
	#rootpath=os.path.join(os.environ['MOSSCO_SETUPDIR'],'sns')
    try:
        rootpath = os.path.join(os.environ['MOSSCO_SETUPDIR'], 'sns')
    except:
        mossco_setupdir = '/home/onur/setups/mossco-setups/sns/'
        # mossco_setupdir=='/Volumes/ifksrv05/devel/mossco/setups/scripts/'
        # mossco_setupdir=='/ocean-data/nasermoa/mossco-setups/sns/'
        # mossco_setupdir=='/net/themis/meer/nordsee/MOSSCO/03_Modelle/02_MOSSCO/mossco-setups/sns/'
        if os.path.exists(mossco_setupdir):
          rootpath = mossco_setupdir
        else:
          raise (ValueError('set a valid env.var MOSSCO_SETUPDIR, or mossco_setupdir inside the script'))

        infilename=os.path.join(rootpath,'bdy_3d_bio_ecoham_2000-2010_linear_ref_20SigmaL.nc')
        if not os.path.exists(infilename):
            raise RuntimeError('input file ' + infilename + ' could not be found')
 
  
  ncin=netCDF4.Dataset(infilename,'r')

  outfilename=infilename.split('.nc')[0] + '_helgoland.nc'
  ncout=netCDF4.Dataset(outfilename,'w',format='NETCDF3_CLASSIC')
  copyNetcdfContent(ncin, ncout)
  createNetcdfShape(ncout,'helgoland')
  ncout.close()
  print 'created: '+ outfilename

  outfilename=infilename.split('.nc')[0] + '_flat.nc'
  ncout=netCDF4.Dataset(outfilename,'w',format='NETCDF3_CLASSIC')
  copyNetcdfContent(ncin, ncout)
  ncout.close()
  print 'created: '+ outfilename

  outfilename=infilename.split('.nc')[0] + '_box.nc'
  ncout=netCDF4.Dataset(outfilename,'w',format='NETCDF3_CLASSIC')
  copyNetcdfContent(ncin, ncout)
  createNetcdfShape(ncout,'box')
  ncout.close()
  print 'created: '+ outfilename
      
  outfilename=infilename.split('.nc')[0] + '_cross.nc'
  ncout=netCDF4.Dataset(outfilename,'w',format='NETCDF3_CLASSIC')
  copyNetcdfContent(ncin, ncout)
  createNetcdfShape(ncout,'cross')
  ncout.close()
  print 'created: '+ outfilename
      
        
  ncin.close()
    


