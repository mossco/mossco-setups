
import os
import netCDF4

setupdir='/home/onur/setups/mossco-setups/sns/'

nc=netCDF4.Dataset(os.path.join(setupdir,'topo.nc'))
lonx=nc.variables['lonx'][:]
latx=nc.variables['latx'][:]
lonc=0.25*(lonx[:-1,:-1]+lonx[:-1,1:]+lonx[1:,:-1]+lonx[1:,1:])
latc=0.25*(latx[:-1,:-1]+latx[:-1,1:]+latx[1:,:-1]+latx[1:,1:])


riverinfofile = os.path.join(setupdir, 'riverinfo.dat')
fid = open(riverinfofile,'r')
nriver=int(fid.readline())

for line in fid:
    #words=re.split(r' *',line)
    words=line.split(' ')
    r=words[3]
    j=int(words[1])
    i=int(words[2])
    print ('%s  lon:%s, lat:%s'%(r,str(lonc[i,j]),str(latc[i,j])))