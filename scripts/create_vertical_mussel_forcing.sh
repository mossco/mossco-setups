#!/bin/bash

# This shell script is part of MOSSCO
#
# @copyright (C) 2016 Helmholtz-Zentrum Geesthacht
# @author Carsten Lemmen <carsten.lemmen@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

test -e ${MOSSCO_SETUPDIR} || exit 1

A=${MOSSCO_SETUPDIR}/sns/Forcing/mytilus_edulis_abundance_wzhang_whole_water_column.nc
B=${MOSSCO_SETUPDIR}/sns/Forcing/mytilus_edulis_abundance_wzhang_seabed_only.nc

test -f $A || exit 1
test -f $B || exit 1

ncdiff -O -v mytilus_edulis_mean_abundance $A $B  temp.nc
ncrename -v mytilus_edulis_mean_abundance,mussel_abundance_at_water_surface temp.nc
ncks -A -v mytilus_edulis_mean_abundance $B temp.nc
ncrename -v mytilus_edulis_mean_abundance,mussel_abundance_at_soil_surface temp.nc
ncatted -a missing_value,mussel_abundance_at_soil_surface,o,f,-99.0 temp.nc
ncatted -a missing_value,mussel_abundance_at_water_surface,o,f,-99.0 temp.nc
ncks -A -v longitude,latitude $A temp.nc
ncks -O -v longitude,latitude,mussel_abundance_at_water_surface,mussel_abundance_at_soil_surface \
  temp.nc ${MOSSCO_SETUPDIR}/sns/Forcing/mytilus_edulis_abundance_2.nc

exit 0
