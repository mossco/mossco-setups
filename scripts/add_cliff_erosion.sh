#!/bin/bash
#> @brief Adds to some predefined (i,j) indices SPM as boundary condition
#
#  This computer program is part of MOSSCO.
#> @copyright Copyright (C) 2016 Helmholtz-Zentrum Geesthacht
#> @author Carsten Lemmen <carsten.lemmen@hzg.de>
#
# MOSSCO is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License v3+.  MOSSCO is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
# LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.
#


#SPM Fraktionen	1	2	3
#Suffolk (g/m3)- i=40, j=54	300,00000	100,00000	100,00000
#Norfolk (g/m3)-i=10, j=69	270,00000	90,00000	90,00000
#Holderness (g/m3)-i=7, j=83	348,00000	116,00000	116,00000


if [[ "x$1" == "x" ]]; then
  echo "ERROR: Please specify an input file"
  exit 1
fi

IN=$1
OUT=${IN%.nc}_cliff.nc

echo "Adding cliff points to $IN and storing results in $OUT"

ncap2 -O -s"SPM_001(:,:,54,40)=300.0;SPM_002(:,:,54,40)=100.0;SPM_003(:,:,54,40)=100.0;SPM_001(:,:,69,10)=270.0;SPM_002(:,:,69,10)=90.0;SPM_003(:,:,69,10)=90.0;SPM_001(:,:,83,7)=348.0;SPM_002(:,:,83,7)=116.0;SPM_003(:,:,83,7)=116.0" $IN $OUT
