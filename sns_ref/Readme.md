This directory contains the setup for the Southern North Sea (SNS)
that can be run with difficult MOSSCO-coupled applications.

Input files can be linked to this directory by issuing the make target "input"

	make input

Currently, three preconfigured applications are available and can be linked
into this directyr by either of

	make default | ref3d | spm


For the default application, all files should be present already in this
setup.  For other applications, you may need to obtain additional forcing 
files.

GETM requires getm.inp which can be created by "make namelist_getm".
(existing getm.inp will be overwritten)
