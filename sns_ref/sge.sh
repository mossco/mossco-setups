#!/bin/bash

#$ -N sns_ref
#$ -pe orte_rr 61
#$ -m beas
#$ -M kai.wirtz@hzg.de
#$ -cwd
#$ -V

cat $PE_HOSTFILE

#mkdir -p 
#test -d  || (echo "Directory  could not be created" ; exit 1)

mpirun ./gfbfrrdw > sns-1x61-gfbfrrdw.stdout 2> sns-1x61-gfbfrrdw.stderr
