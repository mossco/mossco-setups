In Order to check the river_input component in sns, use the following changes in getm.inp, mossco_run.nml and parallel.inp,
gunzip rgf_1min.nc.gz and run the test.

diff --git a/sns/fabm_pelagic.nml b/sns/fabm_pelagic.nml
index dce5403..f0340fc 100644
--- a/sns/fabm_pelagic.nml
+++ b/sns/fabm_pelagic.nml
@@ -6,7 +6,7 @@
 !              3: 3/8-rule Runge-Kutta 4th order, 0: Euler 1st order
 !--------------
 &fabm_pelagic
- dt=100.
+ dt=60.
  dt_min=1.0
  ode_method=3
  background_extinction=0.13
diff --git a/sns/getm.inp b/sns/getm.inp
index ba676d1..996a91e 100644
--- a/sns/getm.inp
+++ b/sns/getm.inp
@@ -25,7 +25,7 @@
 
 &param
    dryrun = .false.,
-   runid = 'sns2d',
+   runid = "sns-1x2-rivertest",
    title = 'Southern North Sea',
    parallel = .true.,
    runtype = 4,
diff --git a/sns/mossco_run.nml b/sns/mossco_run.nml
index 27fca1b..1fae96c 100644
--- a/sns/mossco_run.nml
+++ b/sns/mossco_run.nml
@@ -1,7 +1,7 @@
 &mossco_run
- title = "sns",
+ title = "sns-1x2-rivertest",
  start = '2002-01-01 00:00:00',
- stop = '2002-01-31 00:00:00',
- loglevel = 'warning',
+ stop = '2002-01-01 00:10:00',
+ loglevel = 'all',
  logflush = .false.,
 /
diff --git a/sns/parallel.inp b/sns/parallel.inp
index 142ecc6..2a902bd 100644
--- a/sns/parallel.inp
+++ b/sns/parallel.inp
@@ -1,6 +1,6 @@
  &nampar
 ! -1=ONE_CELL,0=ONED_MESH,1=TWOD_MESH,2=MESH_FROM_FILE
-  MeshMethod=2,
+  MeshMethod=1,
   Reorder=.false.
 ! 0=ONED_SENDRECV,1=ONED_NONBLOCKING,2=TWOD_SENDRECV,3=TWOD_NONBLOCKING
   MsgMethod=3,
