This setup reproduces the study by Bayne 1993 et al on mussel filtration



The mussels were held in trays within a system of recirculating sea-water, which was temperature-controlled at 15 degree and a salinity of 32.5. Each tray held 12 mussels. Water was passed across the mussels at 6 l h-1.
