#!/bin/bash -x

# This is a run script for the moab queuing system as used on juropa.fz-juelich.de

#MSUB -l nodes=8:ppn=8
#MSUB -l walltime=0:10:00

#MSUB -M carsten.lemmen@hzg.de
#MSUB -m abe
#MSUB -N getm-fp-deep

# do not edit below this line
# ======================================================================
### start of jobscript

cd $PBS_O_WORKDIR
echo "workdir: $PBS_O_WORKDIR"

# NSLOTS = nodes * ppn = 8 * 8 = 64
NSLOTS=64

EXE=/home/${USER}/devel/MOSSCO/code/examples/generic/getm--fabm_pelagic--netcdf
test -x ${EXE} || (echo "Executable ${EXE} cannot be executed." ; exit 1)

echo "running on $NSLOTS cpus ..."

mpiexec -np $NSLOTS ${EXE} > $PBS_O_WORKDIR/$PBSJOBID.stdout 2> $PBS_O_WORKDIR/$PBSJOBID.stderr
