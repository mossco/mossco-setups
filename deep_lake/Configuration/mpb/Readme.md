This configuration directory contains configuration and namelist files
for (1) Macrophytobenthos and (2) Macrophytobenthos effect 1D simulations.

Copyright::
  2017, 2018 Helmholtz-Zentrum Geesthacht
Author::
  Carsten Lemmen <carsten.lemmen@hzg.de>
License::
  MOSSCO is free software: you can redistribute it and/or modify it under the
  terms of the GNU General Public License v3+.  MOSSCO is distributed in the
  hope that it will be useful, but WITHOUT ANY WARRANTY.  Consult the file
  LICENSE.GPL or www.gnu.org/licenses/gpl-3.0.txt for the full license terms.

# Macrophytobenthos effect

jfsweb.yaml:
  Coupling configuration for gotm--fabm_pelagic--fabm_sediment--simplewave--
  erosed--benthos_effect coupled model.
  Build this example with `mossco -b jfsweb`

mossco_jfsweb.cfg:
  Output component configuratino for jfsweb coupled application. Include or
  exclude fields for output here.

mbalthica.nml/microphyt.nml:
  Configuration for benthos effect model with input data on abundance of
  microphytobenthos and macrozoobenthos.  Default values are 30 mug g-1
  chlorophyll for microphytobenthos and 500 m-2 for Macoma balthica macrozoobenthos.

sedparams.txt/benthic.nml:
  Configuration for erosed model. Attention: adjust number of fractions with
  number of spm models in fabm.nml.

fabm.nml/fabm_pelagic.nml:
  Configurations for pelagic model component and pelagic models.

fabm_sed.nml/run_sed.nml:
  Configurations for sediment ecology component and models.





# Prognostic macrophytobenthos

This examples it currently *not* tested

mpb.yaml:
  Coupling configuration for benthic--pelagic system including
  hydrodynamics, waves, erosion, benthic and pelagic ecosystem
