# scaling setup

This simulates a 9.4 m deep rectangular lake, that is big enough to run
scaling experiments up to 10^5 processors. This setup runs without forcing data and is meant to produce no output except for a log. By default GETM is running with MAECS, 2 SPM size classes and the sediment model OMEXDIA for 2 hours.

## MOSSCO setups to run here

gffscaling

## How to do the scaling experiments

First clean up and create a topo.nc file for GETM (by default 1600x1600)

    make clean
    make topo

compile the executable

    mossco -br gffscaling

run and analize the experiment

    mossco -sF -n 10000 -lA gffscaling
    python $MOSSCO_DIR/src/scripts/postprocess/plot_petlog_by_time.py PET0.scaling*
