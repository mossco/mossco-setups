import commands

procnums=[96 ,288,1008,3024,6048]
times =  [80,25 ,8  ,3   ,2   ]
#procnums=[288]
#times =  [25]

dependencystr=''

for time,num in zip(times,procnums):

  jobid = '%04dp'%num # name of result directory
  command = 'sbatch --ntasks=%d --time=%d %s slurm_scaling.sh %s'%(num,time,dependencystr,jobid)
  print (command)
  err,out = commands.getstatusoutput(command)
  if err!=0:
    print ('out:\n'+str(out))
  else:
    dependencystr=' --dependency=afterok:%s'%out.split()[-1]
  
