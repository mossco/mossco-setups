from numpy import ones
from pylab import linspace
import netCDF4
import os,sys

if len(sys.argv)<3:
  print('usage: create_topo.py inum jnum')
else:
  inum = int(sys.argv[1])
  jnum = int(sys.argv[2])

lon = [0.0,10.0]
lat = [54.0,60.0]

nc=netCDF4.Dataset('topo.nc','w',format='NETCDF3_CLASSIC')

nc.createDimension('lonc',inum)
nc.createDimension('latc',jnum)

v = nc.createVariable('grid_type','i4',())
v.long_name = "Type of horizontal grid"
v.option_1_ = "Cartesian"
v.option_2_ = "Spherical"
v.option_3_ = "Curvilinear"
v.option_4_ = "Spherical Curvilinear"
v[:] = 2

v = nc.createVariable('lonc','f8',('lonc'))
v.long_name = 'longitude'
v.units = 'degrees east'
v[:] = linspace(lon[0],lon[1],inum)

v = nc.createVariable('latc','f8',('latc'))
v.long_name = 'latitude'
v.units = 'degrees north'
v[:] = linspace(lat[0],lat[1],jnum)

v = nc.createVariable('bathymetry','f8',('latc','lonc'))
v.long_name = 'mean water depth at soil surface'
v.units = 'm'
v.missing_value=-10.
v[:] = -10.*ones((jnum,inum),dtype='f8')
v[1:-1,1:-1] = 9.4*ones((jnum-2,inum-2),dtype='f4')

nc.close()
