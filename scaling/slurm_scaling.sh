#!/bin/bash -x

#SBATCH --output=gffscaling-%j.stdout
#SBATCH --error=gffscaling-%j.stderr
#SBATCH --partition=batch
#SBATCH --mail-type=FAIL
#SBATCH --job-name=gffscaling

jobid=$1

mkdir -p Results/$jobid

srun $HOME/MOSSCO/mossco-code/examples/generic/gffscaling

mv PET* *.stderr *.stdout Results/$jobid
cp *.nml Results/$jobid
